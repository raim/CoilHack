library(segmenTools)
options(stringsAsFactors=FALSE)

## R 4 colors
col4 <- c("#000000", "#DF536B", "#61D04F", "#2297E6",
          "#28E2E5", "#CD0BBC", "#EEC21F", "#9E9E9E")

## long repeating vector, w/o black and gray
colr <- rep(col4[2:7],6)
pchr <- rep(c(20,15,18,17,4),7) #c(rep(19,7), rep(4,7)) #
ltyr <- c(rep(1,6), rep(2,8))
dnsr <- c(rep(-1,6),rep(40,8))

## Endpoint measurements of GyrA, GyrB knock-down and topA overexpression
## vs. EVC (Wandana Murugathas, master thesis).
## data analysis for Behle et al. 2021

expid <- "20200602_WM_coilhack_endpoint"

## DATA PATHS
coilhack.path <- "~/work/CoilHack"
data.path <- file.path(coilhack.path,"experiments/RNAseq",expid)
inpath <- file.path(data.path,"analysis_4") # same as _3 but rm obsolete plots
outpath <- file.path(data.path,"analysis_4")


dir.create(file.path(outpath), showWarnings=FALSE)
dir.create(file.path(outpath,"goi"), showWarnings=FALSE)

FIG="png" # replace by sed from calling script
fig.type <-  FIG
sfig.type <- "png" # supplemental
hfig.type <- "png" # heatmaps/dense2d

W <- 3.5
H <- W/2
Wc  <- Hc <- 2
Wi <- Hi <- 3

## GOI
goi.file <- file.path(coilhack.path,"sequences","goi_selection.R")


### PARSE DATA (from 20200602_WM_RNAseq_merge_experiments.R)
infile <- paste0(expid,"_results.tsv")
data <- read.delim(file.path(inpath,infile))

### FILTER GENES WITH STRONG RESPONSE
## TODO: choose distribution-dependent threshold
gyrA.up <- data$GyrA_log2FoldChange > 5
gyrB.up <- data$GyrB_log2FoldChange > 5
topA.up <- data$TOPA_log2FoldChange > 2
gyrA.down <- data$GyrA_log2FoldChange < -5
gyrB.down <- data$GyrB_log2FoldChange < -5
topA.down <- data$TOPA_log2FoldChange < -2

## write out data
name.cols <- c("Locus","Feature",
               "name","ID","cyanobaseID","product","RefSeq", ## ,"EC.Number"
               grep("log2FoldChange",colnames(data),value=TRUE))
write.table(data[gyrA.up&gyrB.up,name.cols],
            file=file.path(outpath,"gyr_up.csv"),
            row.names=FALSE, quote=FALSE, na="", sep="\t")
write.table(data[gyrA.down&gyrB.down,name.cols],
            file=file.path(outpath,"gyr_down.csv"),
            row.names=FALSE, quote=FALSE, na="", sep="\t")


## COMPARE EXPERIMENTS

## LOAD COMMON GOI LIST

source(goi.file)
gois <- getGoi(data)

## local GOI
goi <- c(grep("gyr",data$name),grep("topA",data$name))#,
## grep("ftsZ",data$name),grep("min",data$name))
names(goi) <- data$name[goi]
goi <- goi[c(3,2,4,1)] ## FILTER & ORDER
goi.col <- rep("#FF0000",length(goi))
names(goi.col) <- names(goi)
goi.col["gyrA2"] <- 1 #"#969696" # col4[4] #"#009696"
goi.pch <- 1
goi.pch <- 1:length(goi)
names(goi.pch) <- names(goi)
goi.pch["gyrA"] <- 2
goi.pch["gyrB"] <- 6
goi.pch["topA"] <- 1
goi.pch["gyrA2"] <- 5

## replace gyrA2 name by cyanobase
names(goi)[4]  <- data$cyanobaseID[goi[4]]

addgoi <- function(x,y, goi, cols) {
    points(x[goi],y[goi],pch=19,col=cols)
    text(x[goi],y[goi],names(goi), col=cols, pos=2, font=2, cex=1.2)
}

LM <- 30
plotdev(file.path(outpath,"comparison"), type="png",
        width=3*W, height=W, res=300)
par(mfcol=c(1,3), mai=c(.35,.35,.1,.1), mgp=c(1.3,.4,0), tcl=-.25)
x <- data$GyrA_log2FoldChange
y <- data$GyrB_log2FoldChange
plot(x, y, xlim=c(-LM,LM),ylim=c(-LM,LM),pch=20, col="#00000033",
     xlab="log2(kd_gyrA/EVC)",ylab="log2(kd_gyrB/EVC)")
abline(h=0, col=8);abline(v=0, col=8)
addgoi(x, y, goi,goi.col)
points(x, y, col=ifelse(gyrA.up&gyrB.up, "#0000FFFF",NA),pch=20)
points(x, y, col=ifelse(gyrA.down&gyrB.down, "#FF0000FF",NA),pch=20)
x <- data$TOPA_log2FoldChange
y <- data$GyrA_log2FoldChange
plot(x, y, xlim=c(-LM,LM),ylim=c(-LM,LM),pch=20, col="#00000033",
     ylab="log2(kd_gyrA/EVC)",xlab="log2(ox_topA/EVC)")
abline(h=0, col=8);abline(v=0, col=8)
addgoi(x, y, goi,goi.col)
points(x, y, col=ifelse(gyrA.up&gyrB.up, "#0000FFFF",NA),pch=20)
points(x, y, col=ifelse(gyrA.down&gyrB.down, "#FF0000FF",NA),pch=20)
#points(x, y, col=ifelse(gyrA.up&topA.up, "#0000FFFF",NA),pch=20)
#points(x, y, col=ifelse(gyrA.down&topA.down, "#FF0000FF",NA),pch=20)
x <- data$TOPA_log2FoldChange
y <- data$GyrB_log2FoldChange
plot(x, y, xlim=c(-LM,LM),ylim=c(-LM,LM),pch=20, col="#00000033",
     ylab="log2(kd_gyrB/EVC)",xlab="log2(ox_topA/EVC)")
abline(h=0, col=8);abline(v=0, col=8)
addgoi(x, y, goi,goi.col)
points(x, y, col=ifelse(gyrA.up&gyrB.up, "#0000FFFF",NA),pch=20)
points(x, y, col=ifelse(gyrA.down&gyrB.down, "#FF0000FF",NA),pch=20)
#points(x, y, col=ifelse(gyrB.up&topA.up, "#0000FFFF",NA),pch=20)
#points(x, y, col=ifelse(gyrB.down&topA.down, "#FF0000FF",NA),pch=20)
dev.off()



## DETAILED ANALYSIS - Behle et al. 2021
lg2fc <- data[,grep("log2FoldChange",colnames(data))]
lg2se <- data[,grep("lfcSE",colnames(data))] # SE: standard error


lmx <- apply(lg2fc,1,max,na.rm=TRUE)

cat(paste(data$Product[which(lmx>20)],"with lg2 foldchange >20\n"))

##lg2fc.orig <- lg2fc
##lg2fc[abs(lg2fc)>50] <- NA #Inf

exp <- sub("_.*","",colnames(lg2fc))
colnames(lg2fc) <- exp

## expression for log2 expression data
expn <- exp
expn[exp=="TOPA"] <- expression(log[2](topA^OX/EVC))
expn[exp=="GyrA"] <- expression(log[2](gyrA^kd/EVC))
expn[exp=="GyrB"] <- expression(log[2](gyrB^kd/EVC))
names(expn) <- exp

## expression for 
expl <- exp
expl[exp=="TOPA"] <- expression(topA^OX)
expl[exp=="GyrA"] <- expression(gyrA^kd)
expl[exp=="GyrB"] <- expression(gyrB^kd)
names(expl) <- exp

## GENE NAMES to plot
gnms <- data$name
idx <- which(gnms=="")
gnms[idx] <- data$Locus[idx]
gnms <- sub("SGL_","",gnms)

## TODO: why is SGL_RS19505 NA in GyrA, but rather high in others?
MISSING <-is.na(lg2fc) 
lg2fc[MISSING] <- 0




library(segmenTools) # hist2d
library(viridis)


highlight <- function(idx, legend=FALSE, ...) {
    if ( length(idx) ) {
        cols <- pchs <- rep(1, length(idx))
        if ( legend ) {
            cols <- paste0(colr[1:length(idx)],"FF")
            pchs <- pchr[1:length(idx)]
        }

        if ( !is.null(names(idx)) )
            nms <- names(idx)
        else nms <- gnms[idx]
            
        points(lg2fc[idx,c(x,y)],cex=1, pch=pchs, col=cols, lwd=2)
        if ( !legend ) 
            text(lg2fc[idx,c(x,y)], labels=nms, xpd=TRUE,...)
        else
            legend("bottomright",nms, ncol=2, pt.lwd=2,cex=.8,
                   col=cols, pch=pchs, bg="#FFFFFF99",y.intersp=.75,box.col=NA)
    }
}
addgoi2 <- function(x,y, goi, cols, pchs, top) {
    points(x[goi],y[goi],col=cols,pch=pchs, lwd=2)
    ##text(x[goi],y[goi],names(goi), col=cols, pos=2, font=2, cex=1.2)
    arrows(x0=0,y0=0,x1=x[goi], y1=y[goi], length=.05, col="black",lwd=2)
    arrows(x0=0,y0=0,x1=x[goi], y1=y[goi], length=.05, col="white")
    if ( !missing(top) )
        points(x[goi[top]],y[goi[top]],col=cols,pch=pchs, lwd=2)
 
}


## PLOT DATA COMPARISON
show.extremes <- FALSE

## columns in lg2fc "TOPA" "GyrA" "GyrB"
combis <- list(c(1,2),
               c(3,2),
               c(1,3))
               

for ( i in 1:length(combis) ) {

    ## columns in lg2fc
    x <- combis[[i]][1]
    y <- combis[[i]][2]

    ylm <- xlm <- c(-12.75,12.75) # c(-13,13)
    if ( x==1 ) xlm <- c(-7,7)
    ## manipulated gene
    ## TODO: indicate manipulated genes in addgoi?

    cr <- cor.test(lg2fc[,x],lg2fc[,y])

     
    plotdev(file.path(outpath,paste0(expid,"_",exp[x],"_",exp[y])),
            type=hfig.type, width=2.5, height=2.5, res=300)
    par(mfrow=c(1,1),mai=c(.5,.5,.1,.1), mgp=c(1.4,0.3,0), tcl=-.25,
        xaxs="i",yaxs="i")
    #par(mai=c(.45,.45,.05,.05), mgp=c(1.25,.3,0), tcl=-.25,xaxs="i",yaxs="i")
    dense2d(lg2fc[,x],lg2fc[,y], xlab=NA, ylab=NA,
            xlim=ylm, ylim=ylm,cex=.5, col=viridis, axes=FALSE)
    axis(1)#, at=c(-10,10),las=1);axis(1, labels=FALSE, at=seq(-10,10,5))
    axis(2)#, at=c(-10,10),las=2);axis(2, labels=FALSE, at=seq(-10,10,5))
    axis(3, labels=FALSE, at=seq(-10,10,5))
    axis(4, labels=FALSE, at=seq(-10,10,5))
    box()
    mtext(expn[x], 1, par("mgp")[1], cex=.9)
    mtext(expn[y], 2, par("mgp")[1]*.7, cex=.9)
    legend("bottomright", legend=paste0("r=",round(cr$estimate,2)), bty="n")
    abline(h=0, col=1, lty=3)
    abline(v=0, col=1, lty=3)
    ## highlight extremes - TopA
    if (FALSE ) {
        idx <- which(lg2fc[,x] > 4.5)
        highlight(idx, pos=4)
        idx <- which(lg2fc[,x] < -4.5)
        highlight(idx, pos=2)
    }
    if ( show.extremes ) {
        ## dont plot twice, collect indices
        idxs <- numeric()
        
        ## highlight extremes - top 3 in y
        idx <- tail(order(lg2fc[,y]),3)
        highlight(idx, pos=3)#c(4,3,2))
        idxs <- unique(c(idxs,idx))
        
        ## highlight extremes - bottom 3 in y
        idx <- head(order(lg2fc[,y]),3)
        highlight(idx[!idx%in%idxs], pos=1)
        idxs <- unique(c(idxs,idx))
        
        ## highlight extremes - top 3 in x
        idx <- tail(order(lg2fc[,x]),3)
        highlight(idx[!idx%in%idxs], pos=4)
        idxs <- unique(c(idxs,idx))
        
        ## highlight extremes - bottom 3 in x
        idx <- head(order(lg2fc[,x]),3)
        highlight(idx[!idx%in%idxs], pos=2)
        idxs <- unique(c(idxs,idx))
    }
    
    ## no change in topA
    if (FALSE) {
        idx <- which(lg2fc[,y] < -7 & lg2fc[,x] >0)
        highlight(idx, pos=c(1,4))
        ## little change in gyrA
        idx <- which(lg2fc[,y] >0 & lg2fc[,x] < -3)
        highlight(idx, pos=2)
    }
    
    ## ADD GOI
    addgoi2(lg2fc[,x], lg2fc[,y], goi,goi.col,goi.pch)
    if ( x==3 && y==2 )
        legend("topleft",names(goi), col=goi.col, pch=goi.pch,
               y.intersp=.8, pt.lwd=2, cex=.7, bg=NA, box.col=NA)
    dev.off()
}

## Koskinen et al. 2016 - sigBCDE transcriptome

for ( x in exp ) {
    #x <- "GyrA"#"TOPA"
    #ylm <- xlm <- c(-13,13)
    ina <- is.na(data$sigBCDE_log2FC)
    cr <- cor.test(lg2fc[!ina,x],data$sigBCDE_log2FC[!ina])
    
    plotdev(file.path(outpath,paste0(expid,"_clusters_sigBCDE_",x)),
            type=fig.type, width=W, height=W, res=300)
    par(mai=c(.45,.45,.05,.05), mgp=c(1.25,.3,0), tcl=-.25,xaxs="i",yaxs="i")
    dense2d(lg2fc[!ina,x],data$sigBCDE_log2FC[!ina], col=viridis, cex=.5,
            xlab=NA, ylab=NA)#,xlim=ylm, ylim=ylm)
    abline(h=0, col=1, lty=3)
    abline(v=0, col=1, lty=3)
    legend("bottomright", legend=c(paste0("r=",round(cr$estimate,2)),
                                   paste0("p=",signif(cr$p.value,2))), bty="n")
    mtext(expn[x],1,par("mgp")[1])
    mtext(expression(log[2](Delta~"sigBCDE"/control)),2,par("mgp")[1])
   ## ADD GOI
    addgoi2(lg2fc[,x], data$sigBCDE_log2FC, goi,goi.col,goi.pch)
    dev.off()
}


### GOI LISTS

combis <- list(c(2,1),
               c(3,2),
               c(3,1))
               

for ( i in 1:length(combis) ) {

    ## columns in lg2fc
    x <- combis[[i]][1]
    y <- combis[[i]][2]

    ylm <- xlm <- c(-13,13)
    #if ( x==1 ) xlm <- c(-7,7)
    if ( y==1 ) ylm <- c(-7,7)

    for ( g in 1:length(gois) ) {
        file.name <- file.path(outpath,"goi",
                               paste0(expid,"_",exp[y],"_",exp[x],"_",
                                      names(gois[g])))
        plotdev(file.name,
                type=sfig.type, width=Wi, height=Wi, res=300)
        par(mai=c(.5,.5,.1,.1), mgp=c(1.3,.3,0), tcl=-.25,
            xaxs="i",yaxs="i")
        dense2d(lg2fc[,x],lg2fc[,y], xlab=NA, ylab=NA,
                xlim=xlm, ylim=ylm,cex=.5, col=grey.colors)
        ##plot(lg2fc[,x],lg2fc[,y], xlab=NA, ylab=NA,
        ##     xlim=xlm, ylim=ylm,cex=.1, col="#77777777", pch=19)
        mtext(expn[x], 1, par("mgp")[1])
        mtext(expn[y], 2, par("mgp")[1]*.7)
        abline(h=0, col=1, lty=3)
        abline(v=0, col=1, lty=3)
        abline(a=0, b=1, col=1, lty=3)

        idx <- gois[[g]]
        ## TODO: plot names only for diff.regulated ?
        highlight(idx, pos=4, legend=TRUE)
        dev.off()
    }
}




## explore in 3D
if ( FALSE ) {
    
    lm <- c(-12,12)
    for ( phi in seq(0,360,30) ) {
        scatter3D(x=lg2fc[,1], y=lg2fc[,2], z=lg2fc[,3], pch=19, cex=1,
                  phi = phi, bty ="g",ticktype = "detailed",
                  xlab=exp[1], ylab=exp[2], zlab=exp[3],
                  xlim=lm, ylim=lm, zlim=lm)
        Sys.sleep(1)
    }
    
    library("plot3Drgl")
    plotrgl()
    identify3d(x=lg2fc[,1], y=lg2fc[,2], z=lg2fc[,3])

    ## TODO: PCA useful?
    pca <- prcomp(t(lg2fc))

}

### REVISION qPCR and CRISPRi EVALUATION

## experiment colors
ecols <- col4[c(6,2,3)]
names(ecols) <- colnames(lg2fc)

error.bar <- function(x, y, upper, lower=upper, length=0.05,...){
  arrows(x,y+upper, x, y-lower, angle=90, code=3, length=length, ...)
}


## HOMEOSTASIS vs. qPCR CONTROLS

goiq <- c(grep("topA",data$name),
         grep("gyr",data$name),
         grep("rpoA",data$name))
names(goiq) <- data$name[goiq]

## sort to match order in qPCR endpoint plots
goiq <- goiq[c(1,4,3,2,5)]
goiq.col <- col4[c(4,2,7,8,8)]


for ( j in 1:ncol(lg2fc) ) {
    id <- colnames(lg2fc)[j]
    plotdev(file.path(outpath,paste0(expid,"_qPCR_",id)),
            type=fig.type, width=2.5, height=2.5, res=300)
    par(mfrow=c(1,1),mai=c(.6,.5,.1,.1), mgp=c(1.4,0.3,0), tcl=-.25)
    bx <- barplot(lg2fc[goiq,j], names.arg=names(goiq),ylim=c(-7,7),
            ylab=expression(log[2]~"fold change"), las=2, col=goiq.col)
    error.bar(bx, lg2fc[goiq,j], lg2se[goiq,j])
    abline(h=0)
    axis(4, labels=FALSE)
    legend("topright",expl[id], text.col=ecols[id], bty="n", cex=1.5)
    dev.off()
}


## pSNDY verification
goic <- which(data$Locus=="ER3413_RS20180")
names(goic) <- "rhaS"
goic.col <- col4[8]
goic.dns <- -1

plotdev(file.path(outpath,paste0(expid,"_pSNDY")),
        type=fig.type, width=2.5, height=2.5, res=300)
par(mfrow=c(1,1),mai=c(.65,.5,.1,.1), mgp=c(1.4,0.3,0), tcl=-.25)
bx <- barplot(unlist(lg2fc[goic,]), names.arg=expl[colnames(lg2fc)],
              ylim=c(-2,25),
              ylab=expression(log[2]~"fold change"), las=2,
              col=ecols[colnames(lg2fc)]) 
error.bar(bx, unlist(lg2fc[goic,]), unlist(lg2se[goic,]), col="black")
abline(h=0)
axis(4, labels=FALSE)
legend("top","rhaS", text.col=1, bty="n", cex=1.5)
dev.off()

## CRISPRi verification
goic <- c(unique(c(which(data$name=="gyrA"),
                   which(data$name=="gyrB"),
                   which(data$name=="topA"),
                   grep("gyrA2",data$name),
                   grep("sll1712",data$name),
                   grep("lrtA",data$name),
                   grep("rpoA",data$name),
                   gois$off_gyrA,gois$off_gyrB)))
names(goic) <-  sub("hisS.2","slr1560",
                    sub("gyrA2","sll1941",
                        data$name[goic]))
goic.col <- c(ecols[c("GyrA","GyrB","TOPA")],
              rep(col4[8],4),
              rep(ecols["GyrA"],2),
              rep(ecols["GyrB"],2))

goic.dns <- c(-1,-1,-1,-1,-1,-1,-1,
              40,40,40,40)

itype <- c(GyrA="CRISPRi",
           GyrB="CRISPRi",
           TOPA="pSNDY")

for ( j in 1:ncol(lg2fc) ) {
    id <- colnames(lg2fc)[j]
    plotdev(file.path(outpath,paste0(expid,"_CRISPRi_",id)),
            type=fig.type, width=2.5, height=2, res=300)
    par(mfrow=c(1,1),mai=c(.17,.5,.1,.1), mgp=c(1.2,0.3,0), tcl=-.25)
    bx <- barplot(lg2fc[goic,j],ylim=c(-8.5,8.5),
                  names.arg=NULL,#names(goic),
                  ylab=expn[id], las=2,
                  col=goic.col, density=goic.dns, lwd=1, axes=FALSE)
    error.bar(bx, lg2fc[goic,j], lg2se[goic,j], col="black")
    text(bx, -7.3, names(goic), srt=90, xpd=TRUE)
    abline(h=0)
    axis(2, at=seq(-8,8,2))
    axis(2, at=-8:8, labels=FALSE, tcl=par("tcl")/2)
    axis(4, at=seq(-8,8,2), labels=FALSE)
    axis(4, at=-8:8, labels=FALSE, tcl=par("tcl")/2)
    legend("topright",expl[id], text.col=ecols[id], bty="n", cex=1.5)
    idx <- which(tolower(names(goic))==tolower(id))
    ys <- c(2,.5); acol<- 2; ya <- .9; aang <- 90;tpos <- 4
    if ( id=="TOPA" ) {
        ys <- -ys; ya <- -ya; acol <- "green"; aang=45; tpos <- 4
    }
    arrows(x0=bx[idx], y0=ys[1], y1=ys[2], col=ecols[id],
           angle=aang, length=.05, lwd=5)
    text(bx[idx]-.9, ys[1]+ya, itype[id], srt=0,
         pos=tpos, cex=.7, col=ecols[id], adj=-1)
    
    dev.off()
}

## SIGMA FACTORS
sigs <- paste0("sig",toupper(letters[1:9]))
goic <- c()
for ( sig in sigs )
    goic <- c(goic, grep(sig, data$name))
names(goic) <-  sigs
goic.col <- colr[1:length(goic)]
              

goic.dns <- dnsr

for ( j in 1:ncol(lg2fc) ) {

    id <- colnames(lg2fc)[j]
    ylm <- c(-8,8)
    at <- seq(-8,8,2)
    atl <- seq(-8,8,1)
    if ( id == "TOPA" ) {
        ylm <- c(-1.5,1.5)
        at <- -2:2
        atl <- seq(-2,2,.5)
    }
    
    plotdev(file.path(outpath,paste0(expid,"_SIGMA_",id)),
            type=fig.type, width=2.5, height=2, res=300)
    par(mfrow=c(1,1),mai=c(.17,.5,.1,.1), mgp=c(1.2,0.3,0), tcl=-.25)
    bx <- barplot(lg2fc[goic,j],ylim=ylm,
                  names.arg=NULL,#names(goic),
                  ylab=expn[id], las=2,
                  col=goic.col, density=goic.dns, lwd=1, axes=FALSE)
    error.bar(bx, lg2fc[goic,j], lg2se[goic,j], col="black")
    text(bx, min(ylm), names(goic), srt=90, xpd=TRUE)
    abline(h=0)
    axis(2, at=at);
    axis(2, at=atl,labels=FALSE, tcl=par("tcl")/2)
    axis(4, at=at, labels=FALSE)
    axis(4, at=atl,labels=FALSE, tcl=par("tcl")/2)
    legend("top",expl[id], text.col=ecols[id], bty="n", cex=1.5, y.intersp=0)

    
    dev.off()
}
