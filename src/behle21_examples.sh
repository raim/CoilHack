#!/bin/bash

SRC=~/work/CoilHack/src

## defined range around rRNA loci
## 16S - 1500: 16Sa: 2453675+2000 = 2455675
##             16Sb: 3325053-2000 = 3323053
## 5S + 1500: 5Sa: 2448648-5000 = 2443648
##            5Sb: 3330080+5000 = 3335080
cd   ${MYDATA}/pcc6803/figures/behle21/rRNA
$GENBRO/src/plotFeature.R -i $GENDAT/pcc6803 --coor "1,2442648:2455675" -r 0 -F -s rrna_at2 -S $SRC/selections_pcc6803.R  -f png -v -W 4.5 -H .9 -o rrna_at2
$GENBRO/src/plotFeature.R -i $GENDAT/pcc6803 --coor "1,3323053:3336080" -r 0 -s rrna_at2 -S $SRC/selections_pcc6803.R  -f png -v -W 4.5 -H .9 -o rrna_at2
#$GENBRO/src/plotFeature.R -i $GENDAT/pcc6803 --coor "1,2442648:2455675" -r 0 -F -s rrna_at -S $SRC/selections_pcc6803.R  -f png -v -W 4.5 -H .9 -o rrna_at
#$GENBRO/src/plotFeature.R -i $GENDAT/pcc6803 --coor "1,3323053:3336080" -r 0 -s rrna_at -S $SRC/selections_pcc6803.R  -f png -v -W 4.5 -H .9 -o rrna_at
cd -
## selected TU
cd   ${MYDATA}/pcc6803/figures/behle21/TU/selected/
#$GENBRO/src/plotFeature.R -i $GENDAT/pcc6803 --coor "4,32868:36416;1,28866:31506;1,1532185:1537864;1,853385:862219;1,60507:66040;1,853385:862219;1,833768:842951;1,853385:862219;1,27873:33586"  -r 3000 -s tu -S $SRC/selections_pcc6803.R  -f png -v
$GENBRO/src/plotFeature.R -i $GENDAT/pcc6803 -n crhR --rng 3000 -s tu -S $SRC/selections_pcc6803.R  -f png -v -W 4 -H 3

$GENBRO/src/plotFeature.R -i $GENDAT/pcc6803 --coor "1,36586:24873"   -s tu -S $SRC/selections_pcc6803.R  -f png -v -W 4 -H 3
$GENBRO/src/plotFeature.R -i $GENDAT/pcc6803 --coor "1,845951:827768" -s tu -S $SRC/selections_pcc6803.R  -f png -v -W 8 -H 3
$GENBRO/src/plotFeature.R -i $GENDAT/pcc6803 --coor "1,845951:817768" -s tu -S $SRC/selections_pcc6803.R  -f png -v -W 8 -H 3
$GENBRO/src/plotFeature.R -i $GENDAT/pcc6803 --coor "1,845951:807768" -s tu -S $SRC/selections_pcc6803.R  -f png -v -W 8 -H 3
$GENBRO/src/plotFeature.R -i $GENDAT/pcc6803 --coor "1,865219:850385" -s tu -S $SRC/selections_pcc6803.R  -f png -v -W 4 -H 3
$GENBRO/src/plotFeature.R -i $GENDAT/pcc6803 --coor "1,2440648:2458675" -r 0 -F -s tu -S $SRC/selections_pcc6803.R  -f png -v -W 4 -H 3


## large view of rRNA
$GENBRO/src/plotFeature.R -i $GENDAT/pcc6803 --coor "1,3303053:3356080" -r 0 -s tu -S $SRC/selections_pcc6803.R  -f png -v -W 9 -H 3
$GENBRO/src/plotFeature.R -i $GENDAT/pcc6803 --coor "1,2421648:2475675" -r 0 -F -s tu -S $SRC/selections_pcc6803.R  -f png -v -W 9 -H 3

## direct TU selection - doesnt work well, looks for genes that overlap and centers on those
$GENBRO/src/plotFeature.R -i $GENDAT/pcc6803 -r 0 -s tu -S $SRC/selections_pcc6803.R  -f png -v -W 9 -H 3 --idcol TU_kopf14 --name TU32,TU5036 --rng 15000
$GENBRO/src/plotFeature.R -i $GENDAT/pcc6803 -r 0 -s tu -S $SRC/selections_pcc6803.R  -f png -v -W 18 -H 3 --idcol TU_kopf14 --name TU355 --rng 30000

cd -

## slim
cd   ${MYDATA}/pcc6803/figures/behle21/TU/slim/
$GENBRO/src/plotFeature.R -i $GENDAT/pcc6803 --coor "1,850385:865219" -r 0 -F --strand "-" -s tu -S $SRC/selections_pcc6803.R  -f png -v -W 3 -H 1
$GENBRO/src/plotFeature.R -i $GENDAT/pcc6803 --coor "1,824768:845951" -r 0 -F --strand "-"  -s tu -S $SRC/selections_pcc6803.R  -f png -v -W 3 -H 1



## has AT2 period
cd   ${MYDATA}/pcc6803/figures/behle21/AT2
$GENBRO/src/plotFeature.R -i $GENDAT/pcc6803 -r 0 -s tu -S $SRC/selections_pcc6803.R  -f png -v -W 9 -H 3 --name glsF --rng 15000

## strange TU
cd   ${MYDATA}/pcc6803/figures/behle21/strange
$GENBRO/src/plotFeature.R -i $GENDAT/pcc6803 -s genes -S $SRC/selections_pcc6803.R  -f png -v -W 8 -H 3  --name slr6100,sll0682 --rng 3000

## RNA Pol
cd   ${MYDATA}/pcc6803/figures/behle21/RNApol

$GENBRO/src/plotFeature.R -i $GENDAT/pcc6803 -s genes -S $SRC/selections_pcc6803.R  -f png -v -W 8 -H 3  --name rpoA,rpoB,rpoC1 --rng 5000
