#!/usr/bin/env bash

SRC=~/work/CoilHack/src

## command line script to call plotFeature.R for the yeast genome

genes=$1
selection=$2
range=$3
if [ -z "$4" ]
  then
    fig=png
else
    fig=$4 # pdf or png
fi
## TODO: defaults if not passed

## TODO: allow this as arguments?
settings=$SRC/selections_pcc7942.R
genome=$GENDAT/pcc7942
idcols=name,ID,NCBI_gene,cyanobaseID,oldID

## TODO: pass possible ID columns!

cmd="$GENBRO/src/plotFeature.R -i $genome -n $genes -I $idcols -r $range  -s $selection -S $settings  -f $fig -v"
echo $cmd
$cmd 
