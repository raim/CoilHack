## COLLATE BIOMASS MEASURES FROM BATCH EXPERIMENT, 202101

library(platexpress) #wavelength colors
library(segmenTools) #plotdev, add_alphas
options(stringsAsFactors=FALSE)

expid <- "210204_AB_endpoint"
setwd(file.path("~/work/CoilHack/experiments/batch/",expid))
fig.type <- ifelse(interactive(), "png","pdf") # "png" # 
hfig.type <- "png" # "pdf" # 

## R4 colors
## https://www.r-bloggers.com/2019/11/evaluation-of-the-new-palette-for-r/
col4 = c("#000000", "#DF536B", "#61D04F", "#2297E6",
         "#28E2E5", "#CD0BBC", "#EEC21F", "#9E9E9E")

e.nms <- c(evc=expression("EVC"),
           topakd=expression(topA^kd),
           gyra=expression(gyrA^kd),
           gyrb=expression(gyrB^kd),
           gyrab=expression(gyrAB^kd),
           topaox=expression(topA^OX))

## read sample dates
samples <- read.csv("endpoint_time-log.csv",sep=";", dec=",")
samples <- samples[samples[,1]!="",]
sdates <- strptime(paste(samples$date, samples$time),
                   format="%d.%m.%y %H:%M")

## align sample IDs - rm Exp tag, underscores
sids <- tolower(sub("Exp1", "", gsub("_", "", samples[,1])))
## fix wrong IDs
sids <- sub("evc012", "evc02", sids)
## experiments
eids <- sub("[0-9][0-9]","",sids)

## experiment colors
ecols <- col4[1:length(unique(eids))]
names(ecols) <- unique(eids)

stimes <- rep(NA, length(sdates))
names(stimes) <- sids
for ( eid in unique(eids) ) {
    stimes[sids[eids==eid]] <- difftime(sdates[eids==eid],
                                        min(sdates[eids==eid]),unit="days")
    
}
names(sdates) <- names(stimes) <- names(eids) <- sids


## read measured data, glycogen, ATP, qPCR, spectra
dfile <- "210201_All_data_endpoint.csv"
header <- read.csv(dfile, dec=",", sep=";",header=FALSE,nrow=1)
header <- unlist(header[1,])
header[1] <- "sample"
units <- read.csv(dfile, dec=",", sep=";",header=FALSE,nrow=1,
                  skip=1,check.names=FALSE, fileEncoding="iso-8859-1")
units <- unlist(units[1,])
names(units) <- header
alldata <- read.csv(dfile,sep=";", dec=",",header=FALSE,skip=2)

colnames(alldata) <- header

## aligend ID
alldata$sid <- tolower(gsub("_","", alldata[,1]))
alldata$eid <- eids[alldata$sid]

## sample time
alldata$TIME <- stimes[alldata$sid]

##OD timeseries
OD <- split(alldata[,c("sid","TIME","OD")], f=alldata$eid)

if ( interactive() ) {
    plot(stimes[alldata$sid], alldata$OD)
}

## NOTE:
## only OD & CASY for all time points
## spectra for 01 and 06
## all other only for 06
endpoint <- alldata[grep("06",alldata$sid),]

## Cell Dry Weight in mg/mL culture
cdw <- endpoint$CDW
names(cdw) <- eids[endpoint$sid]

## SORTING OF SAMPLES by CDW
e.srt <- names(sort(cdw,decreasin=TRUE))

cdw <- cdw[e.srt]

## ATP in pmol/mL
## sampling triplicates,
## each twice on one plates
atp <- endpoint[,colnames(endpoint)=="ATP"]
axp <- endpoint[,colnames(endpoint)=="ATP+ADP"]
rownames(atp) <- rownames(axp) <- eids[endpoint$sid]

atp <- atp[e.srt,]
axp <- axp[e.srt,]

## glycogen in ug/mL culture
## 2x enyzme assay/plates
## each 3x3 samples per strain
glyc <- endpoint[,colnames(endpoint)=="glycogen"]
rownames(glyc) <- eids[endpoint$sid]
glyc <- glyc[e.srt,]


## qPCR
topa <- endpoint[,grep("topA",colnames(alldata))]
gyra <- endpoint[,grep("gyrA",colnames(alldata))]
gyrb <- endpoint[,grep("gyrB",colnames(alldata))]
rownames(topa) <- rownames(gyra) <- rownames(gyrb) <-eids[endpoint$sid]

topa <- topa[e.srt,]
gyra <- gyra[e.srt,]
gyrb <- gyrb[e.srt,]

## read raw data file with rnpB and rpoA as reference genes
rnpb.file <- "220410_rnpB.csv"
rpoa.file <- "220410_rpoA.csv"
rnpb <- read.csv(rnpb.file, sep=";", fileEncoding="latin1", dec=",")
rpoa <- read.csv(rpoa.file, sep=";", fileEncoding="latin1", dec=",")

if ( interactive() ) {
    par(mfrow=c(2,3))
    boxplot(log2(rnpb$X2DDCQ.gyrA.rnpB) ~ rnpb$Sample.Strain.)
    boxplot(log2(rnpb$X2DDCQ.gyrB.rnpB) ~ rnpb$Sample.Strain.)
    boxplot(log2(rnpb$X2DDCQ.topA.rnpB) ~ rnpb$Sample.Strain.)
    boxplot(log2(rpoa$X2DDCQ.gyrA.rpoA) ~ rpoa$Sample.Strain.)
    boxplot(log2(rpoa$X2DDCQ.gyrB.rpoA) ~ rpoa$Sample.Strain.)
    boxplot(log2(rpoa$X2DDCQ.topA.rpoA) ~ rpoa$Sample.Strain.)
}

## simplify colnames
colnames(rnpb)<- gsub("\\.\\.",".",sub("X2.....","X2",
                                    gsub("Æ","D",colnames(rnpb))))
colnames(rpoa)<- gsub("\\.\\.",".",sub("X2.....","X2",
                                      gsub("Æ","D",colnames(rpoa))))

qpcr.file <- "220412_qPCR_calculations-endpoint.csv"
qpcr <- read.csv(qpcr.file, sep=";", fileEncoding="latin1", dec=",")
colnames(qpcr)<- gsub("\\.\\.",".",sub("X2.....","X2",colnames(qpcr)))

## construct tables as above for original data
topa.rnpb <- cbind.data.frame(strain=tolower(gsub(" ","",qpcr$Sample.Strain.)),
                              ddCQ=qpcr$X2ddCQ.topA.rnpB)
topa.rnpb <- do.call(rbind,split(topa.rnpb$ddCQ, f=topa.rnpb$strain))[e.srt,]
gyra.rnpb <- cbind.data.frame(strain=tolower(gsub(" ","",qpcr$Sample.Strain.)),
                              ddCQ=qpcr$X2ddCQ.gyrA.rnpB)
gyra.rnpb <- do.call(rbind,split(gyra.rnpb$ddCQ, f=gyra.rnpb$strain))[e.srt,]
gyrb.rnpb <- cbind.data.frame(strain=tolower(gsub(" ","",qpcr$Sample.Strain.)),
                              ddCQ=qpcr$X2ddCQ.gyrB.rnpB)
gyrb.rnpb <- do.call(rbind,split(gyrb.rnpb$ddCQ, f=gyrb.rnpb$strain))[e.srt,]

topa.rpoa <- cbind.data.frame(strain=tolower(gsub(" ","",qpcr$Sample.Strain.)),
                              ddCQ=qpcr$X2ddCQ.topA.rpoA)
topa.rpoa <- do.call(rbind,split(topa.rpoa$ddCQ, f=topa.rpoa$strain))[e.srt,]
gyra.rpoa <- cbind.data.frame(strain=tolower(gsub(" ","",qpcr$Sample.Strain.)),
                              ddCQ=qpcr$X2ddCQ.gyrA.rpoA)
gyra.rpoa <- do.call(rbind,split(gyra.rpoa$ddCQ, f=gyra.rpoa$strain))[e.srt,]
gyrb.rpoa <- cbind.data.frame(strain=tolower(gsub(" ","",qpcr$Sample.Strain.)),
                              ddCQ=qpcr$X2ddCQ.gyrB.rpoA)
gyrb.rpoa <- do.call(rbind,split(gyrb.rpoa$ddCQ, f=gyrb.rpoa$strain))[e.srt,]

## reference genes
rpoa.rnpb <- cbind.data.frame(strain=tolower(gsub(" ","",qpcr$Sample.Strain.)),
                              ddCQ=qpcr$X2ddCQ.rpoA.rnpB)
rpoa.rnpb <- do.call(rbind,split(rpoa.rnpb$ddCQ, f=rpoa.rnpb$strain))[e.srt,]
rnpb.rpoa <- cbind.data.frame(strain=tolower(gsub(" ","",qpcr$Sample.Strain.)),
                              ddCQ=qpcr$X2ddCQ.rnpB.rpoA)
rnpb.rpoa <- do.call(rbind,split(rnpb.rpoa$ddCQ, f=rnpb.rpoa$strain))[e.srt,]



## spectra
spc <- alldata[,names(units)=="spectra"]
spc.row <- !is.na(spc[,1])
spc <- spc[spc.row,]
spc.ids <- alldata$sid[spc.row]
spc.1 <- grep("01",spc.ids) 
spc.6 <- grep("06",spc.ids) 
spc.wl <- as.numeric(units[names(units)=="spectra"])

## normalize to OD 750
spc.nrm <- spc / spc[,spc.wl==750]

if ( interactive() )  {
    matplot(spc.wl, t(spc[spc.1,]),type="l",
            col=ecols[sub("[0-9]+","",spc.ids[spc.1])],lty=1)
    matplot(spc.wl, t(spc[spc.6,]),type="l",
            col=ecols[sub("[0-9]+","",spc.ids[spc.1])],lty=1)
    matplot(spc.wl, t(spc.nrm[spc.1,]),type="l",
            col=ecols[sub("[0-9]+","",spc.ids[spc.1])],lty=1)
}

### ATP/ADP fraction and ratio with error propagation

## error propagation for division
## z = x/y, where z, x and y are means of replicates
## sd(z)/z = sqrt((sd(x)/x)^2 + (sd(y)/y)^2)

atp.m <- apply(atp/cdw,1,mean)
atp.s <- apply(atp/cdw,1,sd)
axp.m <- apply(axp/cdw,1,mean)
axp.s <- apply(axp/cdw,1,sd)

frac <- atp.m/axp.m
frac.sd <- sqrt((atp.s/atp.m)^2 + (axp.s/axp.m)^2)*frac


## ATP/ADP ratio - error propagation
## https://oddhypothesis.blogspot.com/2015/01/easy-error-propagation-in-r.html
## get derivatives
if ( interactive() ) {
    f = z ~ x/(y-x)
    f = z ~ 1/(y/x-1)
    lapply(all.vars(f[[3]]), function(v) D(f[[3]], v))
    
    lapply(all.vars(f[[3]]), function(v) deparse(D(f[[3]], v)))
    
    sprintf('sqrt(%s)', 
            paste(
                sapply(all.vars(f[[3]]), function(v) {
                    sprintf('(d%s*(%s))^2', v, deparse(D(f[[3]], v)))
                }), 
                collapse='+'))
}

aratio <- function(x, y) {
    x <- unlist(x)
    y <- unlist(y)
    mean(x)/(mean(y)-mean(x))
}
aerr <- function(x, y) {
    x <- unlist(x)
    y <- unlist(y)
    dx <- sd(x)
    dy <- sd(y)
    x <- mean(x)
    y <- mean(y)
    #sqrt((dx*(1/(y - x) + x/(y - x)^2))^2+(dy*(-(x/(y - x)^2)))^2)
    sqrt((dy*(-(1/x/(y/x - 1)^2)))^2+(dx*(y/x^2/(y/x - 1)^2))^2)
}

## ATP/ADP ratio
ratio <- sapply(1:nrow(atp), function(i) aratio(atp[i,], axp[i,]))
## propagated standard deviation for ATP/ADP ratio
ratio.sd <- sapply(1:nrow(atp), function(i) aerr(atp[i,], axp[i,]))
names(ratio) <- names(ratio.sd) <- rownames(atp)

par(mfcol=c(3,1), mai=c(.25,.75,.1,.1))
boxplot(t(atp/cdw), ylim=c(0,1000))
boxplot(t(axp/cdw), ylim=c(0,1000),add=TRUE,border=col4[2],axes=FALSE,
        xlab=NA, ylab=NA)

plot(1:length(frac), frac, ylim=c(0,1.2),xlim=c(.5,6.5),
     pch=19, axes=FALSE, xlab=NA, ylab="ATP/(ATP+ADP)")
axis(2)
axis(1, at=1:length(frac), labels=names(frac), las=2)
arrows(x0=1:length(frac), y0=frac-frac.sd, y1=frac+frac.sd,
       angle=90, code=3, length=.1)

plot(1:length(ratio), ratio, ylim=c(0,15), xlim=c(.5,6.5),
     pch=19, axes=FALSE, xlab=NA, ylab="ATP/ADP")
axis(2)
axis(1, at=1:length(ratio), labels=names(ratio), las=2)
arrows(x0=1:length(ratio), y0=ratio-ratio.sd, y1=ratio+ratio.sd,
       angle=90, code=3, length=.1)


plotdev(paste0(expid,"_OD_CDW_calibration"), width=3.5, height=3.5, res=300,
        type=fig.type)
ode <- unlist(lapply(OD, function(x) tail(x$OD,1)))[e.srt]
par(mai=c(.5,.5,.1,.1), mgp=c(1.3,.4,0), tcl=-.25)
plot(x=ode, y=cdw[e.srt],ylim=c(0,1.7),xlim=c(0,3),
     xlab=expression(OD[750]), ylab="CDW, mg/mL",
     col=ecols[e.srt], pch=19)
fit <- lm(cdw[e.srt] ~ ode)
abline(a=coef(fit)[1], b=coef(fit)[2])
legend("topleft", e.srt, col=ecols[e.srt], pch=19)
dev.off()

plotdev(paste0(expid,"_spectra_t0_v2"), width=4, height=4, res=300,
        type=fig.type)
par(mfcol=c(2,1), mai=c(.44,.5,.16,.1), mgp=c(1.2,.3,0), tcl=-.25,yaxs="i")
matplot(spc.wl, t(spc.nrm[spc.1,]),type="l",
        ylab=expression(A/A[750]),xlab="wavelength, nm",
        col=ecols[sub("[0-9]+","",spc.ids[spc.1])],lty=c(1,1,1,2,1,1),lwd=2)
legend("top","day 0",bty="n")
axis(1, at=seq(0,1000,50), labels=FALSE)
matplot(spc.wl, t(spc.nrm[spc.6,]),type="l",
        ylab=expression(A/A[750]),xlab="wavelength, nm",
        col=ecols[sub("[0-9]+","",spc.ids[spc.1])],lty=c(1,1,1,2,1,1),lwd=2)
legend("top","day 5",bty="n")
legend("topright",e.nms[e.srt], col=ecols[e.srt],lty=c(1,1,1,1,2,1),lwd=2,
       seg.len=1, y.intersp=.85, box.col=NA, bg=NA, cex=.7)
axis(1, at=seq(0,1000,50), labels=FALSE)
dev.off()


plotdev(paste0(expid,"_qPCR"), width=4, height=4, res=300,
        type=fig.type)
par(mfcol=c(1,1), mai=c(.35,.5,.1,.1), mgp=c(1.2,.3,0), tcl=-.25,yaxs="i")
boxplot(t(log2(topa)), border=col4[4], axes=FALSE,
        ylab=expression(log[2]~"fold change"), ylim=c(-5,5.5),
        at=1:length(cdw)-.3, boxwex=.3, xlim=c(.5,length(cdw)+.5)) 
abline(h=0)
boxplot(t(log2(gyra)),add=TRUE, border=col4[2],axes=FALSE,
        at=1:length(cdw), boxwex=.3)
boxplot(t(log2(gyrb)),add=TRUE, border=col4[7],axes=FALSE,
        at=1:length(cdw)+.3, boxwex=.3)
axis(2);axis(2, at=seq(-5,5,1),labels=NA, tcl=par("tcl")/2)
axis(4,labels=NA);axis(4,at=seq(-5,5,1),labels=NA, tcl=par("tcl")/2)
Map(axis, side=1, at=1:length(cdw),
    col.axis=ecols[e.srt], labels=e.nms[e.srt], tcl=0,
    line=par("mgp")[2], col=NA)
axis(1, at=seq(0.5,length(cdw)+.5), labels=NA)
axis(3, at=seq(0.5,length(cdw)+.5), labels=NA, tcl=-par("tcl"))
arrows(x0=seq(0.5,length(cdw)+.5), y0=-1, y1=1, code=0)
legend("topleft",,c("topA","gyrA","gyrB"),
       col=col4[c(4,2,7)], lty=1, lwd=1,pch=1,
       box.col=NA, bg=NA, title="qPCR:")
box()
dev.off()

## NEW VERSION WITH rpoA vs. rnpB reference genes

## boxplot, relative position
bpos <- seq(-.25,.5,.25)-.25/2

plotdev(paste0(expid,"_qPCR_v2_rnpb"), width=4, height=4/2, res=300,
        type=fig.type)
par(mfcol=c(1,1), mai=c(.35,.5,.1,.1), mgp=c(1.2,.3,0), tcl=-.25,yaxs="i")
boxplot(t(log2(topa.rnpb)), border=col4[4], axes=FALSE,
        ylab=expression(log[2]~"fold change"), ylim=c(-5,10),
        at=1:length(cdw)+bpos[1], boxwex=.25, xlim=c(.5,length(cdw)+.5)) 
abline(h=0)
boxplot(t(log2(gyra.rnpb)),add=TRUE, border=col4[2],axes=FALSE,
        at=1:length(cdw)+bpos[2], boxwex=.25)
boxplot(t(log2(gyrb.rnpb)),add=TRUE, border=col4[7],axes=FALSE,
        at=1:length(cdw)+bpos[3], boxwex=.25)
boxplot(t(log2(rpoa.rnpb)),add=TRUE, border=col4[1],axes=FALSE,
        at=1:length(cdw)+bpos[4], boxwex=.25)
axis(2);axis(2, at=seq(-15,15,1),labels=NA, tcl=par("tcl")/2)
axis(4,labels=NA);axis(4,at=seq(-15,15,1),labels=NA, tcl=par("tcl")/2)
tmp<- Map(axis, side=1, at=1:length(cdw),
          col.axis=ecols[e.srt], labels=e.nms[e.srt], tcl=0,
          line=par("mgp")[2], col=NA)
axis(1, at=seq(0.5,length(cdw)+.5), labels=NA)
axis(3, at=seq(0.5,length(cdw)+.5), labels=NA, tcl=-par("tcl"))
arrows(x0=seq(0.5,length(cdw)+.5), y0=-1, y1=1, code=0)
legend("topleft",,c("topA","gyrA","gyrB","rpoA"),
       col=col4[c(4,2,7,1)], lty=1, lwd=1,pch=1,seg.len=.75,
       box.col=NA, bg=NA, ncol=2)#title=paste0("qPCR, ref. rnpB"))
text(3.75,8,"ref: rnpB")
box()
dev.off()

plotdev(paste0(expid,"_qPCR_v2_rpoa"), width=4, height=4/2, res=300,
        type=fig.type)
par(mfcol=c(1,1), mai=c(.35,.5,.1,.1), mgp=c(1.2,.3,0), tcl=-.25,yaxs="i")
boxplot(t(log2(topa.rpoa)), border=col4[4], axes=FALSE,
        ylab=expression(log[2]~"fold change"), ylim=c(-5,10),
        at=1:length(cdw)+bpos[1], boxwex=.25, xlim=c(.5,length(cdw)+.5)) 
abline(h=0)
boxplot(t(log2(gyra.rpoa)),add=TRUE, border=col4[2],axes=FALSE,
        at=1:length(cdw)+bpos[2], boxwex=.25)
boxplot(t(log2(gyrb.rpoa)),add=TRUE, border=col4[7],axes=FALSE,
        at=1:length(cdw)+bpos[3], boxwex=.25)
boxplot(t(log2(rnpb.rpoa)),add=TRUE, border=col4[1],axes=FALSE,
        at=1:length(cdw)+bpos[4], boxwex=.25)
axis(2);axis(2, at=seq(-15,15,1),labels=NA, tcl=par("tcl")/2)
axis(4,labels=NA);axis(4,at=seq(-15,15,1),labels=NA, tcl=par("tcl")/2)
tmp <- Map(axis, side=1, at=1:length(cdw),
           col.axis=ecols[e.srt], labels=e.nms[e.srt], tcl=0,
           line=par("mgp")[2], col=NA)
axis(1, at=seq(0.5,length(cdw)+.5), labels=NA)
axis(3, at=seq(0.5,length(cdw)+.5), labels=NA, tcl=-par("tcl"))
arrows(x0=seq(0.5,length(cdw)+.5), y0=-1, y1=1, code=0)
legend("topleft",,c("topA","gyrA","gyrB","rnpB"),
       col=col4[c(4,2,7,1)], lty=1, lwd=1,pch=1,seg.len=.75,
       box.col=NA, bg=NA, ncol=2)#title="qPCR, ref. rpoA")
text(3.75,8,"ref: rpoA")
box()
dev.off()


## barplot with error bars

if ( interactive() ) {
    library(gridExtra)
    library(ggplot2)
    plts <- list()
    for ( ref in c("rpoa","rnpb") ) {
        for ( gene in c("topa","gyra","gyrb") ) {
             id <- paste0(gene,".", ref)
             xy <- log2(get(id))
             data <- data.frame(name=rownames(xy),
                               value=apply(xy,1,mean),
                               sd=apply(xy,1,sd))
             pl <- ggplot(data, aes(x=name, y=value)) +
                 geom_bar(stat="identity",
                          fill="skyblue", alpha=0.7) +
                 geom_errorbar( aes(x=name, ymin=value-sd,
                                    ymax=value+sd), width=0.4, colour="orange",
                               alpha=0.9, size=1.3) +
                 ylim(-5,10) +
                 ggtitle(ref)+xlab(gene)+ylab(expression(log[2]~"fold change"))
             plts[[id]] <- pl
        }
    }
    grid.arrange(plts[[1]], plts[[2]], plts[[3]],
                 plts[[4]], plts[[5]], plts[[6]],
                 ncol=3,nrow=2)
}

## PLOT ALL
##plotdev(paste0(expid,"_summary"), width=2*4, height=4, res=300,
##        type=fig.type)
##par(mfcol=c(2,2), mai=c(.25,.4,.1,.5), mgp=c(1.2,.3,0), tcl=-.25,yaxs="i")

plotdev(paste0(expid,"_OD"), width=4, height=2, res=300, type=fig.type)
par(mfcol=c(1,1), mai=c(.3,.5,.1,.5), mgp=c(1.2,.3,0), tcl=-.25,yaxs="i")
plot(stimes[alldata$sid], alldata$OD, col=ecols[eids[alldata$sid]],
     xlim=c(0,5.5), ylim=c(0,max(alldata$OD)*1.05),
     ylab=expression(OD[750]), xlab=NA,
     axes=FALSE)
mtext("days", 1, par("mgp")[2], adj=1.05)
axis(1)
axis(2)
box()
for ( eid in unique(eids) ) 
    lines(OD[[eid]]$TIME, OD[[eid]]$OD, col=ecols[eid])
legend("topleft",e.nms[e.srt], col=ecols[e.srt], lty=1, pch=19,
       y.intersp=.8, box.col=NA, bg=NA)
par(new=TRUE)
plot(rep(5.5,length(cdw)), cdw, col=ecols[names(cdw)],xlim=c(0,5.5),
     pch=19,cex=1, ylim=c(0,max(cdw)*1.05), axes=FALSE, xlab=NA, ylab=NA)
#text(rep(5.5,length(cdw)), cdw, labels=e.nms[names(cdw)], pos=2)
axis(4, at=seq(.5,2,.5))
#axis(4, at=seq(0,2,.5), labels=FALSE)
#box()
axis(4, at=seq(.5,2,.1), tcl=par("tcl")/2, labels=FALSE)
mtext("CDW, mg/mL", 4, par("mgp")[1], adj=.65, cex=.8)
#box()
dev.off()

plotdev(paste0(expid,"_spectra_v2"), width=4, height=2, res=300, type=fig.type)
par(mfcol=c(1,1), mai=c(.3,.5,.1,.05), mgp=c(1.2,.3,0), tcl=-.25,yaxs="i")
##par(mai=c(.25,.4,.1,.15))
matplot(spc.wl, t(spc.nrm[spc.6,]),type="l",ylab=expression(A/A[750]),
        xlab=NA,
        col=ecols[sub("[0-9]+","",spc.ids[spc.6])],lty=c(1,1,1,2,1,1), lwd=2,
        xlim=c(400,770))#, ylim=c(0,.5))
mtext("nm", 1, par("mgp")[2], adj=.9)
##legend(x=510, y=.525,
##       e.nms[e.srt], col=ecols[e.srt], lty=1, lwd=2,
##       seg.len=.5, y.intersp=.85, x.intersp=.5, box.col=NA, bg=NA, cex=1)
axis(4, labels=FALSE)
axis(1, at=seq(0,1000,50), labels=FALSE)
dev.off()

plotdev(paste0(expid,"_glycogen"), width=4, height=2, res=300, type=fig.type)
par(mfcol=c(1,1), mai=c(.35,.5,.1,.1), mgp=c(1.2,.3,0), tcl=-.25,yaxs="i")
boxplot(t(glyc/cdw), ylim=c(0,620), at=1:length(cdw), boxwex=.5,
        ylab=expression(glycogen*","~mu*g/mg[CDW]),
        axes=FALSE, xlim=c(.5,length(cdw)+.5))
#boxplot(t(glyc), ylim=c(0,600),at=1:length(cdw)-.15, boxwex=.35,
#        add=TRUE,border=col4[2],axes=FALSE)
axis(2);axis(4,labels=NA)
Map(axis, side=1, at=1:length(cdw),
    col.axis=ecols[e.srt], labels=e.nms[e.srt], tcl=0,
    line=par("mgp")[2], col=NA)
axis(1, at=seq(0.5,length(cdw)+.5), labels=NA)
axis(3, at=seq(0.5,length(cdw)+.5), labels=NA, tcl=-par("tcl"))
legend("topleft",c("glycogen"), col=col4[c(1)], lty=1, pch=1,
       box.col=NA, bg=NA)
#legend("topleft",c(expression("glycogen,"~mu*g/mL),
#                      expression("glycogen,"~mu*g/mg[CDW])),
#       col=col4[2:1], pch=1, lty=1, box.col=NA, bg=NA)
box()
dev.off()


plotdev(paste0(expid,"_ATP"), width=4, height=2, res=300, type=fig.type)
par(mfcol=c(1,1), mai=c(.35,.5,.1,.1), mgp=c(1.2,.3,0), tcl=-.25,yaxs="i")
boxplot(t(atp/cdw), ylim=c(0,1050), at=1:length(cdw)+.15, boxwex=.35,
        ylab=expression("ATP,"~pmol/mg[CDW]),
        names=e.nms[rownames(atp)],axes=FALSE, xlim=c(.5,length(cdw)+.5))
boxplot(t(axp/cdw), ylim=c(0,1000),add=TRUE,border=col4[2],
        at=1:length(cdw)-.15, boxwex=.35, axes=FALSE)
axis(2);axis(4,labels=NA)
Map(axis, side=1, at=1:length(cdw),
    col.axis=ecols[e.srt], labels=e.nms[e.srt], tcl=0,
    line=par("mgp")[2], col=NA)
axis(1, at=seq(0.5,length(cdw)+.5), labels=NA)
axis(3, at=seq(0.5,length(cdw)+.5), labels=NA, tcl=-par("tcl"))
legend("topleft",c("ATP+ADP","ATP"), col=col4[c(2,1)], lty=1, pch=1,
       box.col=NA, bg=NA)
box()
dev.off()


## tigher plot for revision
plotdev(paste0(expid,"_glycogen_v2"),
        width=2, height=1.8, res=300, type=fig.type)
par(mfcol=c(1,1), mai=c(.11,.5,.05,.05), mgp=c(1.2,.3,0), tcl=-.25,yaxs="i")
boxplot(t(glyc/cdw)/1000, ylim=c(0,630)/1000, at=1:length(cdw), boxwex=1,
        ylab=expression(glycogen*","~g/g[CDW]), cex=.5,lwd=1.5,
        axes=FALSE, xlim=c(.5,length(cdw)+.5), border=ecols[e.srt],
        whisklty=1)
axis(2);axis(4,labels=NA)
axis(1, at=seq(0.5,length(cdw)), labels=NA)
axis(3, at=seq(0.5,length(cdw)), labels=NA, tcl=-par("tcl"))
box()
dev.off()

plotdev(paste0(expid,"_ATP_v2"), width=2, height=1.8, res=300, type=fig.type)
par(mfcol=c(1,1), mai=c(.11,.5,.05,.05), mgp=c(1.2,.3,0), tcl=-.25,yaxs="i")
boxplot(t(atp/cdw)/1000, ylim=c(0,1050)/1000, at=1:length(cdw)+.2, boxwex=.5,
        ylab=expression("ATP,"~nmol/g[CDW]), border=ecols[e.srt],
        pch=1, cex=.5,whisklty=1,
        names=e.nms[rownames(atp)],axes=FALSE, xlim=c(.5,length(cdw)+.5))
boxplot(t(axp/cdw)/1000, ylim=c(0,1000)/1000,add=TRUE,col=ecols[e.srt],
        at=1:length(cdw)-.2, boxwex=.5, axes=FALSE, pch=19, cex=.5,whisklty=1)
axis(2);axis(4,labels=NA)
axis(1, at=seq(0.5,length(cdw)+.5), labels=NA)
axis(3, at=seq(0.5,length(cdw)+.5), labels=NA, tcl=-par("tcl"))
legend("topleft",c("ATP+ADP","ATP"), col=1, pch=c(15,22),
       box.col=NA, bg=NA, x.intersp=.5)
box()
dev.off()


## read CASY DATA and plot on the fly - TODO: plot below
casyl <- casys <- list()
for ( i in 1:length(e.nms) ) {

    eid <- names(e.nms)[i]
    
    fid <- file.path("analysis",paste0(expid,"_CASY_",toupper(eid),".tsv"))
    casy <- read.delim(fid)
    
    diax <- casy$diameter
    volx <- casy$volume
    counts <- casy[,-(1:2)]
    
    ## get SAMPLE IDs
    cids <- tolower(sub("_","",sub("EXP1_","",colnames(counts))))
    ## fix wrong id in gyrAB
    cids <- sub("06_02","06",cids)
    
    if ( any(!cids%in%names(stimes) ) )
        cat(paste(eid, "samples not found\n"))

    colnames(counts) <- cids
    casyl[[eid]] <- cbind(diameter=diax,volume=volx,counts)

    ## casy summary!
    fid <- file.path("analysis",paste0(expid,"_CASY_",toupper(eid),
                                       "_summary.tsv"))
    casy <- read.delim(fid, check.names=FALSE)
    cids <- tolower(sub("_","",sub("EXP1_","",casy$sample)))
    ## fix wrong id in gyrAB
    cids <- sub("06_02","06",cids)
    casy$sample <- cids
    casys[[eid]] <- casy
    
   
}

### PLOT CASY
## SIZE FILTER for distribution plot
min.diam <- 1.25
max.diam <- 5
## colors and breaks
cols <- viridis::viridis(100)
cols <- grey.colors(100,start=1,end=0)
brks <- seq(min.diam,max.diam,length.out=101)

## summary: all volume distributions, total counts and volume
plotdev(paste0(expid,"_casy"),
        width=4, height=4, res=300, type=hfig.type)
par(mai=c(.05,.05,.05,.05), mgp=c(1.2,.3,0), tcl=-.25,yaxs="i")
layout(cbind(rep(7,4),rbind(1:2,3:4,5:6,7:8),rep(7,4)),
       widths=c(.2,1,1,.2), heights=c(1,1,1,.2))
for ( i in 1:length(e.nms) ) {
    
    eid <- names(e.nms)[i]
    casy <- casyl[[eid]]
        
    diax <- casy$diameter
    volx <- casy$volume
    counts <- casy[,-(1:2)]

    filter <- diax>min.diam & diax<max.diam
    counts <- counts[filter,]
    diax <- diax[filter]
    volx <- volx[filter]

    cids <- colnames(counts)

    ## smooth
    counts <- apply(counts,2, function(x) ma(x, 15))
    counts[is.na(counts)] <- 0
    
    ## normalize per column: relative frequency
    counts <- apply(counts,2, function(x) x/max(x))

    if ( i%%2==0 )
        par(mai=c(.05,.05,.05,.05), xaxs="i")
    else
        par(mai=c(.05,.05,.05,.05), xaxs="i")
    
    ## plot
    image(x=stimes[cids], y=volx, z=t(counts), ylim=c(0,29),
          col=cols, breaks=seq(0,1,length.out=length(cols)+1),
          xlab=NA, ylab=NA, axes=FALSE, xaxs="i",xlim=c(-.5,5.5))
    legend("topleft", e.nms[eid], text.col=ecols[eid], cex=1.2,
           text.font=2, box.col=NA, bg=NA, seg.len=0, x.intersp=0)
    #axis(4, labels=FALSE)
    axis(3, labels=FALSE)
    axis(2, labels=i%%2!=0, las=2)
    axis(1, labels=i>4)
    if(i==1)
        legend(x=-.5, y=23, c("cell count","total volume"),
               lty=2:1,pch=c(19,1),bty="n")
    if(i==3) mtext(expression("cell volume"~V[cell]*","~ fL), 2,
                   par("mgp")[1]*.95)
    ##if(i==6) mtext("days", 1, par("mgp")[1]*1.1, adj=-.2,xpd=TRUE)
    if(i==6) mtext("time, d", 1, par("mgp")[1]*1.1, adj=-.2,xpd=TRUE)
    box()

    yn <- casys[[eid]][,"cells/mL"]/1e8/2
    yv <- casys[[eid]][,"volume,uL/mL"]
    par(new=TRUE,xaxs="i")
    plot(stimes[cids], yn, col=ecols[eid],lty=2,
         ylim=c(0,6), type="l",xlim=c(-.5,5.5),axes=FALSE,ylab=NA,xlab=NA)
    points(stimes[cids], yn, col=ecols[eid], pch=19)
    lines(stimes[cids], yv, col=ecols[eid], lty=1)
    points(stimes[cids], yv, col=ecols[eid], pch=1)
    axis(4, labels=i%%2==0, las=2)
    ##axis(1)
    if(i==4)
        mtext(expression(5*"x"*10^7~cells/mL~" & "~"total volume,"~mu*L/mL), 4,
              par("mgp")[1]*1.4)
}
dev.off()


## test casy, own vs. pre.calculated values

png("casy_comparison.png", width=7, height=7, res=300, units="in")
par(mfrow=c(3,2), mai=c(.5,.5,.1,.1), mgp=c(1.3,.4,0))
for ( i in 1:length(casys) ) {
    plot(casys[[i]][,"volume,uL/mL"],
         casys[[i]][,"Volume/ml"]/1e9,
         xlim=c(0,3),ylim=c(0,3))
    abline(a=0, b=1)
}
dev.off()

png("casy_summary.png", width=6, height=4, res=300, units="in")
par(mfrow=c(3,2), mai=c(.5,.5,.1,.1), mgp=c(1.3,.4,0))
for ( i in 1:length(casys) ) {
    x <- stimes[casys[[i]]$sample]

    y <- casys[[i]][,"cells/mL"]/1e8
    ##y <- casys[[i]][,"Counts/ml"]/1e8
    plot(x, y, ylim=c(0,8), type="l")

    y <- casys[[i]][,"volume,uL/mL"]
    ##y <- casys[[i]][,"Volume/ml"]/1e9
    par(new=TRUE)
    plot(x, y, ylim=c(0,4), type="l", axes=FALSE,col=2)
    axis(4)
    legend("topleft",names(casys)[i])
}
legend("right", c("1e8 cells/mL", "total volume, uL/mL"),
       col=1:2, lty=1)
dev.off()
