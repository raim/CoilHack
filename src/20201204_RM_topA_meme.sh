
## using online meme, 20210421, all_0 clustering

clid=all_0
COILHACK=~/work/CoilHack
reactorid=20201204_RM_topA
RNASEQ=$COILHACK/experiments/RNAseq/$reactorid/
MEME=$RNASEQ/analysis/$clid/motifs
mkdir $MEME

cd $MEME

### ONLINE

## "classic"
#meme cluster_2.fasta -dna -oc . -nostatus -time 14400 -mod zoops -nmotifs 3 -minw 6 -maxw 50 -objfun classic -revcomp -markov_order 0

wget https://meme-suite.org/meme//opal-jobs/appMEME_5.3.316190128805381993400886/meme.txt -O $MEME/online/cluster_1_meme.txt
wget https://meme-suite.org/meme//opal-jobs/appMEME_5.3.316190114584191598007492/meme.txt -O $MEME/online/cluster_2_meme.txt
wget https://meme-suite.org/meme//opal-jobs/appMEME_5.3.31619012908251-1442516496/meme.txt -O $MEME/online/cluster_3_meme.txt
wget https://meme-suite.org/meme//opal-jobs/appMEME_5.3.31619012929567-1919451005/meme.txt -O $MEME/online/cluster_4_meme.txt
wget https://meme-suite.org/meme//opal-jobs/appMEME_5.3.31619012955059549068993/meme.txt -O $MEME/online/cluster_5_meme.txt
wget https://meme-suite.org/meme//opal-jobs/appMEME_5.3.31619013643344-68329465/meme.txt -O $MEME/online/cluster_6_meme.txt

wget https://meme-suite.org/meme//opal-jobs/appMEME_5.3.316190128805381993400886/meme.html -O $MEME/online/cluster_1_meme.html
wget https://meme-suite.org/meme//opal-jobs/appMEME_5.3.316190114584191598007492/meme.html -O $MEME/online/cluster_2_meme.html
wget https://meme-suite.org/meme//opal-jobs/appMEME_5.3.31619012908251-1442516496/meme.html -O $MEME/online/cluster_3_meme.html
wget https://meme-suite.org/meme//opal-jobs/appMEME_5.3.31619012929567-1919451005/meme.html -O $MEME/online/cluster_4_meme.html
wget https://meme-suite.org/meme//opal-jobs/appMEME_5.3.31619012955059549068993/meme.html -O $MEME/online/cluster_5_meme.html
wget https://meme-suite.org/meme//opal-jobs/appMEME_5.3.31619013643344-68329465/meme.html -O $MEME/online/cluster_6_meme.html


## "differential enrichment"
# meme cluster_2.fasta -dna -oc . -nostatus -time 14400 -mod zoops -nmotifs 3 -minw 6 -maxw 50 -objfun de -neg cluster_3.fasta -revcomp -markov_order 0
wget https://meme-suite.org/meme//opal-jobs/appMEME_5.3.31619011083798203520047/meme.txt -O $MEME/online/cluster_2_vs_3_meme.txt
wget https://meme-suite.org/meme//opal-jobs/appMEME_5.3.31619011083798203520047/meme.xml -O $MEME/online/cluster_2_vs_3_meme.xml
wget https://meme-suite.org/meme//opal-jobs/appMEME_5.3.31619011083798203520047/mast.txt -O $MEME/online/cluster_2_vs_3_mast.txt
wget https://meme-suite.org/meme//opal-jobs/appMEME_5.3.31619011083798203520047/mast.xml -O $MEME/online/cluster_2_vs_3_mast.xml

### LOCAL - version 5.4.1 (directory old: 5.3.3) 

cd $MEME
export PATH=$HOME/meme/bin:$HOME/meme/libexec/meme-5.4.1:$PATH
for fasta in `ls cluster*fasta`; do
    odir=${fasta%.fasta}
    echo $fasta $odir
    mkdir  $odir
    meme $fasta -dna -oc . -nostatus -time 14400 -mod zoops  -minw 6 -maxw 50 -objfun classic -revcomp -markov_order 0  -nmotifs 5 -o $odir &
done

