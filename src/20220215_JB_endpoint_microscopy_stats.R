
## cell detection from  microscopy data Jonas Burmester,
## IDS: 20220125_JB and  20220215_JB

## TODO:
## * use manually selected cells from ImageJ/ObjectJ
## * do background correction for fluor channels


library(png)
library(segmenTools) 
library(EBImage) 
#library(RBioFormats)
library(viridis)

## doesnt really works, requires to specify z=
imagem <- function(col=viridis::viridis(100), ...) 
    image_matrix(...,col=col)
plotit <- function(tif1, tif2, bw, data) {

    par(mai=c(.5,.5,.5,.5),xpd=TRUE)
    nr <- nrow(bw)
    nc <- ncol(bw)
    imagem(z=tif1,xlim=c(0,nc),ylim=c(0,nr))

    imagem(z=bw,col=c("white","black"),xlim=c(0,nc),ylim=c(0,nr))
    axis(1);axis(2)
    points(data$x, nrow(bw)-data$y, col=2, pch=4, cex=.5)
    text(data$x, nrow(bw)-data$y, labels=data$ID,col=2, pos=4)
    
    imagem(z=tif2,xlim=c(0,nc),ylim=c(0,nr), col=grey.colors(100))
    points(data$x, nrow(bw)-data$y, col=2, pch=4, cex=.5)
    text(data$x, nrow(bw)-data$y, labels=data$ID,col="white", pos=4)
    
    par(mai=c(1,1,.1,.1))
    plot(data$c3, data$c2,ylim=c(0,max(data$c2)),xlim=c(0,max(data$c3)),
         xlab="channel 3", ylab="channel 2")
    plot(data$c3, data$c1,ylim=c(0,max(data$c1)),xlim=c(0,max(data$c3)),
         xlab="channel 3", ylab="channel 1")
}

expid <- "20220215_JB"
epath <- file.path("~/work/CoilHack/experiments/",expid)
fig.path <- file.path(epath,"images")
dir.create(fig.path, showWarnings=FALSE)

## R4 colors
## https://www.r-bloggers.com/2019/11/evaluation-of-the-new-palette-for-r/
col4 = c("#000000", "#DF536B", "#61D04F", "#2297E6",
         "#28E2E5", "#CD0BBC", "#EEC21F", "#9E9E9E")

## STRAINS
strains <- c("EVC","gyrA","topA")
expl <- strains
expl[strains=="EVC"] <- expression(EVC)
expl[strains=="topA"] <- expression(topA^OX)
expl[strains=="gyrA"] <- expression(gyrA^kd)
names(expl) <- strains
## experiment colors
ecols <- col4[c(1:2,6)]
names(ecols) <- unique(strains)

## PARAMETERS
CHANNEL <- 3
MINPIXEL <- 30
THRESH <- 0.03

## COUNT CELLS from TIF
setwd(fig.path)

## DNA/PI/Chlorophyll/Durchlicht in .oir file

mfiles <- c("topA+_5_stained.tif", "EVC-_5_stained.tif")

for ( mfile in mfiles ) {
tif <- readImage(mfile)

## inspect color stacks
if ( interactive() ) {
    imagem(z=tif[,,4][])
    imagem(z=tif[,,3][])
    imagem(z=tif[,,2][])
    imagem(z=tif[,,1][])

    ##display(tif, method="raster") #just black?
}

## select one color channel and determine appropriate threshold
imagem(z=tif[,,CHANNEL][])
channel <- tif[,,CHANNEL][] # chlorophyll channel ?

hist(c(channel),xlim=c(0,max(channel)))
abline(v=THRESH,col=2)

## generate b/w image
bw <- channel
bw[] <- 0
bw[channel>THRESH] <- 1
if ( interactive() ) 
    imagem(z=bw, col=c("white","black"))

## label cells - continuous areas
mask <- bwlabel(bw)

## cut "cells with less than 50 pixels
cellids <- table(c(mask))
cellids <- as.numeric(names(cellids[cellids>=MINPIXEL]))

## get statistics for each cell
pixels <- sapply(cellids, function(x) sum(mask==x))
data <- cbind.data.frame(
    ID=cellids,
    y=sapply(cellids, function(x) which.max(apply(mask==x, 1, sum))),
    x=sapply(cellids, function(x) which.max(apply(mask==x, 2, sum))),
    pixels=pixels,
    c1=sapply(cellids, function(x) sum(tif[,,1][mask==x]))/pixels,
    c2=sapply(cellids, function(x) sum(tif[,,2][mask==x]))/pixels,
    c3=sapply(cellids, function(x) sum(tif[,,3][mask==x]))/pixels,
    c4=sapply(cellids, function(x) sum(tif[,,4][mask==x]))/pixels)

## split off background
bg <-data[data$ID==0,]
data <-data[data$ID!=0,]

pdf(sub("\\.tif","_manual.pdf", mfile))
plotit(tif1=tif[,,CHANNEL][], tif2=tif[,,4], bw=bw, data=data)
dev.off()

## USE EBImage tutorial steps

bw <- thresh(tif[,,CHANNEL], w=10, h=10, offset=0.003)
if ( interactive() ) {
    imagem(z=bw)
    display(bw, method="raster")
}

bw <- opening(bw, makeBrush(5, shape='disc'))
if ( interactive() )
    imagem(z=bw)

bw <- fillHull(bw)
if ( interactive() )
    imagem(z=bw)

nmask <- bwlabel(bw)

## get raw matrix of cell labels
mask <- nmask[]

if ( interactive() )
    imagem(z=mask)

## cut "cells with less than 50 pixels
cellids <- table(c(mask))
cellids <- as.numeric(names(cellids[cellids>=MINPIXEL]))

pixels <- sapply(cellids, function(x) sum(mask==x))
data <- cbind.data.frame(
    ID=cellids,
    y=sapply(cellids, function(x) which.max(apply(mask==x, 1, sum))),
    x=sapply(cellids, function(x) which.max(apply(mask==x, 2, sum))),
    pixels=pixels,
    c1=sapply(cellids, function(x) sum(tif[,,1][mask==x]))/pixels,
    c2=sapply(cellids, function(x) sum(tif[,,2][mask==x]))/pixels,
    c3=sapply(cellids, function(x) sum(tif[,,3][mask==x]))/pixels,
    c4=sapply(cellids, function(x) sum(tif[,,4][mask==x]))/pixels)

## split off background
bg <-data[data$ID==0,]
data <-data[data$ID!=0,]

pdf(sub("\\.tif","_ebi.pdf", mfile))
plotit(tif1=tif[,,CHANNEL][], tif2=tif[,,4], bw=bw, data=data)
dev.off()

par(mai=c(.5,.5,.05,.05),xpd=TRUE)
nr <- nrow(bw)
nc <- ncol(bw)
imagem(z=bw,col=c("white","black"),xlim=c(0,nc),ylim=c(0,nr))
axis(1);axis(2)
points(data$x, nrow(bw)-data$y, col=2, pch=4)
text(data$x, nrow(bw)-data$y, labels=data$ID,col=2, pos=4)

plot(data$c3, data$c2,ylim=c(0,max(data$c2)),xlim=c(0,max(data$c3)),
     xlab="channel 3", ylab="channel 2")
plot(data$c3, data$c1,ylim=c(0,max(data$c1)),xlim=c(0,max(data$c3)),
     xlab="channel 3", ylab="channel 1")

imagem(z=tif[,,4][],xlim=c(0,nc),ylim=c(0,nr))
text(data$x, nrow(bw)-data$y, labels=data$ID,col=2, pos=4)

}
