
## runs all scripts required for Behle et al. 2021, topA Overexpression


### TODO:
## check lehmann14 data, 5 vs 11,
## RNA-seq:
##-* CLUSTER Transcription Units by Kopf et al. using k-means with gene clusters
##           as centers
## * GENOME STRUCTURE: bendability, params from Kim et al. 2018, files
##                     in genomeData2 (???),
##                     see $TATADIR/perls/calculateChromatinStructure.pl
##          WAVELET: ~nucleosome-range period of AT2 period!?
##          RATIO: log2(AT2_11.3/AT2_3) bp
## * investigate more info about RNAseq processing, see TODO.md
##     why are some missing? how were chrom start/end features handled?
##     how were multiple mapping loci handled?
## * duplicates still mapped wrongly in RNAseq, eg. ppnK
## * check Inf/-Inf/NA in nrm0 and raw data
##   -> especially see highly-expressed plasmid genes
## * check very high log2 values in latest mapping of batch-seq
##-* HANDLE DUPLICATED crossing chromosome start index, eg. SGL_RS01875 !
##-* TWO LOCI (SGL_RS12025 & SGL_RS01730) missing from batch but not timeseries,
##--> SGL_RS12025/slr0400 identical to SGL_RS09345 (sll1415) duplicate in batch 
## -load batch-seq data here and add to cluster comparison plot!
## -mv cell density/rates to supplement, and move GO/ and timeseries to fig. 4
## -GC/AT promoter profiles
## -get newer GO slim, eg. via protein database?  used for zavrel et al.?
## -analyse topA, gyrA, gyrA2, gyrB,
## -analyse plasmid expression,
## -analyse pSNDY expression.
## -analyze cluster overlap with plasmids
## -CASY RECOVERY of batch cultures
## redo CASY with correct dilutions!
## RNA/DNA/plasmid extraction data
## CQ GEL analysis, incl. old diurnal gel
##-GC-skew at GC/AT along genome
## CASY: check manual diameter vs. volume, correction factors
## reactor: plot light data, load recovered scale and gas sensor data!
## Chl/PC spectra timecourse & digitize @Barthel2013
## period analysis of OD4, and of RNAseq?
## cyano-express data
## Escher maps
## separate analysis of initial response
## ....

LABDATA=/mnt/synmibi/Studierende/DATA/ # exon (in synmibi net)
LABDATA=${MYDATA}/synmibi/ # hoel (in qtb net)
if [ `uname -n` = "intron" ]; then
    LABDATA=/data/synmibi/ # intron
fi    

DIGILAB=~/programs/synmibi_digilab
batchid=20200602_WM_coilhack_endpoint
reactorid=20201204_RM_topA
COILHACK=~/work/CoilHack
COILDATA=~/work/CoilHack/experiments/ # TODO: move this out of git
reactorrun2=$COILDATA/reactor/pcc6803/$reactorid/
batch=$COILDATA/batch/
batchrun=$batch/$batchid/
BATCHSEQ=$COILDATA/RNAseq/$batchid/
RNASEQ=$COILDATA/RNAseq/$reactorid/
## old CQ gels
TATADIR=/home/raim/programs/tataProject
##genome browser & data
GENBRO=/home/raim/programs/genomeBrowser/
PCC6803DAT=${MYDATA}/pcc6803/




### RUN ALL ANALYSES

### REACTOR DATA
## plot and collect reactor (online) data
R --vanilla < $COILHACK/src/20201204_RM_topA_reactor.R

## CASY: plot and collect CASY data
## reactor timeseries
R --vanilla < $DIGILAB/scripts/20201204_RM_topA_CASY.R

## SPECORD: plot and collect Spectra
R --vanilla < $DIGILAB/scripts/20201204_RM_topA_Specord.R

## COLLATE BiOMASS DATA: GLYCOGEN, ATP, SPECTRA, CASY
## paper figures!
cp -a $LABDATA/RNAseq/Endpoint_timeseries_RNA_concentration.csv $reactorrun2/offline/
R --vanilla < $COILHACK/src/20201204_RM_topA_biomass.R

### BATCH DATA
## all strain batch timeseries
R --vanilla < $DIGILAB/scripts/20210104_MD_coilhack_CASYrun.R

## BATCH ENDPOINT 2 - CASY
## parse data and split by experiment
R --vanilla < $DIGILAB/scripts/210204_AB_endpoint_CASY.R
cp -a $LABDATA/CASY/210204_AB_endpoint/*.csv $batch/210204_AB_endpoint/
## recovery
R --vanilla < $DIGILAB/scripts/210204_AB_recovery_CASY.R
cp -a $LABDATA/CASY/210204_AB_recovery/*.csv $batch/210204_AB_recovery/
## reactor re-induction
R --vanilla < $DIGILAB/scripts/210204_AB_topA_2nd_CASY.R
cp -a $LABDATA/CASY/210204_AB_topA_2nd/*.csv $batch/210204_AB_topA_2nd/

## collect all data and generate paper figures!
R --vanilla < $COILHACK/src/210204_AB_endpoint_biomass.R
R --vanilla < $COILHACK/src/210204_AB_recovery_casy.R
R --vanilla < $COILHACK/src/210204_AB_topA_2nd_casy.R

### RNA-seq ANALYSIS - BATCH & TIMESERIES
## NOTE: order is important, the scripts read each other's results

## extract data from files sent from Bielefeld Cebitec
$COILHACK/src/20201204_RM_topA_extract_RNAseq.sh


## Saha et al. 2016 - clustering of diurnal expression data
## TODO: find out why cluster order is messed up sometimes, intron vs. exon?
R --vanilla < $COILHACK/src/20210326_RM_saha16_clustering.R


## BATCH ENDPOINT 1 - RNAseq

## growth curves
cp -a '/mnt/synmibi/Studierende/Projects/CoilHack/Results Wandana/Examination of kd_gyrA, kd_gyrB and ox_topA/20200123_Growth_curves_Synechocystis_6803_kd_gyrA_kd_gyrB_ox_topA.csv' $BATCHSEQ/samples
R --vanilla < $COILHACK/src/20200602_WM_RNAseq_growth.R



## rRNA - Bioagilent data from Bielefeld
#cp -a $LABDATA/RNAseq/20200602_WM_coilhack_endpoint/samples/"Prokaryote Total RNA Nano_2020-07-08_004.xml" $BATCHSEQ/samples/
R --vanilla < $COILHACK/src/20200602_WM_RNAseq_rRNA.R
R --vanilla < $COILHACK/src/20200602_WM_RNAseq_rRNA_v2.R

## first overview - comparison with prakash et al., lehmann et al.
##R --vanilla < $COILDATA/RNAseq/20200602_WM_coilhack_endpoint/scripts/evaluate.R
R --vanilla < $COILHACK/src/20200602_WM_RNAseq_merge_experiments.R


## TIMESERIES - RNAseq CLUSTERING
## - genome&all plasmids - 6 clusters, supplement
sed 's/^genomes=.*/genomes="all"/;s/^FIG.*/FIG="pdf"/' $COILHACK/src/20201204_RM_topA_RNAseq.R | R --vanilla -
## - genome only - 4 clusters
#sed 's/^genomes=.*/genomes="genome"/;s/^FIG.*/FIG="png"/;s/^OTHERS.*/OTHERS=FALSE/' $COILHACK/src/20201204_RM_topA_RNAseq.R | R --vanilla -
## - plasmids only
#sed 's/^genomes=.*/genomes="plasmids"/;s/^FIG.*/FIG="pdf"/;s/^OTHERS.*/OTHERS=TRUE/' $COILHACK/src/20201204_RM_topA_RNAseq.R | R --vanilla -

# NOTE: (OBSOLETE FIGURE) REQUIRES TIME-SERIES CLUSTERS to be run before!
sed 's/^FIG.*/FIG="pdf"/' $COILHACK/src/20200602_WM_RNAseq_analyse.R | R --vanilla -

### PROMOTER STRUCTURE PROFILES OF CLUSTERS
## uses file ${MYDATA}/pcc6803/pcc6803_AT_p0_w396_s10_SNR_amplitudes.csv
## calculated by genomeBrowser/data/pcc6803/ - reproduce here w/o permutation
mkdir $PCC6803DAT/genomeData/behle21/
## AT2 motif - raw periodicity
$GENBRO/src/calculateDiNuclProfile.R --id=pcc6803 --fdir=$PCC6803DAT/chromosomes --fend=.fasta --out=$PCC6803DAT/genomeData/behle21 --win=396 --stp=10  --circular --verb --AT2 >& $PCC6803DAT/genomeData/behle21/at2_period_behle21_nosnr.log &


#sed 's/^CLID=.*/CLID="genome_0"/;s/^FIG.*/FIG="pdf"/;s/^use.tu.*/use.tu=FALSE/' $COILHACK/src/20201204_RM_topA_promoters.R| R --vanilla -
#sed 's/^CLID=.*/CLID="genome_0"/;s/^FIG.*/FIG="pdf"/;s/^use.tu.*/use.tu=TRUE/' $COILHACK/src/20201204_RM_topA_promoters.R| R --vanilla -

##  MOTIFS IN DIFFERENTIAL RESPONSE CLUSTERS
## TSS alignment - short distance
FTYPE="pdf" # "png" #
## TSS alignment - long distance GC vs. AT
sed "s/^CLID=.*/CLID='all_0'/;s/^FIG.*/FIG='${FTYPE}'/;s/^use.tu.*/use.tu=TRUE/;s/^dst=.*/dst=1500/;s/^use=.*/use=''/" $COILHACK/src/20201204_RM_topA_promoters.R| R --vanilla -
## TSS alignment - short distance, PROMOTER FOCUS
sed "s/^CLID=.*/CLID='all_0'/;s/^FIG.*/FIG='${FTYPE}'/;s/^use.tu.*/use.tu=TRUE/;s/^dst=.*/dst=250/;s/^use=.*/use=''/" $COILHACK/src/20201204_RM_topA_promoters.R| R --vanilla -
## TSS alignment - zoom on CORE PROMOTER
sed "s/^CLID=.*/CLID='all_0'/;s/^FIG.*/FIG='${FTYPE}'/;s/^use.tu.*/use.tu=TRUE/;s/^dst=.*/dst=50/;s/^use=.*/use=''/" $COILHACK/src/20201204_RM_topA_promoters.R| R --vanilla -
## TSS alignment - zoom further - SEQUENCE LOGOS!
sed "s/^CLID=.*/CLID='all_0'/;s/^FIG.*/FIG='${FTYPE}'/;s/^use.tu.*/use.tu=TRUE/;s/^dst=.*/dst=49/;s/^use=.*/use=''/" $COILHACK/src/20201204_RM_topA_promoters.R| R --vanilla -

## MOTIFS IN IMMEDIATE RESPONSE CLUSTERS
## total GC - 
sed "s/^CLID=.*/CLID='all_0'/;s/^FIG.*/FIG='${FTYPE}'/;s/^use.tu.*/use.tu=TRUE/;s/^dst=.*/dst=1500/;s/^use=.*/use='early'/" $COILHACK/src/20201204_RM_topA_promoters.R| R --vanilla -
sed "s/^CLID=.*/CLID='all_0'/;s/^FIG.*/FIG='${FTYPE}'/;s/^use.tu.*/use.tu=TRUE/;s/^dst=.*/dst=250/;s/^use=.*/use='early'/" $COILHACK/src/20201204_RM_topA_promoters.R| R --vanilla -
sed "s/^CLID=.*/CLID='all_0'/;s/^FIG.*/FIG='${FTYPE}'/;s/^use.tu.*/use.tu=TRUE/;s/^dst=.*/dst=50/;s/^use=.*/use='early'/" $COILHACK/src/20201204_RM_topA_promoters.R| R --vanilla -
## TSS alignment - zoom further - SEQUENCE LOGOS!
sed "s/^CLID=.*/CLID='all_0'/;s/^FIG.*/FIG='${FTYPE}'/;s/^use.tu.*/use.tu=TRUE/;s/^dst=.*/dst=49/;s/^use=.*/use='early'/" $COILHACK/src/20201204_RM_topA_promoters.R| R --vanilla -

## PERIODICITY, ATG and TSS, with and without super clustering
sed 's/^CLID=.*/CLID="all_0"/;s/^FIG.*/FIG="pdf"/;s/^use.tu.*/use.tu=TRUE/;s/^ONLY2.*/ONLY2=FALSE/;s/^SUPER.*/SUPER=FALSE/' $COILHACK/src/20201204_RM_topA_periodicity.R| R --vanilla -
sed 's/^CLID=.*/CLID="all_0"/;s/^FIG.*/FIG="pdf"/;s/^use.tu.*/use.tu=TRUE/;s/^ONLY2.*/ONLY2=FALSE/;s/^SUPER.*/SUPER=TRUE/' $COILHACK/src/20201204_RM_topA_periodicity.R| R --vanilla -
sed 's/^CLID=.*/CLID="all_0"/;s/^FIG.*/FIG="pdf"/;s/^use.tu.*/use.tu=FALSE/;s/^ONLY2.*/ONLY2=FALSE/;s/^SUPER.*/SUPER=FALSE/' $COILHACK/src/20201204_RM_topA_periodicity.R| R --vanilla -

## overlap of clusters with Kopf et al. TU classes
## classes: condition of maximal expression
## TODO: difference to geneXgene overlap as in kopf14.R
## TODO: find better classification of kopf14 TUs
behle21=$RNASEQ/analysis/genome_0/20201204_RM_topA_results.tsv
b21=$RNASEQ/analysis/genome_0/20201204_RM_topA_results_genome.tsv
b21wt=$RNASEQ/analysis/genome_0/20201204_RM_topA_results_wt.tsv
grep 'genome\|Feature' $behle21 > $b21
grep -v pSNDY $behle21 > $b21wt
kopf14=/data/pcc6803/processedData/kopf14_tu.tsv
~/programs/segmenTools/scripts/segmentOverlaps.R -q $b21wt --qclass CL_behle21  -t $kopf14 --tclass Max.cond. --chrfile ${MYDATA}/pcc6803/chromosomes/pcc6803.csv -o $RNASEQ/analysis/genome_0/kopf14_all --perm 1000
## reverse q/t
~/programs/segmenTools/scripts/segmentOverlaps.R -t $b21wt --tclass CL_behle21  -q $kopf14 --qclass Max.cond. --chrfile ${MYDATA}/pcc6803/chromosomes/pcc6803.csv -o $RNASEQ/analysis/genome_0/kopf14_all_reverse --perm 1000
#~/programs/segmenTools/scripts/segmentOverlaps.R -q $b21 --qclass CL_behle21  -t $kopf14 --tclass Max.cond. --ttypcol chr --ttypes 1 --chrfile ${MYDATA}/pcc6803/chromosomes/pcc6803.csv -o $RNASEQ/analysis/genome_0/kopf14_genome --perm 1000

## genomeBrowser plots
## add cluster and colors to genomebrowser
## cyanobase annotation
R --vanilla < $GENBRO/data/pcc6803/cyanobase.R
## ncbi annotation + mappings - requires cyanobase.RData !
R --vanilla < $GENBRO/data/pcc6803/annotation.R # takes a while


### BATCH-SEQ OD CURVES
#at /mnt/synmibi/Studierende/Projects/CoilHack/Results Wandana/Examination of kd_gyrA, kd_gyrB and ox_topA/
## 20200123_Growth_curves_Synechocystis_6803_supercoiling_mutant_strains.csv
## 20200123_Growth_curves_Synechocystis_6803_kd_gyrA_kd_gyrB_ox_topA.csv
## 20200123_Growth_curves_Synechocystis_6803_with_graphs.xlsx

### NUCLEIC ACID EXTRACTION VALUES
#at /mnt/synmibi/Studierende/DATA/RNAseq/
## Endpoint_timeseries_RNA_concentration.csv
## Endpoint_timeseries_plasmid_concentration.csv

##TODO:
## RNA gel Gel 2021-01-08 17hr 25min_RNA.tif
## RIN VALUES, rRNA REMOVAL etc from Bielefeld

### CQ-gels: collect gel data
##210111_CQ_time-series.tiff
##20201106_CQgel_1-8_nicht_induziert.tif
##20210127_endpoint_CQ.tif
cp -a $LABDATA/CQgels/210111_CQ_time-series.tiff $COILDATA/CQgels/
cp -a $LABDATA/CQgels/20210127_endpoint_CQ.tif $COILDATA/CQgels/
cp -a $LABDATA/CQgels/"Gel 2020-08-12 15hr 30min_endpoint_samples.tif" $COILDATA/CQgels/
cp -a $LABDATA/CQgels/20201106_CQgel_1-8_nicht_induziert.tif $COILDATA/CQgels/

## MANUAL: use ImageJ to get electropherograms, see src/20210507_RM_CQgels.md

## plot
#$COILHACK/src/20210507_RM_CQgels.R --par $COILHACK/src/20210127_endpoint_CQ_params_upper.R
$COILHACK/src/20210507_RM_CQgels.R --par $COILHACK/src/20210127_endpoint_CQ_params.R
$COILHACK/src/20210507_RM_CQgels.R --par $COILHACK/src/210111_CQ_time-series_params.R 
$COILHACK/src/20210507_RM_CQgels.R --par $COILHACK/src/210111_CQ_time-series_params_upper.R 
$COILHACK/src/20210507_RM_CQgels.R --par $COILHACK/src/210111_CQ_time-series_params_lower.R

## growth curve, plasmid yields, and final ODs for CQ experiments
cp -a $LABDATA/RNAseq/Endpoint_timeseries_plasmid_concentration.csv $COILHACK/experiments/CQgels/210111_CQ_time-series_growth.csv
R --vanilla < $COILHACK/src/210111_CQ_time-series_growth.R

## 2013 DIURNAL CQ GELS

## call old script that evaluates all gels and generates plots
$TATADIR/cyano/scripts/evaluateAll_201307.sh
## copy original gels
cp -a '/home/raim/work/cyanoSampling/sc/gels/analyzed/20130618_1511/20130618_1511.tif' $COILDATA/CQgels/
cp -a '/home/raim/work/cyanoSampling/sc/blots/Bio-Rad 2013-08-21 15hr 33min_Exposure_192.0sec.tif' $COILDATA/CQgels/
cp -a '/home/raim/work/cyanoSampling/sc/blots/pCA Syncy 20.06. CQ1.tif' $COILDATA/CQgels/

## FLOW CYTOMETRY
## same samples as for endpoint sequencing!
cp -a $LABDATA/FACS/Cyano_Syto9_003 $COILHACK/experiments/flowcytometry/20200226_AB/
R --vanilla < $COILHACK/src/20200226_AB_flowcytometry.R


## copy results to synmibi server

sudo cp -a $reactorrun2/offline/20201204_RM_topA_casy_summary.tsv $LABDATA/Lambda/$reactorid/offline/
sudo cp -a $reactorrun2/offline/20201204_RM_topA_casy.tsv $LABDATA/Lambda/$reactorid/offline/

## JONAS BURMESTER experiments for revision 202202

## TODO: cell statistics from microscopy images
##R --vanilla < $COILHACK/src/20220215_JB_microscopy_stats.R
## annotate images
R --vanilla < $COILHACK/src/20220215_JB_microscopy.R
## collect casy data
R --vanilla < $COILHACK/src/20220125_JB_endpoint_CASY.R
R --vanilla < $COILHACK/src/20220215_JB_endpoint_CASY.R
## plot all data
R --vanilla < $COILHACK/src/20220215_JB_endpoint.R




## rRNA ANALYSIS

## get Kopf et al. TUs with ribosomal proteins
grep "ribosomal protein" ${MYDATA}/pcc6803/processedData/kopf14_tu.tsv | cut -f 2 |sort|uniq
