## COLLATE BIOMASS MEASURES FROM BATCH EXPERIMENT, 202101

library(platexpress) #wavelength colors
library(segmenTools) #plotdev, add_alphas
options(stringsAsFactors=FALSE)

expid <- "210204_AB_recovery"
setwd(file.path("~/work/CoilHack/experiments/batch/",expid))
fig.type <- "png" # "pdf" # 
hfig.type <- "png" # "pdf" # 

## R4 colors
## https://www.r-bloggers.com/2019/11/evaluation-of-the-new-palette-for-r/
col4 = c("#000000", "#DF536B", "#61D04F", "#2297E6",
         "#28E2E5", "#CD0BBC", "#EEC21F", "#9E9E9E")

e.nms <- c(evc=expression("EVC"),
           topakd=expression(topA^kd),
           gyra=expression(gyrA^kd),
           gyrb=expression(gyrB^kd),
           gyrab=expression(gyrAB^kd),
           topaox=expression(topA^OX))

## read sample dates
samples <- read.csv("recovery_time-log.csv",sep=";", dec=",")
samples <- samples[samples[,1]!="",]
sdates <- strptime(paste(samples$date, samples$time),
                   format="%d.%m.%y %H:%M")

## align sample IDs - rm Exp tag, underscores
sids <- tolower(sub("Exp2", "", gsub("_", "", samples[,1])))
## fix wrong IDs
sids <- sub("evc012", "evc02", sids)
## experiments
eids <- sub("[0-9][0-9]","",sids)

## experiment colors
ecols <- col4[1:length(unique(eids))]
names(ecols) <- unique(eids)

stimes <- rep(NA, length(sdates))
names(stimes) <- sids
for ( eid in unique(eids) ) {
    stimes[sids[eids==eid]] <- difftime(sdates[eids==eid],
                                        min(sdates[eids==eid]),unit="days")
    
}
names(sdates) <- names(stimes) <- names(eids) <- sids



## read CASY DATA and plot on the fly - TODO: plot below
casyl <- casys <- list()
for ( i in 1:length(e.nms) ) {

    eid <- names(e.nms)[i]
    
    fid <- file.path("analysis",paste0(expid,"_CASY_",toupper(eid),".tsv"))
    casy <- read.delim(fid)
    
    diax <- casy$diameter
    volx <- casy$volume
    counts <- casy[,-(1:2)]
    
    ## get SAMPLE IDs
    cids <- tolower(sub("_","",sub("EXP2_","",colnames(counts))))
    
    
    if ( any(!cids%in%names(stimes) ) )
        cat(paste(eid, "samples not found\n"))

    colnames(counts) <- cids
    casyl[[eid]] <- cbind(diameter=diax,volume=volx,counts)

    ## casy summary!
    fid <- file.path("analysis",paste0(expid,"_CASY_",toupper(eid),
                                       "_summary.tsv"))
    casy <- read.delim(fid, check.names=FALSE)
    cids <- tolower(sub("_","",sub("EXP2_","",casy$sample)))
    casy$sample <- cids
    casys[[eid]] <- casy
    
   
}

### PLOT CASY
## SIZE FILTER for distribution plot
min.diam <- 1.25
max.diam <- 5
## colors and breaks
cols <- viridis::viridis(100)
cols <- grey.colors(100,start=1,end=0)
brks <- seq(min.diam,max.diam,length.out=101)

## summary: all volume distributions, total counts and volume
plotdev(paste0(expid,"_casy"),
        width=4, height=4, res=300, type=hfig.type)
par(mai=c(.05,.05,.05,.05), mgp=c(1.2,.3,0), tcl=-.25,yaxs="i")
layout(cbind(rep(7,4),rbind(1:2,3:4,5:6,7:8),rep(7,4)),
       widths=c(.2,1,1,.2), heights=c(1,1,1,.2))
for ( i in 1:length(e.nms) ) {
    
    eid <- names(e.nms)[i]
    casy <- casyl[[eid]]
        
    diax <- casy$diameter
    volx <- casy$volume
    counts <- casy[,-(1:2)]

    filter <- diax>min.diam & diax<max.diam
    counts <- counts[filter,]
    diax <- diax[filter]
    volx <- volx[filter]

    cids <- colnames(counts)

    ## smooth
    counts <- apply(counts,2, function(x) ma(x, 15))
    counts[is.na(counts)] <- 0
    
    ## normalize per column: relative frequency
    counts <- apply(counts,2, function(x) x/max(x))

    if ( i%%2==0 )
        par(mai=c(.05,.05,.05,.05), xaxs="i")
    else
        par(mai=c(.05,.05,.05,.05), xaxs="i")
    
    ## plot
    image(x=stimes[cids], y=volx, z=t(counts), ylim=c(0,29),
          col=cols, breaks=seq(0,1,length.out=length(cols)+1),
          xlab=NA, ylab=NA, axes=FALSE, xaxs="i",xlim=c(-.5,14.5))
    legend("topleft", e.nms[eid], text.col=ecols[eid], cex=1.2,
           text.font=2, box.col=NA, bg=NA, seg.len=0, x.intersp=0)
    #axis(4, labels=FALSE)
    axis(3, labels=FALSE)
    axis(2, labels=i%%2!=0, las=2)
    axis(1, labels=i>4)
    if(i==3) mtext(expression("cell volume"~V[cell]*","~ fL), 2,
                   par("mgp")[1]*.95)
    if(i==6) mtext("time, d", 1, par("mgp")[1]*1.1, adj=-.2,xpd=TRUE)
    box()

    if ( FALSE ) {
        if(i==1)
            legend(x=-.5, y=23, c("cell count","total volume"),
                   lty=2:1,pch=c(19,1),bty="n")
        yn <- casys[[eid]][,"cells/mL"]/1e8/2
        yv <- casys[[eid]][,"volume,uL/mL"]
        par(new=TRUE,xaxs="i")
        plot(stimes[cids], yn, col=ecols[eid],lty=2,
             ylim=c(0,4), type="l",xlim=c(-.5,14.5),axes=FALSE,ylab=NA,xlab=NA)
        points(stimes[cids], yn, col=ecols[eid], pch=19)
        lines(stimes[cids], yv, col=ecols[eid], lty=1)
        points(stimes[cids], yv, col=ecols[eid], pch=1)
        axis(4, labels=i%%2==0, las=2)
        ##axis(1)
        if(i==4)
        mtext(expression(5*"x"*10^7~cells/mL~" & "~"total volume,"~mu*L/mL), 4,
              par("mgp")[1]*1.4)
    }
}
dev.off()


## test casy, own vs. pre.calculated values

png("casy_comparison.png", width=7, height=7, res=300, units="in")
par(mfrow=c(3,2), mai=c(.5,.5,.1,.1), mgp=c(1.3,.4,0))
for ( i in 1:length(casys) ) {
    plot(casys[[i]][,"volume,uL/mL"],
         casys[[i]][,"Volume/ml"]/1e9,
         xlim=c(0,3),ylim=c(0,3))
    abline(a=0, b=1)
}
dev.off()

png("casy_summary.png", width=7, height=7, res=300, units="in")
par(mfrow=c(3,2), mai=c(.5,.5,.1,.1), mgp=c(1.3,.4,0))
for ( i in 1:length(casys) ) {
    x <- stimes[casys[[i]]$sample]

    y <- casys[[i]][,"cells/mL"]/1e8
    ##y <- casys[[i]][,"Counts/ml"]/1e8
    plot(x, y, ylim=c(0,8), type="l")

    y <- casys[[i]][,"volume,uL/mL"]
    ##y <- casys[[i]][,"Volume/ml"]/1e9
    par(new=TRUE)
    plot(x, y, ylim=c(0,4), type="l", axes=FALSE,col=2)
    axis(4)
    legend("topleft",names(casys)[i])
}
legend("right", c("1e8 cells/mL", "total volume, uL/mL"),
       col=1:2, lty=1)
dev.off()
