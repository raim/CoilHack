#!/bin/bash

SRC=~/work/CoilHack/src

## 6803

## Behle et al. 2021
cd   ${MYDATA}/pcc6803/figures/behle21
$SRC/plotPCC6803.sh secY,slr0408,leuB,slr2107 genes 15000 png
$SRC/plotPCC6803.sh rpoB genes 10000 png
$SRC/plotPCC6803.sh rpoC1 genes 5000 png
$SRC/plotPCC6803.sh sll1532 genes 5000 png # rpoZ

$SRC/plotPCC6803.sh sigA,sigB,sigC,sigC,sigD,sigE,sigF,sigG,sigH,sigI genes 5000 png
$SRC/plotPCC6803.sh slr0609 genes 10000 png # strong code
$SRC/plotPCC6803.sh slr0352,rrn5Sb,rrn5Sa,rrn16Sb,rrn16Sa,rpl1 genes 7500 png

## RP and RNA Pol loci
$SRC/plotPCC6803.sh purA,rpoA,secY,rpl5,rps3,rpl22,rpl1,rps1a,nbp1,rps21,rpl28,rpl11,rps8,rps17 genes 3000 png
cd -

## defined range around rRNA loci
## 16S - 1500: 16Sa: 2453675+2000 = 2455675
##             16Sb: 3325053-2000 = 3323053
## 5S + 1500: 5Sa: 2448648-5000 = 2443648
##            5Sb: 3330080+5000 = 3335080
cd   ${MYDATA}/pcc6803/figures/behle21/rRNA
$GENBRO/src/plotFeature.R -i $GENDAT/pcc6803 --coor "1,2441648:2455675" -r 0 -F -s rrna -S $SRC/selections_pcc6803.R  -f png -v -W 4.5 -H .9
$GENBRO/src/plotFeature.R -i $GENDAT/pcc6803 --coor "1,3323053:3336080" -r 0 -s rrna -S $SRC/selections_pcc6803.R  -f png -v -W 4.5 -H .9
cd -
## selected TU
cd   ${MYDATA}/pcc6803/figures/behle21/TU/selected/
#$GENBRO/src/plotFeature.R -i $GENDAT/pcc6803 --coor "4,32868:36416;1,28866:31506;1,1532185:1537864;1,853385:862219;1,60507:66040;1,853385:862219;1,833768:842951;1,853385:862219;1,27873:33586"  -r 3000 -s tu -S $SRC/selections_pcc6803.R  -f png -v
$GENBRO/src/plotFeature.R -i $GENDAT/pcc6803 -n crhR --rng 3000 -s tu -S $SRC/selections_pcc6803.R  -f png -v -W 4 -H 3

$GENBRO/src/plotFeature.R -i $GENDAT/pcc6803 --coor "1,36586:24873"   -s tu -S $SRC/selections_pcc6803.R  -f png -v -W 4 -H 3
$GENBRO/src/plotFeature.R -i $GENDAT/pcc6803 --coor "1,845951:827768" -s tu -S $SRC/selections_pcc6803.R  -f png -v -W 8 -H 3
$GENBRO/src/plotFeature.R -i $GENDAT/pcc6803 --coor "1,845951:817768" -s tu -S $SRC/selections_pcc6803.R  -f png -v -W 8 -H 3
$GENBRO/src/plotFeature.R -i $GENDAT/pcc6803 --coor "1,845951:807768" -s tu -S $SRC/selections_pcc6803.R  -f png -v -W 8 -H 3
$GENBRO/src/plotFeature.R -i $GENDAT/pcc6803 --coor "1,865219:850385" -s tu -S $SRC/selections_pcc6803.R  -f png -v -W 4 -H 3
$GENBRO/src/plotFeature.R -i $GENDAT/pcc6803 --coor "1,2440648:2458675" -r 0 -F -s tu -S $SRC/selections_pcc6803.R  -f png -v -W 4 -H 3


## large view of rRNA
$GENBRO/src/plotFeature.R -i $GENDAT/pcc6803 --coor "1,3303053:3356080" -r 0 -s tu -S $SRC/selections_pcc6803.R  -f png -v -W 9 -H 3
$GENBRO/src/plotFeature.R -i $GENDAT/pcc6803 --coor "1,2421648:2475675" -r 0 -F -s tu -S $SRC/selections_pcc6803.R  -f png -v -W 9 -H 3
cd -

## DOMAINS with RP genes - selection in 20201204_RM_topA_RNAseq.R
mkdir  ${MYDATA}/pcc6803/figures/behle21/RP
cd   ${MYDATA}/pcc6803/figures/behle21/RP
$GENBRO/src/plotFeature.R -i $GENDAT/pcc6803 --coor "1,318630:319068;1,502012:502667;1,824080:826431;1,826507:831245;1,833768:842951;1,922663:926600;1,926601:927251;1,1751111:1751823;1,1826735:1829631;1,1957509:1958168;1,1958169:1959526;1,2404512:2405336;1,2831068:2831842;1,1431803:1434673;1,1532185:1537864;1,335717:336639;1,853385:862219;1,1096994:1099188;1,1227582:1228073;1,1722027:1723066;1,1914707:1919882;1,2127847:2128689;1,2448095:2448492;1,2603912:2604826;1,2644757:2645254;1,2886039:2887533;1,2797995:2798551;1,1815424:1816696" -r 3000 -s tu -S $SRC/selections_pcc6803.R  -f png -v 
cd -

## LARGE TU
## TODO: do this elsewhere, loop through TU result file, and name after TU
mkdir  ${MYDATA}/pcc6803/figures/behle21/TU
cd   ${MYDATA}/pcc6803/figures/behle21/TU
$GENBRO/src/plotFeature.R -i $GENDAT/pcc6803 --coor "1,1914707:1919882;1,1378480:1383744;1,1306165:1311440;1,2319434:2324768;1,524320:529712;1,1367100:1372517;1,352232:357723;1,2871555:2877048;1,60507:66040;1,1532185:1537864;1,27873:33586;1,749124:754965;1,1184483:1190482;5,3811:9873;5,58528:64591;1,587161:593231;1,2109846:2116033;1,2769102:2775294;1,2353387:2359609;1,171884:178117;2,6354:12659;1,131860:138264;1,2051350:2057786;1,683669:690297;1,1671953:1678733;1,3124829:3132274;1,2271555:2279033;1,3168888:3176441;1,755764:763405;1,2781334:2789070;1,1002548:1010621;1,853385:862219;1,1190719:1199651;1,833768:842951;1,3143608:3153365;5,65774:75583;5,11056:20913;2,52615:62704;1,404483:414659;1,357724:379038" -r 3000 -s tu -S $SRC/selections_pcc6803.R  -f png -v
cd -


cd   ${MYDATA}/pcc6803/figures/behle21/diverse
$SRC/plotPCC6803.sh topA,pCB2.4_M,pCA2.4_M,kaiC1,kaiC2,topA,gyrA,gyrA.2,gyrB,dnaB,dnaE-N,dnaE-C,gap1,CRISPR_1,CRISPR_2,CRISPR_3,TPP_riboswitch,ssrA,kaiC3,sll1712,psbA1,psbA2,psbA3,sds,dnaA,sll1397,smr0012,slr1075,rbcL,cmpA,fbpI,fbpII,trxA,trxA.2,trxA.3,rbp1,slr0006,sigA,hliC,slr0168 genes 5000 png
cd -

## various genes of interest
cd  ${MYDATA}/pcc6803/figures/diverse
$SRC/plotPCC6803.sh pCB2.4_M,pCA2.4_M,kaiC1,kaiC2,topA,gyrA,gyrA.2,gyrB,dnaB,dnaE-N,dnaE-C,gap1,CRISPR_1,CRISPR_2,CRISPR_3,TPP_riboswitch,ssrA,kaiC3,sll1712,psbA1,psbA2,psbA3,sds,dnaA,sll1397,smr0012,slr1075,rbcL,cmpA,fbpI,fbpII,trxA,trxA.2,trxA.3,rbp1,slr0006,sigA,hliC,slr0168 genes 5000 pdf
$SRC/plotPCC6803.sh slr1616,slr1063,sll1409,rrn23Sb genes 20000
$SRC/plotPCC6803.sh slr0915 genes 500 # gI intron
$SRC/plotPCC6803.sh sll1393,sll0945 genes 500 # glycogen synthases
# TODO: PBS proteins $SRC/plotPCC6803.sh sll1393,sll0945 genes 500 # 

cd -

## "housekeeping genes" - qPCR controls
cd  ${MYDATA}/pcc6803/figures/housekeeping
$SRC/plotPCC6803.sh rnpB,secA,rpoA genes 1500
cd -

### ALL CDS
/home/raim/programs/genomeBrowser//src/plotFeature.R -i ${MYDATA}/genomeData//pcc6803 -t CDS  -r 5000 -s genes -S /home/raim/programs/genomeBrowser//data/pcc6803/selections.R -f png -v

#$SRC/plotPCC6803.sh sll0750 genes 5000
#$SRC/plotPCC6803.sh rpaA genes 5000
#$SRC/plotPCC6803.sh trnfM-CAU genes 5000

## tight figures for coilhack project
cd  ${MYDATA}/pcc6803/figures/coilhack/
# gyr/top/hup # kai # metabolic # best oscillators
$SRC/plotPCC6803.sh gyrA,gyrA2,gyrB,topA,sll1712,kaiA,kaiC2,sll0484,kaiC3,psbA1,psbA2,psbA3,purF,slr1176,slr0244,slr0179,slr0633,slr0626,slr0090,rbp1,slr0006,sigA,hliC,slr0168,rpaA,rpaB,sll0887,slr1435 genes 1500 png 
cd -

## gyrA sgRNA:
cd  ${MYDATA}/pcc6803/figures
$SRC/plotPCC6803.sh slr0090 genes 1500 pdf
cd -

cd  ${MYDATA}/pcc6803/figures/sgRNA
## 2684846 putative offtarget of topA crRNA
$GENBRO/src/plotFeature.R -i $GENDAT/pcc6803 --coor "1,272975:274975;1,2683846:2685846"  -s genes -S $SRC/selections_pcc6803.R  -f png -v


## Ovidiu's Lrs2
cd  ${MYDATA}/pcc6803/figures/ovidiu
$SRC/plotPCC6803.sh slr0270,slr0058,slr0456,sll0269 genes 1500 png
cd -

## fMet-tRNA
cd  ${MYDATA}/pcc6803/figures/gI
$SRC/plotPCC6803.sh slr0915 genes 500 png
cd -

##7942
## topA:1416;dnaK:0111;kaiC:1216;hup?:1108
## 1208 - Antitoxin Phd_YefM
## 1202 - unknown
cd  ${MYDATA}/pcc7942/figures
$SRC/plot7942.sh gyrB,1416,0111,1216,1208,1202,1108 genes 5000 
$SRC/plot7942.sh 0254 genes 1500 # gyrA
$SRC/plot7942.sh 1694 genes 1500 # gyrA2

## tight figures for coilhack project
cd  ${MYDATA}/pcc7942/figures
$SRC/plot7942.sh 0254,1694,gyrB,1416,1108,2248 genes 1500 pdf # gyr/top/hupA/hupB
$SRC/plot7942.sh 1218 genes 1500 pdf # kai
$SRC/plot7942.sh 0424,1389,0893,0004 genes 1500 pdf # metabolic 
$SRC/plot7942.sh 1954,2038,1557,2535 genes 1500 pdf #  best oscillators vijayan
$SRC/plot7942.sh rpsF,0442,1913,2465,2326 genes 1500 pdf #  best oscillators ito
cd -

