## not required, but can be modified here!
strandSets <- c("annotation","vijayan11_transcripts","vijayan11_tiling")

## LOAD DATA SETS
selection <- list()
## note that the ordering is directly used in plotData()
selection[["genes"]] <- c("vijayan11_transcripts","vijayan11_tiling","annotation","AT","at_period","primers")#,"GCskew","nucleotides")
