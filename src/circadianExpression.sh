#!/bin/bash

cd ${MYDATA}/arabidopsis/edwards06/
R --vanilla < $GENBRO/data/publications/edwards06/edwards06_clustering.R &> edwards06_clustering.log &
cd ${MYDATA}/pcc6803/lehmann13/
R --vanilla < $GENBRO/data/publications/lehmann13/lehmann13_clustering.R &> lehmann13_clustering.log &
cd ${MYDATA}/pcc6803/saha16/
R --vanilla < $GENBRO/data/publications/saha16/saha16_clustering.R &> saha16_clustering.log &
cd ${MYDATA}/pcc7942/vijayan09/
R --vanilla < $GENBRO/data/publications/vijayan09/vijayan09_clustering.R &> vijayan09_clustering.log &
