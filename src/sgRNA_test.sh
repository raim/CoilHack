
## testing used sgRNA by blast:

export GSRC=~/work/CoilHack
export EXP=$GSRC/experiments/sgRNA_tests

blastn -query $GSRC/sequences/sgRNA_6803.fasta -db $PCC6803DAT/chromosomes/pcc6803.fasta  -task "blastn-short"  -outfmt 6 &> $EXP/sgRNA_6803_blast_all.tab
blastn -query $GSRC/sequences/sgRNA_6803.fasta -db $PCC6803DAT/chromosomes/pcc6803.fasta  -task "blastn-short"  -max_target_seqs 1 -outfmt 6 | sort -t$'\t' -k1,1 -k4,4gr  | sort -u -k1,1 --merge &> $EXP/sgRNA_6803_blast.tab


blastn -query $EXP/sgRNA_6803_offtargets.fasta -db $PCC6803DAT/chromosomes/pcc6803.fasta  -task "blastn-short"  -max_target_seqs 1 -outfmt 6 | sort -t$'\t' -k1,1 -k4,4gr  | sort -u -k1,1 --merge &> $EXP/sgRNA_6803_offtargets_blast.tab

## plot in genomeBrowser
R --vanilla < $GSRC/src/sgRNA_pcc6803_genomeBrowsertrack.R

cd  ${MYDATA}/pcc6803/figures/sgRNA
## 2684846 putative offtarget of topA crRNA
$GENBRO/src/plotFeature.R -i $GENDAT/pcc6803 --coor "1,273976:273997;1,2898899:2898873;1,1267563:1267587;1,2687959:2687937;1,1414173:1414151"  -s sgrna -S $GSRC/src/selections_pcc6803.R  -f png -v
cd -
