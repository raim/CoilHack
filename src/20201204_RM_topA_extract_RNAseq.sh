#!/bin/bash

### TODO: store .zip fles from Bielefeld on server and
##        extract required data here!
## 20210319 -- all files on server
## wget https://docs.cebitec.uni-bielefeld.de/s/R5XEFdz5TPsZn5k/download
## sudo mv download  $LABDATA/RNAseq/$batchid/RNA_020_2020_Cyanobacteria_CLIB_EFRE_CKB_Axmann.zip
## 20210319 -- manual via browser: select all files in 2021_new_reference
## and download
#mv ~/Downloads/2021_new_reference.zip $LABDATA/RNAseq/$batchid/
## 20210908 - re-download 15.7 GB file: Run_020_2020_UPLOAD.tar.gz
## mv ~/Downloads/Run_020_2020_UPLOAD.tar.gz $LABDATA/RNAseq/
## cd $LABDATA/RNAseq;mkdir tmp; mv Run_020_2020_UPLOAD.tar.gz tmp

LABDATA=/mnt/synmibi/Studierende/DATA/ # exon
if [ `uname -n` = "intron" ]; then
    LABDATA=/data/synmibi/ # intron
fi    
LABDATA=~/data/synmibi/ # intron

batchid=20200602_WM_coilhack_endpoint
reactorid=20201204_RM_topA
COILHACK=~/work/CoilHack
BATCHSEQ=$COILHACK/experiments/RNAseq/$batchid/
RNASEQ=$COILHACK/experiments/RNAseq/$reactorid/

finalversion=20210323_CeBiTec_final

## TIMESERIES
unzip -p $LABDATA/RNAseq/$reactorid/RNA_001_2021_EFRE_CKB_Axmann_HHU.zip RNA_001_2021_EFRE_CKB_Axmann_HHU/RNA_001_TPM_RPKM_raw_read_counts.xlsx > $RNASEQ/RNA_001_TPM_RPKM_raw_read_counts.xlsx
ssconvert --export-type=Gnumeric_stf:stf_assistant -O "locale=C format=automatic separator=, eol=unix sheet='ALL'" $RNASEQ/RNA_001_TPM_RPKM_raw_read_counts.xlsx $RNASEQ/20201204_topA_RM_RNAseq_timeseries_2.csv

## BATCH
unzip $LABDATA/RNAseq/$batchid/2021_new_reference.zip -d $BATCHSEQ/deseq2/

ssconvert --export-type=Gnumeric_stf:stf_assistant -O "locale=C format=automatic separator=, eol=unix sheet='Results'" $BATCHSEQ/deseq2/2021_new_reference/DeSeq2_all_in_reference_GyrA_vs_EVC_wildtype.xlsx $BATCHSEQ/deseq2/2021_new_reference/${batchid}_GyrA_vs_EVC.csv
ssconvert --export-type=Gnumeric_stf:stf_assistant -O "locale=C format=automatic separator=, eol=unix sheet='Results'" $BATCHSEQ/deseq2/2021_new_reference/DeSeq2_all_in_reference_GyrB_vs_EVC_wildtype.xlsx $BATCHSEQ/deseq2/2021_new_reference/${batchid}_GyrB_vs_EVC.csv
ssconvert --export-type=Gnumeric_stf:stf_assistant -O "locale=C format=automatic separator=, eol=unix sheet='Results'" $BATCHSEQ/deseq2/2021_new_reference/DeSeq2_all_in_reference_TOPA_vs_EVC_wildtype.xlsx $BATCHSEQ/deseq2/2021_new_reference/${batchid}_TOPA_vs_EVC.csv

## BATCH - 20210323
## 20220414 - bug in export, some text fields without quotes
ssconvert --export-type=Gnumeric_stf:stf_assistant -O "locale=C format=automatic separator=' HALLOSEP ' eol=unix sheet='Results'" $LABDATA/RNAseq/$batchid/$finalversion/DeSeq2_all_in_reference_GyrA_vs_EVC_wildtype_short_FINAL_20210321.xlsx $BATCHSEQ/deseq2/$finalversion/${batchid}_GyrA_vs_EVC.tsv
ssconvert --export-type=Gnumeric_stf:stf_assistant -O "locale=C format=automatic separator=' HALLOSEP ' eol=unix sheet='Results'" $LABDATA/RNAseq/$batchid/$finalversion/DeSeq2_all_in_reference_GyrB_vs_EVC_wildtype_short_FINAL_20210321.xlsx $BATCHSEQ/deseq2/$finalversion/${batchid}_GyrB_vs_EVC.tsv
ssconvert --export-type=Gnumeric_stf:stf_assistant -O "locale=C format=automatic separator=' HALLOSEP ' eol=unix sheet='Results'" $LABDATA/RNAseq/$batchid/$finalversion/DeSeq2_all_in_reference_TOPA_vs_EVC_wildtype_short_FINAL_20210321.xlsx $BATCHSEQ/deseq2/$finalversion/${batchid}_TOPA_vs_EVC.tsv

## replace temp tag by tab
sed -i 's/ HALLOSEP /\t/g' $BATCHSEQ/deseq2/$finalversion/${batchid}_GyrA_vs_EVC.tsv
sed -i 's/ HALLOSEP /\t/g' $BATCHSEQ/deseq2/$finalversion/${batchid}_GyrB_vs_EVC.tsv
sed -i 's/ HALLOSEP /\t/g' $BATCHSEQ/deseq2/$finalversion/${batchid}_TOPA_vs_EVC.tsv

