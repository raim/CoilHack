
## add CoilHack primer data track for genomeBrowser

coil.path <- "~/work/CoilHack/"

require(segmenTools)

## load functions
browser.path <- sub("GENBRO=","",system("env|grep GENBRO",intern=TRUE))
data.path <- sub("GENDAT=","",system("env|grep GENDAT",intern=TRUE))
source(file.path(browser.path,"src/genomeBrowser_utils.R"))
## data path
cyano.path <- sub("PCC6803DAT=","",system("env|grep PCC6803DAT",intern=TRUE))

op <- options(stringsAsFactors=F)

## CHROMOSOMES: index and name mapper!
chrMap <- read.delim(file.path(browser.path,"data","pcc6803","parameters","chromosomemapping.txt"),header=FALSE,row.names=1)

dat <- parseHomHits(files=file.path(coil.path,"sequences",
                                    "primers_6803_blast.tab"),type="blast")

## todo: use additional info "% identity","alignment length","mismatches","gap opens","mstart","mend","E","bit.score")
cols <- c(ID="model",chr="target",start="tstart",end="tend")

## process blast hits
dat <- dat[,cols]
colnames(dat) <- names(cols)
start <- dat[,"start"]
end <- dat[,"end"]

## resolve strand
strand <- rep("+",nrow(dat))
strand[start>end] <- "-"
dat[,"start"] <- apply(cbind(start,end),1,min)
dat[,"end"] <- apply(cbind(start,end),1,max)

## add strand, type, color info
dat <- cbind.data.frame(dat,strand=strand,type=rep("primer",nrow(dat)),
                        color=rep("#FF0000",nrow(dat)))

## replace chromosome by index
dat[,"chr"] <- chrMap[dat[,"chr"],1]

columns <- c(ID="ID", type="type", chr="chr", strand="strand",
             start="start", end="end", color="color")


## GENOMEBROWSER DATA

out.path <- file.path(data.path,"pcc6803")
odat <- list(ID="primers",
             description="CoilHack primers",
             data=dat,
             settings=list(type="plotFeatures",
               types="primer",columns=columns,
               arrow = list(length=.05, pch=NA, lwd=1.5),
               typord=TRUE,cuttypes=TRUE,names=FALSE,
               ylab=NA,hgt=.1,xaxis=FALSE))
## store data
file.name <- file.path(out.path,paste(odat$ID,".RData",sep=""))
save(odat,file=file.name)
