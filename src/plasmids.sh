#!/bin/bash

export COILHACK=/home/raim/work/CoilHack/

$GENBRO/src/calculateDiNuclProfile.R --id=pMD19T_NS4_PL22_sgRNA_gyrA_KmR --fdir=$COILHACK/strains/pcc6803_dCas9_gyrA/ --fend=.fasta --out=$COILHACK/strains/pcc6803_dCas9_gyrA --win=396 --stp=10 --prm=1000 --nrm=snr --circular --verb  --din AT --plot
