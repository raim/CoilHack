
dat <- read.delim("pMD19T_NS4_PL22_sgRNA_gyrA_KmR_AT_p1000_w396_s10_SNR_pvalues.csv")

ps <- c("X11.6470588235294")
png("pMD19T_NS4_PL22_sgRNA_gyrA_KmR_AT_pvalues.png")
plot(dat[,"coor"],-log2(dat[,ps]),type="l",xlab="position, bp",ylab="AT periodicity, -log2(p)")
dev.off()
