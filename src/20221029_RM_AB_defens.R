
## called from 20201204_RM_topA_biomass.R and
## produces plots requested by Anna Behle for PhD defens

plotdev(file.path("offline",paste0(expid,"_sampling")),
                type=fig.type, width=W, height=W/2, res=300)
par(mai=mait, mgp=c(1.4,.3,0),tcl=-.25, yaxs="i", xaxs="i")
plot(stimes, 1:length(stimes), xlim=XLM, type="l", ylab="sample number",
     axes=FALSE, ylim=c(0,length(stimes)))
axis(3, at=stimes, tcl=.5, labels=FALSE)
axis(3, at=stimes, tcl=-.5, labels=FALSE)
axis(1, at=-10:30,tcl=par("tcl")/2, labels=FALSE)
axis(1, at=XLB)
axis(2)
axis(2, at=0:100, labels=FALSE, tcl=-.125)
box()
mtext("time, d", 1, par("mgp")[2], adj=ADJ)#, cex=.8)
## mode arrows
abline(v=unlist(lapply(mode,function(x) x)), lty=3)
dev.off()

### BIOMASS FIGURE BUILD-UP for Anna B and Max D., phd defenses,
## as Fig 5A of the manuscript: 20201204_RM_topA_biomass_v2.pdf
## 1. just axes,
## 2. + rhamnose,
## 3. + OD.
## 4. + glycogen
buildup <- c("axes","rhamnose","OD","biomass","glycogen","legend")
for ( i in 1:6 ) {

    plotdev(file.path("offline",paste0(expid,"_biomass_v2_",i)),
        type=hfig.type, width=W, height=W/2, res=300)
    par(mai=mait, mgp=c(1.4,.3,0),tcl=-.25, yaxs="i", xaxs="i")

    rhalim <- .5

    col1 <- "#999999"
    col2 <- 1
    
    ## AXES
    ## empty glycogen replicates with axes
    rp <- unlist(split(glyc[,grep("rep",colnames(glyc))], f=glyc$sample))
    tm <- stimes[sub("\\..*","",names(rp))]
    df <- data.frame(time=tm, value=rp/1000)
    lplot(df, xlim=XLM, ylim=c(0,bmlim), pch=NA, col=NA,
          type="l",cex=.7, axis.col=col2,
          side=4, ylab=expression("g/L"),#~mu*g/mL),
          num=0, add=FALSE, fit=TRUE, span=.5)
     ## empty OD plot with axes
    df <- data.frame(value=OD4$ODc,
                     time=OD4$time)
    lplot(df, xlim=XLM, ylim=c(0,odlim), pch=NA, col=NA,type="l",
          ylab=expression(OD[lambda]), side=2, add=TRUE, fit=FALSE,
          axis.col=col1)

    axis(1, at=-10:30,tcl=par("tcl")/2, labels=FALSE)
    axis(1, at=XLB)
    mtext("time, d", 1, par("mgp")[2], adj=ADJ)#, cex=.8)
    
    ## mode arrows
    shft <- 0 #c(0.5,0,.5,0)#,.5)
    mod <- mode[-length(mode)]  #skip harvest for this figure
    nms <- names(mod)#sub("induction","rhamnose",names(mod))
    mody <- odlim
    arrows(x0=unlist(mod), y0=mody+shft, y1=mody-.15+shft, length=.05,xpd=TRUE)
                                        #text(x=unlist(mod), y=mody+.15+shft, labels=nms,xpd=TRUE)
    axis(3, at=unlist(mod), labels=nms, mgp=c(0,.05,0),
         col=NA,cex=.7)
    abline(v=unlist(lapply(mode,function(x) x)), lty=3)
    ## CDW, glycogen and density all on same axis
    if ( i>5 ) {
        legend(x=13,y=3.3,#"right",
               c(expression(OD[lambda]),
                 expression(CDW),
                 expression(glycogen),
                 expression(rhamnose)),
               col=col4[c(8,2,4,1)], lty=c(1,1,1,2), lwd=c(4,1,1,2),
               pch=c(NA,1,19,NA), pt.cex=c(NA,.7,.7,NA),
               box.col=NA, bg="#FFFFFF66",
               x.intersp=.5, seg.len=.9, cex=.9)
    }
    ## OD4
    if ( i>2 ) {
        df <- data.frame(value=OD4$ODc,
                         time=OD4$time)
        lplot(df, xlim=XLM, ylim=c(0,odlim), pch=19, col=col1,axis.col=NA,
              type="l",
              ylab=NA, side=2, add=TRUE, fit=FALSE, axes=FALSE)
        ##lines(OD4$time, OD4$ODs, col=col4[7],lwd=1)
    }
    
    ## RHAMNOSE
    ## TODO: USE ACTUAL DILUTION RATE and calculate with other rates!!
    if ( i>1 ) {
        par(mgp=c(1.4,.3,0))
        
        ## glycogen replicates
        rp <- unlist(split(glyc[,grep("rep",colnames(glyc))], f=glyc$sample))
        tm <- stimes[sub("\\..*","",names(rp))]
        df <- data.frame(time=tm, value=rp/1000)
        lplot(df, xlim=XLM, ylim=c(0,bmlim), pch=NA, col=NA,
              type="p",cex=.7,
              ylab=NA,#~mu*g/mL),
              num=0, add=TRUE, fit=TRUE, span=.5)
        
        mw_rha <- 164.16 #g/mol 
        phi <- 0.012 # total reactor dilution rate, per hour
        c0 <- 0.002*mw_rha # 2 mM, 164.16 g/mol -> 0.32832 g/L
        time <- seq(0,28, .1) # days
        ## dc/dt = -phi(t) * c0
        ct <- c0 * exp(-phi*24*time)
        lines(c(0,time), c(0,ct), type="l",lty=2, col=col2, lwd=2)
        ##text(0,1.8,"rhamnose",pos=4, col=col4[8])
    }
    if ( i>4 ) {
        ## glycogen replicates
        rp <- unlist(split(glyc[,grep("rep",colnames(glyc))], f=glyc$sample))
        tm <- stimes[sub("\\..*","",names(rp))]
        df <- data.frame(time=tm, value=rp/1000)
        lplot(df, xlim=XLM, ylim=c(0,bmlim), pch=19, col=col4[4],type="p",cex=.7,
              side=4, ylab="",#expression("glycogen, g/L"),#~mu*g/mL),
              num=0, add=TRUE, fit=TRUE, span=.5)
    }
    if ( i>3 ) {
        ## CDW
        df <- data.frame(value= cdw$CDW,
                         time=cdw$time,
                         outlier=cdw$outlier)
        lplot(df, xlim=XLM, ylim=c(0,bmlim),
              pch=1, col=col4[2],type="p", cex=.7, lwd=2,
              side=4, ylab="",
              num=0, add=TRUE, fit=TRUE, span=.75)
    }
    dev.off()    
    
}


### BIOMASS FIGURE BUILD-UP for Anna B, phd defense,
## as Fig. S11 of the manuscript, figures/20201204_RM_topA_casy.png
## 1. just axes,
## 2. + count,
## 3. + volume.
for ( i in 1:3 ) {
    plotdev(file.path("offline",paste0(expid,"_casy_",i)),
            type=hfig.type, width=W, height=W/2, res=300)
    par(mai=MAI, mgp=c(1.2,.3,0),tcl=-.25, yaxs="i", xaxs="i")
    ## normalized volume distributions
    image(y=volx,x=stimes[colnames(casyn)],z=t(casyn), col=rep(NA,length(cols)),
                                        #breaks=brks,
          ylab=NA, #expression("cell volume,"~fL),
          xlab=NA,
          axes=FALSE, ylim=c(0,30),xlim=XLM)#, xaxs="r",yaxs="r")
    abline(v=unlist(lapply(mode,function(x) x)), lty=3)

    axis(1, at=-10:30,tcl=par("tcl")/2, labels=FALSE)
    axis(1, at=XLB)
    mtext("time, d", 1, par("mgp")[2], adj=ADJ)

    col1 <- col4[7]
    col2 <- col4[4]

    par( mgp=c(1.2,.3,0), yaxs="i", xaxs="i")
    df <- data.frame(value=casy$peak.volume.casy,
                     time=casy$time,
                     outlier=casy$outlier)
    ##par( mgp=c(.9,.3,0))
    lplot(df, xlim=XLM, ylim=c(0,30), lty=1, pch=1, type="p",cex=.7,
          col=NA, axis.col=col1,
          side=4, ylab=expression("cell volume,"~fL),
          num=0, add=TRUE, fit=TRUE)
    ## cell count
    df <- data.frame(value=casy$count/1e8, time=casy$time,
                     outlier=casy$outlier)
    lplot(df, xlim=XLM, ylim=c(0,7), pch=1, type="p",cex=.7,
          col=NA, axis.col=col2,
          ylab=expression("cell count,"~10^8/mL),
              side=2, num=0, add=TRUE, fit=TRUE)

    if ( FALSE ) {
        legend(x=0,y=c(7),c("count","volume dist.","volume peak"),
               col=c(col4[4],"#00000099",col4[7]),
               lty=c(1,1,1), pch=c(1,NA,1), pt.cex=c(.7,NA,.7),
               lwd=c(1,5,1), box.col=NA, bg="#FFFFFF00",
               x.intersp=.5, seg.len=.9, cex=.9)
    }

    if ( i>1 ) {
        ## peak volume
        df <- data.frame(value=casy$peak.volume.casy,
                         time=casy$time,
                         outlier=casy$outlier)
        ##par( mgp=c(.9,.3,0))
        lplot(df, xlim=XLM, ylim=c(0,30), lty=1, pch=1, type="p",cex=.7,
              col=col1, axis.col=NA,
              ylab=expression("cell volume,"~fL),
              num=0, add=TRUE, fit=TRUE)
    }
    if ( i>2 ) {
        ## cell count
        par( mgp=c(1.2,.3,0), yaxs="i", xaxs="i")
        df <- data.frame(value=casy$count/1e8, time=casy$time,
                         outlier=casy$outlier)
        lplot(df, xlim=XLM, ylim=c(0,7), pch=1, type="p",cex=.7,
              col=col2, axis.col=NA,
              ylab=expression("cell count,"~10^8/mL),
              num=0, add=TRUE, fit=TRUE)
        axis(1, at=-10:30,tcl=par("tcl")/2, labels=FALSE)
        axis(1, at=XLB)
        mtext("time, d", 1, par("mgp")[2], adj=ADJ)
    }
    
    dev.off()
}


