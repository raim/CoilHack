#!/bin/bash

## log file for using recovery data from arduiono sensors

cd ~/work/CoilHack/experiments/reactor/pcc6803/20201204_RM_topA
cp -a Gas_Sensor.csv Gas_Sensor_orig.csv
chmod a-r Gas_Sensor_orig.csv
cp -a Gas_Sensor/Recovery/Gas_Sensor_Combined.csv Gas_Sensor.csv
##cat Gas_Sensor/Recovery/Gas_Sensor_session_20201204_16-33-33.csv > Gas_Sensor.csv

##grep -v Time Gas_Sensor/Recovery/Gas_Sensor_session_20201207_17-04-15.csv >>  Gas_Sensor.csv


cp -a Dual_Scale.csv Dual_Scale_orig.csv
chmod a-r Dual_Scale_orig.csv

cat Dual_Scale/Recovery/Dual_Scale_session_20201204_16-32-01.csv > Dual_Scale.csv
grep -v "^time" Dual_Scale/Recovery/Dual_Scale_session_20201229_12-41-17.csv >> Dual_Scale.csv
grep -v "^time" Dual_Scale/Recovery/Dual_Scale_session_20201230_12-45-09.csv >> Dual_Scale.csv
grep -v "^time" Dual_Scale/Recovery/Dual_Scale_session_20201230_12-46-32.csv >> Dual_Scale.csv
grep -v "^time" Dual_Scale/Recovery/Dual_Scale_session_20201230_12-47-07.csv >> Dual_Scale.csv
