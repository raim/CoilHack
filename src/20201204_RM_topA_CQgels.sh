
LABDATA=/mnt/synmibi/Studierende/DATA/ # exon
if [ `uname -n` = "intron" ]; then
    LABDATA=/data/synmibi/ # intron
fi    
COILHACK=~/work/CoilHack


## copy GELS
#cp -a '$LABDATA/RNAseq/20201204_RM_topA/samples/Gel 2021-01-08 17hr 25min_RNA.tif' $COILHACK/experiments/CQgels/20201204_RM_topA_RNAgel.tif
#cp -a $LABDATA/CQgels/20201106_CQgel_1-8_nicht_induziert.tif $COILHACK/experiments/CQgels/
#cp -a $LABDATA/CQgels/20210127_endpoint_CQ.tif $COILHACK/experiments/CQgels/
cp -a $LABDATA/CQgels/210111_CQ_time-series.tiff  $COILHACK/experiments/CQgels/

