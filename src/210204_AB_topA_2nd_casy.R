## COLLATE BIOMASS MEASURES FROM BATCH EXPERIMENT, 202101

library(platexpress) #wavelength colors
library(segmenTools) #plotdev, add_alphas
options(stringsAsFactors=FALSE)

expid <- "210204_AB_topA_2nd"
setwd(file.path("~/work/CoilHack/experiments/batch/",expid))
fig.type <- "png" # "pdf" # 
hfig.type <- "png" # "pdf" # 

## R4 colors
## https://www.r-bloggers.com/2019/11/evaluation-of-the-new-palette-for-r/
col4 = c("#000000", "#DF536B", "#61D04F", "#2297E6",
         "#28E2E5", "#CD0BBC", "#EEC21F", "#9E9E9E")

e.nms <- c("+"=expression("+rhamnose"),
           "-"=expression("-rhamnose"))

## read sample dates
samples <- read.csv("second_induction_time-log.csv",sep=";", dec=",")
samples <- samples[samples[,1]!="",]
sdates <- strptime(paste(samples$date, samples$time),
                   format="%d.%m.%y %H:%M")

## align sample IDs - rm Exp tag, underscores
sids <- tolower(sub("SAMPLE3", "", gsub("_", "", samples[,1])))
## experiments
eids <- sub("[0-9][0-9]","",sids)

## experiment colors
ecols <- col4[1:length(unique(eids))]
names(ecols) <- unique(eids)

stimes <- rep(NA, length(sdates))
names(stimes) <- sids
for ( eid in unique(eids) ) {
    stimes[sids[eids==eid]] <- difftime(sdates[eids==eid],
                                        min(sdates),unit="days")
    
}
names(sdates) <- names(stimes) <- names(eids) <- sids



## read CASY DATA and plot on the fly - TODO: plot below
casyl <- casys <- list()
for ( i in 1:length(e.nms) ) {

    eid <- names(e.nms)[i]
    
    fid <- file.path("analysis",paste0(expid,"_CASY_",toupper(eid),".tsv"))
    casy <- read.delim(fid,check.names=FALSE)
    
    diax <- casy$diameter
    volx <- casy$volume
    counts <- casy[,-(1:2)]
    
    ## get SAMPLE IDs
    cids <- sub("\\.1$","",sub("SAMPLE3_","",colnames(counts)))
    
    
    if ( any(!cids%in%names(stimes)) )
        cat(paste(i, eid, "samples not found:",
                  paste(cids[!cids%in%names(stimes)],collapse=";"),"\n"))
    if ( any(duplicated(cids)) )
        cat(paste(i, eid, "duplicated samples found:",
                  paste(cids[duplicated(cids)],collapse=";"),"\n"))
    ## handle duplicated
    colnames(counts) <- cids

    counts <- sapply(unique(cids),function(x) {
        ## for ( x in unique(cids) ) {
        idx <- which(colnames(counts)==x)
        if ( length(idx)==0 ) {
            cat(paste("no", idx, "\n"))
        } else if ( length(idx)==1 ) {
            res <- counts[,idx]
        } else {
            res <- apply(counts[,idx],1,mean)
        }
        res})


    casyl[[eid]] <- cbind.data.frame(diameter=diax,volume=volx,counts)

    ## casy summary!
    fid <- file.path("analysis",paste0(expid,"_CASY_",toupper(eid),
                                       "_summary.tsv"))
    casy <- read.delim(fid, check.names=FALSE)
    cids <- sub("SAMPLE3_","",casy$sample)
    casy$sample <- cids
    casys[[eid]] <- casy
}

### PLOT CASY
## SIZE FILTER for distribution plot
min.diam <- 1.25
max.diam <- 5
## colors and breaks
cols <- viridis::viridis(100)
cols <- grey.colors(100,start=1,end=0)
brks <- seq(min.diam,max.diam,length.out=101)

## summary: all volume distributions, total counts and volume
plotdev(paste0(expid,"_casy"),
        width=4, height=4, res=300, type=hfig.type)
par(mai=c(.05,.05,.05,.05), mgp=c(1.2,.3,0), tcl=-.25,yaxs="i")
layout(rbind(c(3,1,3),rbind(c(3,2,3),rep(3,3))),
       widths=c(.2,1,.2), heights=c(1,1,.2))
for ( i in length(e.nms):1 ) {
    
    eid <- names(e.nms)[i]
    casy <- casyl[[eid]]
        
    diax <- casy$diameter
    volx <- casy$volume
    counts <- casy[,-(1:2)]

    filter <- diax>min.diam & diax<max.diam
    counts <- counts[filter,]
    diax <- diax[filter]
    volx <- volx[filter]

    cids <- colnames(counts)

    ## smooth
    counts <- apply(counts,2, function(x) ma(x, 15))
    counts[is.na(counts)] <- 0
    
    ## normalize per column: relative frequency
    counts <- apply(counts,2, function(x) x/max(x))

    
    ## plot
    image(x=stimes[cids], y=volx, z=t(counts), ylim=c(0,29),
          col=cols, breaks=seq(0,1,length.out=length(cols)+1),
          xlab=NA, ylab=NA, axes=FALSE, xaxs="i",xlim=c(-.5,5.5))
    legend("topleft", e.nms[eid], text.col=ecols[eid], cex=1.2,
           text.font=2, box.col=NA, bg=NA, seg.len=0, x.intersp=0)
    #axis(4, labels=FALSE)
    axis(3, labels=FALSE)
    axis(2, las=2)
    axis(1, labels=i==1)
    if(i==1) mtext(expression("cell volume"~V[cell]*","~ fL), 2,
                   par("mgp")[1]*.95, adj=2.5)
    if(i==1) mtext("time, d", 1, par("mgp")[1]*1.1, xpd=TRUE)
    box()

    if ( TRUE ) {
        if(i==2)
            legend(x=-.5, y=23, c("cell count","total volume"),
                   lty=2:1,pch=c(19,1),bty="n")
        yn <- casys[[eid]][,"cells/mL"]/1e8/2
        yv <- casys[[eid]][,"volume,uL/mL"]
        cids <- casys[[eid]][,"sample"]
        par(new=TRUE,xaxs="i")
        plot(stimes[cids], yn, col=ecols[eid],lty=2,
             ylim=c(0,4), type="l",xlim=c(-.5,5.5),axes=FALSE,ylab=NA,xlab=NA)
        points(stimes[cids], yn, col=ecols[eid], pch=19)
        lines(stimes[cids], yv, col=ecols[eid], lty=1)
        points(stimes[cids], yv, col=ecols[eid], pch=1)
        axis(4, las=2)
        ##axis(1)
        if(i==1)
            mtext(expression(5*"x"*10^7~cells/mL~" & "~"total volume,"~mu*L/mL), 4,
                  par("mgp")[1]*1.4, adj=0)
    }
}
dev.off()


## test casy, own vs. pre.calculated values

png("casy_comparison.png", width=7, height=3.5, res=300, units="in")
par(mfcol=c(1,2), mai=c(.5,.5,.1,.1), mgp=c(1.3,.4,0))
for ( i in 1:length(casys) ) {
    plot(casys[[i]][,"volume,uL/mL"],
         casys[[i]][,"Volume/ml"]/1e9,
         xlim=c(0,3),ylim=c(0,3))
    abline(a=0, b=1)
}
dev.off()

png("casy_summary.png", width=7, height=3.5, res=300, units="in")
par(mfrow=c(1,2), mai=c(.5,.5,.1,.1), mgp=c(1.3,.4,0))
for ( i in 1:length(casys) ) {
    x <- stimes[casys[[i]]$sample]

    y <- casys[[i]][,"cells/mL"]/1e8
    ##y <- casys[[i]][,"Counts/ml"]/1e8
    plot(x, y, ylim=c(0,8), type="l")

    y <- casys[[i]][,"volume,uL/mL"]
    ##y <- casys[[i]][,"Volume/ml"]/1e9
    par(new=TRUE)
    plot(x, y, ylim=c(0,4), type="l", axes=FALSE,col=2)
    axis(4)
    legend("topleft",names(casys)[i])
}
legend("right", c("1e8 cells/mL", "total volume, uL/mL"),
       col=1:2, lty=1)
dev.off()
