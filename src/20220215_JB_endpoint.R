
## processing data for the two experiments run by Jonas Burmester,
## IDS: 20220125_JB and  20220215_JB

## NOTE: 20220125_JB, gyrA vs EVC, was run with rhamnose
##       which does NOT induce CRISPRi, but EVC can be used as
##       comparison, and now also has a rhamnose +/- control

## TODO
## * OD curves : also OD curves for Wandana: error bars over replicates!
## * 8-shaped cells, just a table?
## * total protein measurements

library(readxl)
library(segmenTools) #plotdev, dense2d
options(stringsAsFactors=FALSE)

expid1 <- "20220125_JB" # first experiment, consider EVC only
expid <- "20220215_JB"
ipath <- "~/work/CoilHack/experiments/"
epath <- file.path(ipath,expid)
fig.path <- file.path(epath,"figures")
mpath <- file.path(epath,"20220225_JB_microscopy_measurements")
dir.create(fig.path, showWarnings=FALSE)

setwd(epath)
fig.type <- ifelse(interactive(), "png","pdf") # "png" # 
hfig.type <- "png" # "pdf" # 
W <- H <- 3.5
Wc <- Hc <- 2

## R4 colors
## https://www.r-bloggers.com/2019/11/evaluation-of-the-new-palette-for-r/
col4 = c("#000000", "#DF536B", "#61D04F", "#2297E6",
         "#28E2E5", "#CD0BBC", "#EEC21F", "#9E9E9E")

## STRAINS
strains <- c("EVC","gyrA","topA")
expl <- strains
expl[strains=="EVC"] <- expression(EVC)
expl[strains=="topA"] <- expression(topA^OX)
expl[strains=="gyrA"] <- expression(gyrA^kd)
names(expl) <- strains
## experiment colors
ecols <- col4[c(1:2,6)]
names(ecols) <- unique(strains)
## map names between casy and microscopy
nmap <- c("EVC+"="EVC+A",
          "EVC-"="EVC-A",
          "gyrA+"="GYRA_A+",
          "gyrA-"="GYRA_A-",
          "topA+"="TOPA_A+",
          "topA-"="TOPA_A-")

## utility functions

## tight axis, with tick labels only at two extreme ends (lim)
taxis <- function(xlim=c(1,5), ylim ) {
    if ( missing(ylim) ) ylim <- xlim
    axis(1,labels=FALSE)
    axis(2,labels=FALSE)
    axis(3,labels=FALSE)
    axis(4,labels=FALSE)
    axis(1,at=xlim)
    axis(2,at=ylim)
}




### MICROSCOPY STATISTICS
## TODO: load 8-cell count statistics and plot,
##       file: 20220323_Cellcount_Ovs8.tsv

## axis labels
dlb <- expression(diameter/mu*m)  # Diameter
wlb <- expression(width/mu*m)  # Diameter
llb <- expression(length/mu*m) # Axis
## parse microscopy stats
micro <- NULL
for ( i in 1:length(strains) ) {
    strid <- strains[i]
    for ( ind in c("+","-") ) {
        file <- file.path(mpath,paste0("Results_",strid,ind,"_new.txt"))
        cat(paste("parsing", file, "\n"))
        dat <- read.delim(file)
        dat$strain <- strid
        dat$inducer <- ind
        micro <- rbind(micro, dat)
    }
}

## experiment tag
micro$exp <- paste0(micro$strain,micro$inducer)
## add tag without special chars
micro$exp2 <- paste0(micro$strain,ifelse(micro$inducer=="+","p","m"))

## TODO: calculate volumes and compare to casy volumes?
if ( interactive() )
    vioplot::vioplot(Area ~ exp2, data=micro)


frc8 <- c()
pchs <- c("+"=19, "-"=1)
for ( eid in unique(micro$exp) ) {
    ud <- micro[micro$exp==eid,]
    plotdev(file.path(fig.path,paste0("microscopy_sizes_",eid)), type=fig.type,
            width=Wc,height=Hc, res=300)
    par(mai=c(.35,.35,.1,.1), mgp=c(.7,.4,0), tcl=-.25)
    dense2d(ud$Axis, ud$Dia, pch=19, cex=.5,
            xlim=c(0,5),ylim=c(0,5), xlab=llb,ylab=wlb, axes=FALSE)
    taxis(xlim=c(0,5))
    abline(a=0,b=1)
    STR <- sub("[-|+]","",eid)
    ind <- ifelse(length(grep("\\+",eid))>0,"+ ind.","- ind.")
    legend("top", legend=c(expl[STR],ind), text.col=ecols[STR],bty="n")

    abline(a=-.5, b=1, lty=3)
    frc <- sum(ud$Dia < ud$Axis -.5)/nrow(ud)
    frc8[eid] <- frc
    legend("bottomright", paste(round(100*frc,1),"%"),bty="n",
           y.intersp=0)
    dev.off()

}


STR <- micro$strain
IND <- micro$inducer
mbrks <- seq(0,6,.2)

plotdev(file.path(fig.path,"length"), type=fig.type,
        width=2*W,height=H, res=300)
par(mfrow=c(2,3),mai=c(.5,.5,.25,.1), mgp=c(1.3,.4,0), tcl=-.25)
hist(micro$Dia[STR=="EVC"&IND=="-"], xlim=c(0,6),breaks=mbrks,
     main="EVC",xlab=llb)
hist(micro$Dia[STR=="gyrA"&IND=="-"],xlim=c(0,6),breaks=mbrks,
     main="gyrA",xlab=llb)
hist(micro$Dia[STR=="topA"&IND=="-"],xlim=c(0,6),breaks=mbrks,
     main="topA",xlab=llb)
hist(micro$Dia[STR=="EVC"&IND=="+"] ,xlim=c(0,6),breaks=mbrks,
     main="+inducer",xlab=llb)
hist(micro$Dia[STR=="gyrA"&IND=="+"],xlim=c(0,6),breaks=mbrks,
     main="+inducer",xlab=llb)
hist(micro$Dia[STR=="topA"&IND=="+"],xlim=c(0,6),breaks=mbrks,
     main="+inducer",xlab=llb)
dev.off()

plotdev(file.path(fig.path,"width"), type=fig.type,
        width=2*W,height=H, res=300)
par(mfrow=c(2,3),mai=c(.5,.5,.25,.1), mgp=c(1.3,.4,0), tcl=-.25)
hist(micro$Axis[STR=="EVC"&IND=="-"], xlim=c(0,6),breaks=mbrks,
     main="EVC",xlab=wlb)
hist(micro$Axis[STR=="gyrA"&IND=="-"],xlim=c(0,6),breaks=mbrks,
     main="gyrA",xlab=wlb)
hist(micro$Axis[STR=="topA"&IND=="-"],xlim=c(0,6),breaks=mbrks,
     main="topA",xlab=wlb)
hist(micro$Axis[STR=="EVC"&IND=="+"] ,xlim=c(0,6),breaks=mbrks,
     main="+inducer",xlab=wlb)
hist(micro$Axis[STR=="gyrA"&IND=="+"],xlim=c(0,6),breaks=mbrks,
     main="+inducer",xlab=wlb)
hist(micro$Axis[STR=="topA"&IND=="+"],xlim=c(0,6),breaks=mbrks,
     main="+inducer",xlab=wlb)
dev.off()

### OD Data
## run 1: 20220125_JB_OD_EVC_gyrA.tsv - use for EVC (-rha)
## run 2: 20220214_JB_OD_topA_gyrA.tsv - use for gyrA-kd and topA-OX
od.file1 <- "20220125_JB_OD_EVC_gyrA.xlsx"
od.file2 <- "20220214_JB_OD_topA_gyrA.xlsx"

od1 <- readxl::read_excel(file.path(ipath,expid1, "Data",od.file1))
## rm diluted
od1 <- od1[seq(2,10,2),]
colnames(od1) <- sub("rha","",colnames(od1))
## only keep EVC
od1 <- od1[,grep("EVC",colnames(od1))]

od2 <- readxl::read_excel(file.path(ipath,expid, "Data",od.file2))
## rm diluted
od2 <- od2[seq(2,10,2),]
colnames(od2) <- sub("rha","",colnames(od2))

## bind all
od <- cbind(od1,od2)

atimes <- 0:4 # sampling days

for ( rpl in c("A","B") ) {

    ## split into A and B series
    oda <- od[,grep(paste0("_",rpl),colnames(od))]
    colnames(oda) <- sub(paste0("_",rpl),"",colnames(oda))

    plotdev(file.path(fig.path,paste0(expid,"_OD_",rpl)),
            width=4, height=2, res=300, type=fig.type)
    par(mfcol=c(1,1), mai=c(.3,.5,.1,.5), mgp=c(1.2,.3,0), tcl=-.25,yaxs="i")

    plot(1,col=NA, xlim=c(0,4.5), ylim=c(0,3.3),
         ylab=expression(OD[750]), xlab=NA, axes=FALSE)
    mtext("days", 1, par("mgp")[2], adj=1.0)
    axis(1)
    axis(2)
    box()
    for ( str in strains ) {
        y <- oda[,paste0(str,"+")]
        lines(atimes[!is.na(y)], y[!is.na(y)], col=ecols[str])
        points(atimes[!is.na(y)], y[!is.na(y)], col=ecols[str],pch=19)
        y <- oda[,paste0(str,"-")]
        lines(atimes[!is.na(y)], y[!is.na(y)], col=ecols[str],lty=2)
        points(atimes[!is.na(y)], y[!is.na(y)], col=ecols[str])
    }
    legend("topleft", c("+inducer","-inducer"), pch=c(19,1), lty=1:2)
    dev.off()
}



### CASY DATA

## CASY DATA, as collected by scripts
## 20220125_JB_endpoint_CASY.R and
## 20220125_JB_endpoint_CASY.R


## read CASY DATA 
casy.files <- c(
    "20220125_JB/CASY/20220125_JB_CASY_EVC-A.tsv",
    "20220125_JB/CASY/20220125_JB_CASY_EVC-B.tsv",
    "20220125_JB/CASY/20220125_JB_CASY_EVC+A.tsv",
    "20220125_JB/CASY/20220125_JB_CASY_EVC+B.tsv",
    "20220215_JB/CASY/20220215_JB_CASY_TOPA_A-.tsv",
    "20220215_JB/CASY/20220215_JB_CASY_TOPA_A+.tsv",
    "20220215_JB/CASY/20220215_JB_CASY_TOPA_B-.tsv",
    "20220215_JB/CASY/20220215_JB_CASY_TOPA_B+.tsv",
    "20220215_JB/CASY/20220215_JB_CASY_GYRA_A-.tsv",
    "20220215_JB/CASY/20220215_JB_CASY_GYRA_A+.tsv",
    "20220215_JB/CASY/20220215_JB_CASY_GYRA_B-.tsv",
    "20220215_JB/CASY/20220215_JB_CASY_GYRA_B+.tsv")
casyl <- casys <- list()
for ( i in 1:length(casy.files) ) {

    
    cfile <- casy.files[i]
    
    fid <- file.path(ipath,cfile)
    casy <- read.delim(fid)
    
    diax <- casy$diameter
    volx <- casy$volume
    counts <- casy[,-(1:2)]
    
    ## get SAMPLE IDs
    eid <- sub(".*_CASY_","",sub("\\.tsv","",fid)) 
    cids <- sub(".*_","",colnames(counts))

    colnames(counts) <- cids
    casyl[[eid]] <- cbind(diameter=diax,volume=volx,counts)

    ## casy summary!
    fid <- sub("\\.tsv","_summary.tsv",fid)
    casy <- read.delim(fid, check.names=FALSE)
    cids <- sub(".*_","",casy$sample)

    casy$sample <- cids
    casys[[eid]] <- casy
}

## PLOT CASY
## SIZE FILTER for distribution plot
min.diam <- 1.25
max.diam <- 5
## colors and breaks
cols <- viridis::viridis(100)
cols <- grey.colors(100,start=1,end=0)
brks <- seq(min.diam,max.diam,length.out=101)

## casy data IDs:
str.A <- c("EVC-A",
           "EVC+A",
           "TOPA_A-",
           "TOPA_A+",
           "GYRA_A-",
           "GYRA_A+")
str.B <- c("EVC-B",
           "EVC+B",
           "TOPA_B-",
           "TOPA_B+",
           "GYRA_B-",
           "GYRA_B+")
STRAINS <- strains
names(STRAINS) <- toupper(strains)

atimes <- 0:4 ## all sampling days, x-axis

str.lst <- list(A=str.A,
                B=str.B)


## summary: all volume distributions, total counts and volume

for ( rpl in names(str.lst) ) {

    rpl.lst <- str.lst[[rpl]]

    plotdev(file.path(fig.path,paste0(expid,"_casy_",rpl)),
            width=4, height=4, res=300, type=hfig.type)
    par(mai=c(.05,.05,.05,.05), mgp=c(1.2,.3,0), tcl=-.25,yaxs="i")
    layout(cbind(rep(7,4),rbind(1:2,3:4,5:6,7:8),rep(7,4)),
           widths=c(.2,1,1,.2), heights=c(1,1,1,.2))
    for ( i in 1:length(rpl.lst) ) {
        
        eid <- rpl.lst[i]
        casy <- casyl[[eid]]
        STR <- STRAINS[sub("[-|+].*","",sub("_.*","",eid))]

        inducer <- ifelse(length(grep("\\+",eid))>0,TRUE,FALSE)
        
        diax <- casy$diameter
        volx <- casy$volume
        counts <- casy[,-(1:2)]
        
        filter <- diax>min.diam & diax<max.diam
        counts <- counts[filter,]
        diax <- diax[filter]
        volx <- volx[filter]
        
        cids <- colnames(counts)
        stimes <- as.numeric(cids)-1
        colnames(counts) <- stimes
        
        ## smooth
        counts <- apply(counts,2, function(x) ma(x, 15))
        counts[is.na(counts)] <- 0
        
        ## normalize per column: relative frequency
        counts <- apply(counts,2, function(x) x/max(x))
        
        ## EXPAND to all sample times
        CNTS <- matrix(0,nrow=nrow(counts), ncol=length(atimes))
        colnames(CNTS) <- atimes
        CNTS[,colnames(counts)] <- counts
        
        if ( i%%2==0 )
            par(mai=c(.05,.05,.05,.05), xaxs="i")
        else
            par(mai=c(.05,.05,.05,.05), xaxs="i")
        
        ## plot
        image(x=atimes, y=volx, z=t(CNTS), ylim=c(0,29),
              col=cols, breaks=seq(0,1,length.out=length(cols)+1),
              xlab=NA, ylab=NA, axes=FALSE, xaxs="i",xlim=c(-.5,4.5))
        legend("topleft", expl[STR], text.col=ecols[STR], cex=1.2,
               text.font=2, box.col=NA, bg=NA, seg.len=0, x.intersp=0)
        if ( i%in%1:2 ) 
        legend("top", ifelse(inducer,"+inducer","-inducer"),
               text.col=1, cex=1.2,y.intersp=0,
               text.font=2, box.col=NA, bg=NA, seg.len=0, x.intersp=0)

        axis(3, labels=FALSE)
        axis(2, labels=i%%2!=0, las=2)
        axis(1, labels=i>4)
        if(i==1)
        legend(x=-.5, y=23, c("cell count","total volume"),
               lty=2:1,pch=c(19,1),bty="n")
        if(i==3) mtext(expression("cell volume"~V[cell]*","~ fL), 2,
                       par("mgp")[1]*.95)
        if(i==6) mtext("days", 1, par("mgp")[1]*1.1, adj=-.2,xpd=TRUE)
        box()
        
        yn <- casys[[eid]][,"cells/mL"]/1e8/2
        yv <- casys[[eid]][,"volume,uL/mL"]
        par(new=TRUE,xaxs="i")
        plot(stimes, yn, col=ecols[STR],lty=2,
             ylim=c(0,6), type="l",xlim=c(-.5,4.5),
             axes=FALSE,ylab=NA,xlab=NA)
        points(stimes, yn, col=ecols[STR], pch=19)
        lines(stimes, yv, col=ecols[STR], lty=1)
        points(stimes, yv, col=ecols[STR], pch=1)
        axis(4, labels=i%%2==0, las=2)
        if(i==4)
            mtext(expression(5*"x"*10^7~cells/mL~" & "~"total volume,"~mu*L/mL),
                  4, par("mgp")[1]*1.4)
    }
    dev.off()
}

## CASY vs. MICROSCOPY
## only the A series were used for microscopy


## check again if casy data all have the same diameters
sze <- casyl[[1]][,"diameter"]
if ( any(unlist(lapply(casyl, function(x) sum(x[,"diameter"]!=sze)))>0) )
    stop("error casy diameters do not match")

## ... and collect all casy size distributions from day 5
cdia <- do.call(cbind, lapply(casyl, function(x) x[,"5"]))

## MICROSCOPY SIZE DENSITY DISTRIBUTION AT CASY BINS

## peak width/length via hist function
micresd <- matrix(NA,nrow=length(nmap),ncol=2)
colnames(micresd) <- c("Dia","Axis")
rownames(micresd) <- names(nmap)
for ( i in 1:length(nmap)) {

    ## experiment IDs in microscopy and CASY
    eid <- names(nmap[i])
    topAp <- micro$exp==eid
    exp <- nmap[i]
    
    ## get length densities at casy sizes
    dns <- density(micro$Axis[topAp])
    mld <- approxfun(dns)(sze)
    mld <- mld/max(mld,na.rm=TRUE) # norm to 1

    ## get width densities at casy sizes
    dns <- density(micro$Dia[topAp])
    mwd <- approxfun(dns)(sze)
    mwd <- mwd/max(mwd,na.rm=TRUE) # norm to 1

    ## get peak size: cell size with maximal count
    ## the density distribution 
    micresd[eid,"Axis"] <- sze[which.max(mld)]
    micresd[eid,"Dia"] <- sze[which.max(mwd)]
    
    ## norm casy numbers to max dens = 1
    ## first filter by cutoffs!
    cdc <- cdia[,exp]
    cdc <- ma(cdc,20)
    filter <- sze>min.diam & sze<max.diam
    cdc <- cdc/max(cdc[filter])
    
    plotdev(file.path(fig.path,paste0("microscopy_casy_densities_",eid)),
            type=fig.type, width=2,height=2, res=300)
    par(mai=c(.35,.35,.1,.1), mgp=c(.8,.4,0), tcl=-.25)
    plot(sze,cdc,pch=19,xlim=c(1,5),col="gray",
         xlab=dlb, ylab="norm. dens.", cex=.25,axes=FALSE)
    taxis(ylim=c(0,1), xlim=c(1,5))
    
    abline(v=c(min.diam,max.diam),col=1, lty=2)
    lines(sze,mld, col=2, lwd=3)
    lines(sze,mwd, col=4, lwd=3)
    lines(sze[filter],cdc[filter], col=1, lwd=3)
    #polygon(x=c(sze,rev(sze)),y=c(mld,rep(0,length(mld))),col=3, border=NA)
    ##polygon(x=c(sze,rev(sze)),y=c(cdc,rep(0,length(cdc))),
    ##        col="#00000099", border=NA)
    ##points(sze,cdc,pch=19, col=ifelse(filter,"black",NA), cex=.25)
    if ( eid == "EVC-" )
        legend("right", c("width","length","CASY"),
               col=c(4,2,1), lwd=3, bty="n", seg.len=.5)
    STR <- sub("[-|+]","",eid)
    ind <- ifelse(length(grep("\\+",eid))>0,"+ ind.","- ind.")
    legend("topright", legend=c(expl[STR],ind), text.col=ecols[STR],bty="n")
    dev.off()
}

## alternative: calculate peak diameter from microscopy
## via hist function; NOTE: currently not useed

## peak width/length via hist function
micres <- list()
for ( ind in unique(micro$inducer) ) {
    for ( str in unique(micro$strain) ) {
        mdat <- micro[micro$strain==str & micro$inducer==ind,]
        hst <- hist(mdat$Dia, breaks=mbrks)
        pk.dia <- hst$mids[which.max(hst$counts)]
        abline(v=pk.dia)
        hst <- hist(mdat$Axis, breaks=mbrks)
        pk.axis <- hst$mids[which.max(hst$counts)]
        micres[[paste0(str,ind)]] <- c(Dia=pk.dia, Axis=pk.axis,
                                       Dia.mean=mean(mdat$Dia),
                                       Axis.mean=mean(mdat$Axis))
        
    }
}
micres <- do.call(rbind, micres)

## get peak diameters from casy report
## TODO: alternatively use the same approach
## as for microscopy
casres <- do.call(rbind, lapply(casys, function(x)
    x[nrow(x),grep("Peak",sub(" .*","",colnames(x)))]))

## get casy data in same order as microscopy
cdia <- casres[nmap[rownames(micresd)],2]


## PLOT SIZE DISTRIBUTIONS
## microscopy vectors for plotting
maxi <- micresd[,"Axis"]
mdia <- micresd[,"Dia"]
mbth <- apply(cbind(maxi,mdia),1,mean) # mean of axis and diameter

## plot
pchs <- rep(1,length(mdia))
pchs[grep("\\+",names(mdia))] <- 4
cols <- ecols[sub("[+|-]","",names(mdia))]

plotdev(file.path(fig.path,"microscopy_casy_width"), type=fig.type,
        width=Wc,height=Hc, res=300)
par(mai=c(.35,.35,.1,.1), mgp=c(.7,.4,0), tcl=-.25)
plot(mdia, cdia, xlim=c(1,3),ylim=c(1,3),
     xlab=expression(width~peak/mu*m),
     ylab=expression(CASY~peak /mu*m),
     col=cols, pch=pchs, lwd=2, axes=FALSE)
taxis(xlim=c(1,3))
fit <- lm(cdia~mdia)
abline(fit)
abline(a=0,b=1,lty=2)
legend("topleft",paste(c("+","-"),"ind."), pch=c(4,1),bty="n",
       lwd=2 ,lty=NA, seg.len=.5, x.intersp=0.5)
legend("bottomright",paste0("r2=",round(summary(fit)$r.squared,2)),
       bty="n", lty=1)
dev.off()

plotdev(file.path(fig.path,"microscopy_casy_length"), type=fig.type,
        width=Wc,height=Hc, res=300)
par(mai=c(.35,.35,.1,.1), mgp=c(.7,.4,0), tcl=-.25)
plot(maxi, cdia, xlim=c(1,3),ylim=c(1,3),
     xlab=expression(length~peak/mu*m), ylab=expression(CASY~peak/mu*m),
     col=cols, pch=pchs, lwd=2, axes=FALSE)
taxis(xlim=c(1,3))
fit <- lm(cdia~maxi)
abline(fit)
abline(a=0,b=1,lty=2)
legend("topleft",paste(c("+","-"),"ind."), pch=c(4,1),bty="n",
       lwd=2 ,lty=NA, seg.len=.5, x.intersp=0.5)
legend("bottomright",paste0("r2=",round(summary(fit)$r.squared,2)),
       bty="n", lty=1)
dev.off()

plotdev(file.path(fig.path,"microscopy_casy_mean"), type=fig.type,
        width=Wc,height=Hc, res=300)
par(mai=c(.35,.35,.1,.1), mgp=c(.7,.4,0), tcl=-.25)
plot(mbth, cdia, xlim=c(1,3),ylim=c(1,3),
     xlab=expression(mean~peak/mu*m),
     ylab=expression(CASY~peak/mu*m),
     col=cols, pch=pchs, lwd=2, axes=FALSE)
taxis(xlim=c(1,3))
fit <- lm(cdia~mbth)
abline(fit)
abline(a=0,b=1,lty=2)
legend("topleft",paste(c("+","-"),"ind."), pch=c(4,1),bty="n",
       lwd=2 ,lty=NA, seg.len=.5, x.intersp=0.5)
legend("bottomright",paste0("r2=",round(summary(fit)$r.squared,2)),
       bty="n", lty=1)
dev.off()


### 8-SHAPED CELL DISTRIBUTION
mcnt.file <- "20220323_Cellcount_Ovs8.xlsx"
mcnt <- readxl::read_excel(file.path(ipath,expid, "Data",mcnt.file))
## cut to later data table
use.new <- TRUE
if ( use.new )  { mcnt <- mcnt[,9:15]
} else {    mcnt <- mcnt[,1:7] }
## get numeric data and fix names - rm ^kd/^OX to match with names used here
mids <- sub("\\^.*[^+|-]","", unlist(mcnt[2:nrow(mcnt),1]))
mcnt <- data.frame(mcnt[2:nrow(mcnt),2:ncol(mcnt)])
mcnt <- apply(mcnt,2,as.numeric)
rownames(mcnt) <- mids
colnames(mcnt) <- c("round","round%","double","double%","total","slides")

## map to strains and inducer
ind <- rep(c("-","+"), length(strains))
STR <- paste0(rep(strains,each=2),c("-","+"))

plotdev(file.path(fig.path,"microscopy_8shaped_manual"), type=fig.type,
        width=Wc,height=Hc, res=300)
par(mai=c(.5,.45,.1,.05), mgp=c(1.3,0,0), tcl=0)
barplot(mcnt[STR,"double%"], ylab="% 8-shaped cells",
        axes=FALSE, names=ind, col=ecols[rep(strains,each=2)],
        ylim=c(0,25))#, width=.1,xlim=c(0,3.75))
par(mgp=c(1.3,.4,0), tcl=-.25)
axis(2, at=seq(0,100,5))
axis(4, at=seq(0,100,5),labels=FALSE)
xlim <- par("usr")[1:2]
xat <- (seq(xlim[1],xlim[2],length.out=4)+diff(xlim)/6)[1:3]
par(mgp=c(0,par("mgp")[1],0),tcl=0)
tmp <- Map(axis, 1, at=xat, labels=expl[strains],
           col.axis=ecols[strains],col=NA, tcl=0)
legend("topleft","manual",bty="n", y.intersp=0,x.intersp=0,seg.len=0)
dev.off()

plotdev(file.path(fig.path,"microscopy_8shaped_auto"), type=fig.type,
        width=Wc,height=Hc, res=300)
par(mai=c(.5,.45,.1,.05), mgp=c(1.3,0,0), tcl=0)
barplot(frc8[STR]*100, ylab="% 8-shaped cells",
        axes=FALSE, names=ind, col=ecols[rep(strains,each=2)],
        ylim=c(0,25))#, width=.1,xlim=c(0,3.75))
par(mgp=c(1.3,.4,0), tcl=-.25)
axis(2, at=seq(0,100,5))
axis(4, at=seq(0,100,5),labels=FALSE)
xlim <- par("usr")[1:2]
xat <- (seq(xlim[1],xlim[2],length.out=4)+diff(xlim)/6)[1:3]
par(mgp=c(0,par("mgp")[1],0),tcl=0)
tmp <- Map(axis, 1, at=xat, labels=expl[strains],
                col.axis=ecols[strains],col=NA, tcl=0)
legend("topleft","automatic",bty="n", y.intersp=0,x.intersp=0,seg.len=0)
dev.off()

### SPECTRA
spectra.files <- c(
    "20220125_JB_EVC_gyrA_Spectrum.csv",
    "20220126_JB_EVC_gyrA_Spectrum.csv",
    "20220127_JB_EVC_gyrA_Spectrum.csv",
    "20220128_JB_EVC_gyrA_Spectrum.csv",
    "20220129_JB_EVC_gyrA_Spectrum.csv",
    "20220215_JB_topA_gyrA_Spectrum.csv",
    "20220216_JB_topA_gyrA_Spectrum.csv",
    "20220218_JB_topA_gyrA_Spectrum.csv",
    "20220219_JB_topA_gyrA_Spectrum.csv")


### PROTEIN CONTENT in extracts, measured by Lutz Berwanger
## from endpoint samples taken by Jonas Burmester,
## experiment series  20220125_JB (EVC) and 20220215_JB (gyrA/topA);
## see file ~/data/synmibi/Clariostar/20220324_BCA_coilhack_20220322_.txt
## TODO: get more recent measurement from Lutz and plot
proteins <- c(2592.098, # gyrA A-
              2676.289, # gyrA A+
              2036.674, # gyrA B-
              2676.289, # gyrA B+
              1650.801, # topA A-
              2170.811, # topA A+ 
              2433.907, # topA B-
              2166.886, # topA B+
              502.452,  # EVC -
              749.26)   # EVC +
prt.file <- "20220325_ProteinQuant_GyrA_TopA_LCB.xlsx"
prt <- readxl::read_excel(file.path(ipath,expid,prt.file)) 
                                    
