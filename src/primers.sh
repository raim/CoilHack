
blastn -query sequences/primers_6803.fasta -db $PCC6803DAT/chromosomes/pcc6803.fasta  -task "blastn-short" -max_target_seqs 1 -outfmt 6 | sort -t$'\t' -k1,1 -k4,4gr  | sort -u -k1,1 --merge &> sequences/primers_6803_blast.tab

blastn -query sequences/primers_7942.fasta -db $PCC7942DAT/chromosomes/pcc7942.fasta  -task "blastn-short" -max_target_seqs 1 -outfmt 6 | sort -t$'\t' -k1,1 -k4,4gr  | sort -u -k1,1 --merge &> sequences/primers_7942_blast.tab

## TODO: format output to be read directly by genomeBrowser; see functions
## used for gI intron and coilseq:puc19 projects

