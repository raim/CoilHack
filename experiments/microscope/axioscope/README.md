# Trouble-Shooting the Axioscope & ZEN Software

## ZEN Software doesn't find/connect to the microscope camera

This can happen after a Windows update.

1) Insert Zeiss Installation CD 
2) Browse to to `Camera > AxioCamIC > x64`, 
3) Run the installer for the camera driver `SvcInst.exe`

Note that a copy of the installer is also available at
`Studierende\Manuals\Zeiss_Axioscope`.
