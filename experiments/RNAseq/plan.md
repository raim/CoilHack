# Selection of Read Length & Coverage

@Rey2010 analyze the required RNA-seq coverage for two bacteria with
L=3.6 MB and L=4.5 MP and got *1 million mRNA reads seems to be ideal:
replicates are maximally correlated and fold-changes can be detected
down to modest levels (3-6 fold). However, even 300,000 mRNA reads per
sample will provide highly correlated replicates capable of detecting
10-fold changes with high accuracy and 6-fold changes with >80%
accuracy*; and *read lengths of 36 nucleotides were more than
sufficient to accurately assign a transcript (cDNA) to a given locus
in the genomes.*

