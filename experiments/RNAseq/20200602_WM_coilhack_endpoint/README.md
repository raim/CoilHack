# RNAseq data from CoilHack pre-experiment

Endpoint measurements of GyrA, GyrB knock-down and topA overexpression vs. EVC 
(Wandana Murugathas, master thesis).

## Directories

* `samples`: experimental data of RNAseq samples,
* `fastq`: fastq files of all sequencing reads,
* `features`: raw read-count data for features, provided by Tobias Busche (Bielefeld),
* `deseq2`: DeSeq2 analysis of strains vs. empty vector control,
* `scripts`: scripts that analyse the data,
* `analysis`: summary result tables and figures (produced by `scripts/run.sh`).

## Details

### File `samples/Gel 2020-06-08 16hr 13min_RNA_gel_08.06.2020.tif`

gel from RNA extracts used for qPCR and RNAseq,
order: EVC, kd_gyrA, kd_gyrB, ox_topA, each 3x


