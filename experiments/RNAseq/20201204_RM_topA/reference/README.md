* NCBI Genbank files for genome and all plasmids from Synechocystis sp. PCC6803,
    - NC_000911.1: genome
    - NC_005230.1: pSYSA
    - NC_005231.1: pSYSG
    - NC_005229.1: pSYSM
    - NC_005232.1: pSYSX
    - CP003270.1: pCA2.4_M
    - CP003271.1: pCB2.4_M
    - CP003272.1: pCC5.2_M
* Genbank file for topA overexpression plasmid:
    - pSNDY_Prha_topA-6_119rhaS.gb
