# How To Use the Lambda Minifor Photobioreactor


This document provides only a summary of important points as well as
instructions for our specific setup. For detailed instructions consult
the official manual:

`Operation_manual_of_LAMBDA_MINIFOR_laboratory_Fermentor_and_Bioreactor.pdf`


# Reactor Set-Up & Sterilization


## Reactor Vessel Setup

* Probes:
    - Use the **red silicone stopper for the O2 probe**, and the normal (white/transparent) stoppers
      for pH and all other probes.
    - Inspect probes for damage: see Figure \ref{fig:pO2membrane} for pO2 probe
    and [TODO: specify for pH],

## Reactor Vessel Sterilization

* Tubing: 
    - Add long tubing for feed, acid and base pumps, close them with Luer locks.
    - **Clamp off**: all tubing must be clamped off **close to the reactor vessel**, otherwise they will be filled with medium and the medium can block attached filters.
* Probes:
    - pH: **always calibrate pH probe before autoclaving**.
    - pO2 and pH: DO NOT put on the little rubber hats of the probe contacts. These can not be autoclaved. Make
      sure the contacts are clean (**never touch contacts** with bare skin),
      and wrap in alu foil.
    - OD4 DasGIP probe: has a little hat to be screwed on.
    - pH probe should be calibrated BEFORE autoclaving.
* Attach Stirrer: **critical**, the stirrer or its rubber sealing often get loose during autoclaving. When screwing on the stirrer, make sure that the rubber seal lies flat on the glas reactor, and that the stirrer itself extends straight into the reactor. The motor with magnetic coupling can be used to press on the stirrer before screwing it thight. 
* Additionally autoclave:
    - Tubing and bottle for feed medium,
    - The sterile sampling device incl. all tubing,
    - pH corrrection bottles and tubing, if the pH
    is too low to keep them sterile.
* **AFTER AUTOCLAVING**:
    - make sure the agitation unit is still skrewed on tightly and the rubber sealing has not slipped off the glass vessel. Fix in clean bench if necessary, and test by attaching to the reactor base unit and start agitation.
    - make sure all airline filters are free by pumping through one volume air of a 50 mL syringe. This should go very easy without any resistance. If a filter is blocked, let them dry and try again or replace it under the clean bench or with lots of 75% ethanol.


## Reactor Vessel Setup in Base Unit

* Make sure there is no water on the bottom of the glass 
vessel or in the hollow of the heating spiral on the base unit.
* Place the Lumo light jacket in its position.
* Carefully lower the glass vessel into its position.
* Attach pH and Oxygen probe cables, and the agitation unit motor.
* Test proper attachment of the agitation unit by starting up
the reactor: press button "R" on the base unit.

We sometimes noticed a **loud pop** after starting up the reactor
the first time. This may likely be due to liquid in the hollow below
the vessel that houses the heating spiral and may stem from water
evaporating after turning on the heating. If you notice this pop
make sure all filters are unblocked and the rubber around
the high-pressure valve is not rippled.

## Cooling System Setup

For all water circulation thermostats, remember: *cooling liquid flows
in at the bottom and out at the top*.

1. Prepare cooling liquid:  0.1 g/L Na$_2$CO$_3$ sodium carbonate in deionized
water. **Never use pure deionized water**, since this will lead to corrosion
of the metal. **Never use tap water** since this will lead to calcification.
2. Fill the circulation thermostat with the cooling liquid.
3. Establish direction of water flow from the circulation thermostat.
4. Attach outlet of the circulation thermostat to the bottom inlet
of the offgas cooler.
5. Connect the top outlet of the offgas cooler to one of the ports
of the cooling finger.
6. Connect the other port of the cooling finger to the inlet tube
of the circulation heater.
7. Make sure all connections are water tight.
8. Switch on the circulation thermostat water flow to test, and
add more cooling liquid if required.
9. Set the bath temperature to 16°C and pump speed to 4.

NOTE: it would be better to have two different cooling systems
for offgas and cooling finger, and to use a lower temperature
for the offgas cooler than for the cooling finger.

## Liquid Lines: Acid & Base, Feed & Waste

1. Connect tubing for acid, base, feed and waste lines,
2. Use ethanol to sterilize 3-way valves and use them
as connectors for feed and waste lines. These will be used
for inoculation and induction, and for sampling, respectively.
**All open lines should be sterilized with ethanol.**
3. Connect the sampling port to the (autoclaved) sterile sampling
device. 
2. Insert tubings in peristaltic pumps,
3. Try to pre-fill all lines by manual start of the peristaltic pumps.

### H2SO4, 0.5 M, 200 mL: 

Recipe for H2SO4, 95% (w/w), density: 1.84 g/mL, MW: 98.07 g/mol:

For 200 mL of 0.5 M H2SO4 solution, we need 0.1 mol. The molar
concentration of the 95% (w/w) stock solution is
$1840\times0.95/98.07=17.824$ mol/L. Thus we need $0.1/17.824 L = 5. 6
mL$ of the stock solution.

1. Add 180 mL milliQ to measuring cylinder,
2. **Work under fume hood**,
3. Pipet 5.6 mL of 95% H2SO4 ($0.1/()$ mL) to milliQ,
4. Fill up to 200 mL with milliQ.


## Gas Lines: Ingas and Offgas



## PBR Server and External Modules

Additional data is calculated by variable add-on modules. All
data from the Lambda reactor and the add-on modules is
stored in directory `Server_Data`.

How to use our additional measurement modules, and
the module data collections server by Louis Goldschmidt at
`git@gitlab.com:L-Gold/PBR-Main.git`.

### Offgas Module: *gasometer*

### Continuous Culture Balances: *liquometer*

### Avantes Spectrometer: *lightometer*

### DASGIP OD4 Module

\newpage

# Reactor Start-Up & Run


## Start All Software

Plug in all USB cables. It may not be required but its best to plug in
the Lambda reactor directly on the laptop, while all others can be
plugged in to the USB hub. **Make sure the USB hub has an external
power supply, and always keep this plugged in**, the external
modules should not rely on power from the server laptop.

* Move old data storage folder,
* Create fresh empty folder for data storage as 
`C:\Users\SynMibi010\Documents\Server_Data`,
* Start the manual log file `LOG.csv`  in `Server_Data`,
* `Lea`: Lambda reactor, setup data storage in the `Save data` field
via the `Config` button, choose `Server_Data` and set file name
to `Lambda_Data.txt`
* `get red-y`: Voegtlin CO2 Mass Flow Controller
* `XFM Communication Utility`: Aalborg
* `DASGIP DTP and OPC Monitor` &  `EasyAccess 4`: OD probe
* `PBR Server` & Arduino Scales Module:
    - Plug out & in USB power connection for the Arduino without
    any weight. At start-up a tare to 0.0 g is performed.
    - Make sure the PBR Server is running to record all data with 
    correct time stamps. TODO: HOW!
    - Weigh a volumetric flask with a lab scale and record the weight.
    - Place volumetric flask on Arduino scale and record the time
    and weight of the flask in the `LOG.csv` file. In the ID column 
    use W1 for the large and W2 for the small scale and add weights
    in the value column.
    - Add defined volumes of water to the flask, step by step, let
    it reset for 1 minute, then proceed to the next step. Record
    all times and added volumes (in gram) in `LOG.csv`.

## Equilibration

Before starting anything, revisit the points above **AFTER
AUTOCLAVING**: is the agitation unit attached properly? Are all
filters free? Is there any water in the heating spiral hollow between
vessel and base unit?

For CO2-enriched cultures it is advicable to let culture parameters
stabilize step by step before a final N2 flush an inoculation. This
procedure should be recorded by the Lea reactor software, and the
offgas and scale Arduino modules, since it allows to calculate some
basic parameters of the reactor configuration.

1. Set cooling system and reactor temperatures, aeration (air only!) and 
agitation to desired culturing parameters, (usually for *Synechocystis*: 
cooling system T$_\text{bath}$=17°C , reactor T$_\text{culture}$=30°C, Aer.=1
L/min, Agit.=5 Hz), but keep pH control pumps **off**, and let it run
(press "R" button on base unit) for about 1 day until pH and T have
stabilized.
2. Add CO2 mass flow (usually 5 mL/min or 0.5 % of air) and wait
until the new lower pH has stabilized.
3. Set pH to the desired culturing value (pH 8) and activate **ONLY**
the base (NaOH) pump; keep the acid pump deactivated to avoid
oscillations of the pH control loop. The set pH will be reached soon,
but the system will keep adding base for several hours. This is due to
the very slow equilibration between CO$_2$ and bicarbonate (HCO$_3^-$)
at higher pH. Place the NaOH bottle on the scale to record the added
amount of NaOH and follow the activity of the base pump.
4. After no more NaOH is added and pH is stable, also activate
the acid (H$_2$SO$_4$) pump. Proceed to calibration of O2 probe and 
the N2 flush.


## O$_2$ Probe Calibration

**NOTE**: the N2 flush after pO2 probe calibration and the scale
should be recorded to allow for potential post-processing calibration.
If the system is aerated by CO2-enriched air, just set the CO2 to
the desired value and leave on during the flush.

* The pH probe has to be calibrated BEFORE autoclaving. Check the pH
by taking a sample and measuring with an external pH probe.
* Calibrate the pO2 probe according to the Lambda manual, plugging out
  the cable for 0% calibration.
* N2 flush as control and for potential post-calibration of data
and/or calculation of the mass transfer coefficient (k[L]a):
    - Switch off pH pumps (pH will go up during N2 flush due 
	  to removal of CO2!),
    - Close Air valve, then open N2 valve,
    - Flush reactor with N2 until O2 and optionally pH (takes much longer) 
	  measurements have stabilized, at least 15 min. 
    - Close N2 valve, open Air and CO2 valves.
    - Wait until pH becomes stable again, only THEN re-activate pH pumps.
	

## Inoculation & Run

* Pre-Culture:
    - Measure OD, and calculate amount of culture required for
   a starting OD in the reactor (1 L volume),
    - Spin down culture (XXX rpm), discard supernatant and 
   resuspend in fresh BG11 medium.
* Switch off aeration before inoculation to reduce pressure.
* Spray Ethanol on the inoculation port.
* Release over-pressure by opening inoculation port valve.
* Attach syringe and quickly inject culture.
* Spray open port and quickly close the port.
* Reactivate aeration.
* During the run:
    - Add marks feed bottles and note the date in LOG.csv for 
	post-calibration of scales, see section on Calibrations for details.
	


## Loss of Communication with Arduino Modules

1. In the `IPython` terminal, stop the Server by clicking `ctrl-c` (`Strg-c`)
and check which ports are still actively transferring data in the log messages,
2. In the Windows device manager (Gerätemanager) go to the `COM & UTP` tree,
find the missing Arduino port named eg. `Arduino Mega 2560 (COM6)`.
3. Right-click this device and in the menu click `deactivate` (`deaktivieren`),
4. Right-click again and click `activate` (`aktivieren`),
5. In the `IPython` terminal, press `arrow-up` key to get to
the server start command and press `enter` to restart the Server.

## Laptop Re-Boot During Run

During long runs the reactor laptop can loose connection to the
SynMibi webdav storage (for data back-up and online data display)
and to the Aalborg offgas massflow meter. Also the OD4 module can
loose contact to the recording software (Easy Access) and the last
updated value is stored at subsequent timepoints. The latter problem
can fixed by stopping the recording in Easy Access, reloading the
interface (press reload button) and re-starting a new
recording. However, we so far found no way to restore communication
with the offgas sensor and to the SynMibi webdav. To reactivate
these, we need to re-boot the reactor laptop. This inevitably leads
to loss of data. Thus, the following procedure requires to be done
quickly.

### No or minimal Data Loss

* R Studio:
    - `Stop` the live plotting loop by clicking the `stop` symbol in the
    R terminal panel,
    - Close software.
* Close PBR Python Server for Arduino modules:
    - Go to the ipython terminal (`IP` symbol in task bar),
    - Press `ctrl-c` (Strg-c) to stop server,
    - Close terminal.
    - NOTE: the longer the server is offline, the longer it will take
    to transfer data at the next connection. During this time (can
    be several minutes to an hour) no values can be stored on the
    Arduino modules.

### Data Loss: act quickly from here!

Proceed in the following order which reflects the importance of
recorded data.

* CamDesk Video Recorder: close the window (right-click on foto/video).
* Aalborg offgas mass flow meter:
    - Click `Start/Stop PI Update for current device`  ("play" button)
    in the `XFM Control Terminal Utility` (yellow symbol with blue A),
    - Click `CloseA` and `ClosePI` to save recording (folder symbol buttons).
    - Close Software.
* DASGIP OD4 Module:
    - Click `Stop recording` in `Easy Access 4` interface, select
    the current csv file, click `Stop recording` and close Easy Access Window,
    - Close `DASGIP DTP and OPC Monitor` window.
* Lea Reactor Software:
    - Deactivate recording by clicking `On` button in the `Save data` menu,
    - Close `Lea` software.
* Reboot laptop, login and restart all of the above software. **The following
steps are required to avoid overwriting of previously recorded data**!
    - PBR Python Server for Arduino modules: restart the IPython terminal
    and use the `arrow up` key to find the previous commands (TODO: give
    detailed commands above in reactor start-up protocol and refer to
    this here):
        - change to the directory where the PBR-Main software lies,
	- import the `nexus` module,
	- re-start the server.
    - OD4: in the `Easy Access 4` Interface, select `start recording`
    and choose the next higher index number for the csv file, eg.,
    if file `OD4_3.csv` exists, choose `OD4_4.csv`.
    - Lea: in the `Save data` window click `config` and
    select a new file name by adding a `1` with one more `1` than
    the last recorded data file, ie., if the last file was `Lambda_Data.txt111`,
    choose `Lambda_Data.txt1111` as the new file name.

# Set-Up Online Viewer

* On Reactor Laptop, Windows:
    - Connect the laptop to the SynMibi server. DO NOT use webdav,
	but use a direct connection: choose a drive (eg. Y:) where
	the server is to be mounted. Enter `\\Synmibi.ad.hhu.de\daten` 
	as directory ("Ordner"). Use your HHU account for identification
	but prepend `ad\`, eg. `ad\<username>` and enter your password.
    - Create a **target folder** on the SynMibi server:
directory `Studierende/DATA/Lambda/<TARGET>`,
    - Edit the `copydat.bat` Windows batch file in `Server_Data` to copy
	all required data to this **target folder**,
* Set up Windows scheduler (german "Aufgabenplaner") to execute `copydat.bat` 
every 10 min:
    - Start Windows scheduler (german "Aufgabenplaner"),
	- Click "Neue Aufgabe erstellen", give it a recognizable name, eg. 
	`lambdadata`,
	- Tab "Allgemein": select to only run when the user is logged in
	("Nur ausführen, wenn der Benutzer angemeldet ist"),
	- Tab "Aktion": select the script `copydata.bat` you just prepared,
	- Tab "Trigger": tick "taeglich" and select a proper time, eg. a full hour, 
	and below at "Wiederholen jede:" select "10 Minuten" from drop-down menu.
* On local Linux computer:
    - Make sure the SynMibi server is mounted, in this example at 
	`/mnt/synmibi/`,
    - Create local folder, to keep track of data choose the same
	name as on the server,
	- Create bash script `copydata.sh` (optionally in this new local folder
	to keep everything togetheter), with a simple call to `rsync`:
	`rsync -avz /mnt/synmibi/Studierende/DATA/Lambda/<TARGET>/ ~/work/experiments/<TARGET>`,
    - Set up cronjob via `crontab -e` to run the copydata.sh script, to decrease the delay in the pipeline choose the times a few minutes after the job on Windows machine runs, eg. with this entry:
	`3,13,23,33,43,53 * * * * ~/work/experiments/<TARGET>/copydata.sh`
* Copy to server in Leipzig:
    - At the Leipzig server create a new folder 
	`public_html/lambda/<TARGET>`, 
	- Copy and adapt the paths (local data folder and remote paths) in (a) 
	the plotting script `Liveplot.R` from the `PBR-Main` git repo, and (b) 
	scripts `offline.R` and `upload.R` from previous runs to the 
	local data folder,
	- After adding of the ssh key for the Leipzig server (`ssh-add`), 
	run the scripts `Liveplot.R` and `upload.R` from a local terminal
	window.


# Calibrations and Post-Calibrations

## LUMO Light Module Calibration

* LiCor xyz - light measurement bulb,
* Set light values on LUMO, record values shown on LiCor,
* Perform linear regression, if appropriate force through 0,
in R: `lm(value ~ 0 + set)`.

![Light calibration from June 2020.\label{fig:light}](light_calibration.png){width=50%}

## DASGIP OD4 Module Calibration

![Example calibration of OD4 to external OD measurement, Synechocystis sp. PCC6803, wild type, Jan 2020](OD4_calib_scystis-wt_202001.png){width=50%}

## Scales & Dilution Rates

* During a run with active feed pump or pH control, mark the liquid
level in the feed, acid/base or waste bottles with roman numbering and
note the date in LOG.csv, eg: `202012017_14:00:00;W1mark;I;mark on
bottle on scale W1`. 
* When the bottle is empty, measure the weight of
water added to this mark and enter the weight in LOG.csv as
`202012017_14:00:00;W1;745;g, weight of bottle on scale W1.`
* Use this data to calculate dilution rates, and/or acid/base addition
rates manually, or to recalibrate the data of the scales which
record to bottle weight online.


# Maintenance & Miscellaneous

## Clean Probe Contacts

The contacts of the probes need to be clean or will produce wrong
values.  If you touched the contacts of pH, pO2 or OD4 probes, use a
Kim wipe with ethanol to gently clean.

## Switching between the Hamilton Visiferm O2 and the Lambda O2 probe

Switching probes requires to enter the correct factor
for Temperature correction on the Lambda base station. 


See the document 
`Helpdesk for the use of VISIFERM pO2 probe with the MINIFOR.pdf` or 
the instructional video at  \url{www.flickr.com/gp/lambdacz/4gKg8S} for 
a guide how to switch between O2 probes, and replace the entered 
4-digit value, the temerapture correction factor, by the one of the 
probe to be used. (TODO: add instructions in this document directly?)

The Hamilton Visiferm probe has its own connecting cables, with
earth to be attached to the reactor and its own power supply.


Correction factors for different probes
    - Hamilton Visiferm: 4269
    - Lambda pO2: 3540


## Peristaltic Pump Tubing	

The tubes can break at the entry/exit points of the peristaltic pumps,
which is especially dangerous for the acid/base correction fluids.

To avoid such damage:

* Please follow the instructions at
\url{https://www.lambda-instruments.com/de/schlauchpumpen-labor/bedienungsanleitung/1-inbetriebnahme-der-schlauchpumpe/#tubing-insertion} in detail,
* Before each run apply *Vaseline* to the bottom side of peristaltic pump
lid.

# Reactor Shut-Down

TL;DR: **Think**, **Release**, **Clamp Off** and wear **Protection**

## SAFETY NOTES:

* Reactor may be **under pressure**!
* Liquid tubing may contain **hazardous liquids** (acid/base, 
medium with antibiotics, genetically modified organisms)!
* Therefore:
    - **Release over-pressure** from reactor.
    - Use **lab coat, red gloves, and safety goggles** at all times!
    - **Think** before opening any connections!
    - **Clamp off** tubing before removing from peristaltic pump heads,
      or opening any closed lines (such as water cooling).

## DETAILED PROCEDURE

1. Close software on Laptop:
    - To allow for post-calibration of gas probes, run a final
      N2 flush,
    - Set all gas flow to 0 in `Lea` and `get-redy5` (Voegtlin),
	- Optionally empty reactor via Waste pump: set speed to 999
	and **disconnect ingas or open sampling valve** to allow pressure
	equilibration during pumping,
    - Stop file save in Lea (reactor software) [TODO: which button?],
    - Stop recording in Aalborg software `XFM Control Terminal Utility` 
      (press play button),
    - "Stop Recording" (button) and **save recorded** files in DasGip 
      `EasyAccess4` interface,
    - Stop Python server in `IPython` terminal (press Ctrl-C).
2. Set gas flow to 0 on all mass flow meters, close valves
of gas sources (wall).
3. Shut off reactor (switch on the left backside).
4. Water cooling unit:
    - Switch of water cooling unit,
    - Clamp off water cooling tubing, and use a beaker for spilling water
      when disconnecting water cooling tubing.
6. Probes:
    - Unscrew all probe connectors and put on their rubber stoppers,
    -  AVOID TOUCHING any electrodes of probe connectors,
    - Unscrew probe holders and carefully remove probes,
    - Rinse all probes with ethanol (to sterilize), then 
      with monodest (VE) water,
    - Inspect probes for damage: see Figure \ref{fig:pO2membrane} for pO2 probe
    and [TODO: specify for pH],
    - Store pH probe in its cap filled with 3 M KCl,
    - Add rubber stopper on O2 probe.
7. Liquid tubing:
    - Clamp off liquid tubing (acid/base/feed/waste),
    - Remove tubing from peristaltic pumps, 
    - Disconnect tubing at Luer lock connectors.
    - USE RED GLOVES and SAFETY GOGGLES for acid/base tubing,
    - Sterilize (ethanol) and close Luer lock stopper of 
      medium/feed tubing, if the medium shall be reused.
8. Unscrew and carefully take off agitation motor.
9. Gas tubing:
    - Disconnect offgas tubing, then offgas filter, 
    THIS MAY RELIEVE REACTOR OVERPRESSURE,
    - Disconnect ingas tubing.
10. Loosen sterile sampler screw.
11. Unmounting reactor:
    - Prepare autoclave stand for reactor, 
    - Make sure all probes and tubing are disconnected, 
    - Carefully dislodge reactor and sterile sampler, and
    - Transfer both to autoclave stand.
12. Remove culture and measure volume, 
    - Tare a balance to the volumetric flask, and 
    record both volume and weight of the culture.
    - Transfer culture to bottles for autoclaving,
    - Sterilize volumetric flask with ethanol.
13. Clean reactor and tubing.
    - Sterilize remaining culture by rinsing reactor and tubing
    with ethanol,
    - Use soap and brush to scrub off biofilms,
    - Rinse reactor and tubing 3x with tap water,
    - Rinse reactor and tubing 3x with monodest (VE) water.

![Lambda pO2 probe membrane: inspect probes for damage on the membrane, this one looks OK!\label{fig:pO2membrane}](pO2_membrane.jpg)
