
options(stringsAsFactors=FALSE)

expid <- "20201204_RM_topA"
local <- file.path("~/work/CoilHack/experiments/reactor/pcc6803",expid)
setwd(local)

cat(paste("PARSING offline_data.csv:", date(), "\n"))

          
## load LOG
log <- try(read.csv("LOG.csv", sep=";"))

if ( !inherits(log, 'try-error') ) { # only proceed if log file could be parsed
    ltime <- as.POSIXct(strptime(log[,1], format="%Y%m%d_%H:%M:%S"))
    t0 <- ltime[which(log[,2]=="Inoculation")]
    dtime <- difftime(ltime, t0, units="hour")
    lsmp <- sub("SAMPLE_","",grep("SAMPLE_",log[,2],value=TRUE))
    stime <- dtime[grep("SAMPLE_",log[,2])]
    names(stime) <- lsmp
    
    ## induction?
    tind <- NULL
    if ( "Induction" %in% log[,2] ) {
        tind <- ltime[which(log[,2]=="Induction")]
        tind <- difftime(tind, t0, units="hour")
    }
    
    ## absolute time of samples
    atime <- ltime[grep("SAMPLE_",log[,2])]
    names(atime) <- lsmp
    
    ## load offline data
    dat <- try(read.csv("offline/offline_data.csv",
                        sep="\t",dec=",", check.names=FALSE))
    
    if ( !inherits(dat, 'try-error') & nrow(dat)>0 ) {
        
        ## get and filter samples
        smp <- sub("SAMPLE_","",dat[,1])
        
        ## filter those in log file
        rm <- !smp%in%lsmp
        smp <- smp[!rm]
        dat <- dat[!rm,]
        
        ## wavelength, spectra and samples
                                        #od <- dat[,"OD750"]
        
                                        #cce <- dat[,"cells/mL STE"]
        dw <- dat[,"CDW/mL"]
        atp <-  dat[,"ATP nM"]
        
        ## sample colors
        cols <- sub("FF$","99",rev(viridis::viridis(length(smp))))
        ltys <- rep(c(1,2), length(cols)/2)
        
### PLOTS
        
        ## TIME COURSES
        
        XAX <- "time since inoculation, h" # "sample"
        x <- stime[smp]
        ax1 <-  function(side=1) {
            axis(side, at=seq(-100*24,100*24,2), labels=NA)
            axis(side, at=seq(-100*24,100*24,24), tcl=2*par("tcl"))
        }
        
        ##
        png(file.path("analysis",paste0(expid,"_offline.png")),
            units="in", height=3.5, width=2*3.5, res=300)
        par(mfcol=c(1,1),mai=c(.5,.5,.1,.5), mgp=c(1.1,.1,0), tcl=.25)
        plot(0,col=NA,xlim=c(0,max(stime)),axes=FALSE,xlab=NA,ylab=NA)
        ax1()
        abline(v=tind,col=2)
        legend("topleft","induction",pch="l",col=2, bty="n",text.col=2)
        legend("bottomleft",smp[!is.na(dw)],
               title="Samples:",col=cols[!is.na(dw)],
               pch=19, lty=ltys[!is.na(dw)],lwd=2,
               cex=.75,y.intersp=.7,bty="n",
               ncol=ifelse(length(smp[!is.na(dw)])>12,2,1))
        if ( any(!is.na(dw)) ) {
            par(new=TRUE)
            plot(x[!is.na(dw)],dw[!is.na(dw)], type="l",col=1,lwd=1.5,lty=1,
                 ylim=c(0,max(dw,na.rm=TRUE)),
                 axes=FALSE, xlab=XAX,ylab="CDW, mg/mL", xlim=c(0,max(stime)))
            points(x,dw, col=cols, pch=19)
            axis(2)
        }
        if ( any(!is.na(atp)) ) {
            par(new=TRUE)
            plot(x[!is.na(atp)],atp[!is.na(atp)], type="l",col=2,lwd=1.5,lty=1,
                 ylim=c(0,max(atp,na.rm=TRUE)),
                 axes=FALSE, xlab=XAX,ylab=NA, xlim=c(0,max(stime)))
            #points(x,atp, col=cols, pch=19)
            axis(4, col=2, col.axis=2)
            mtext("ATP, nM", 4, par("mgp")[1],col=2)
        }
        dev.off()
    } else { cat(paste("CORRUPT offline_data.csv FILE?\n")) }
} else { cat(paste("CORRUPT LOG.csv FILE?\n")) }
