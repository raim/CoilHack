
## rhamnose washout

phi <- 0.012 # total reactor dilution rate, per hour
c0 <- 2 # mM

time <- seq(0,20, .1) # days

ct <- c0 * exp(-phi*24*time)

png("analysis/rhamnose.png", units="in", height=3.5, width=3.5, res=200)
par(mai=c(.5,.5,.1,.1), mgp=c(1.3,.4,0), tcl=-.25)
plot(time, ct, type="l", ylim=c(0,c0), ylab="I(t)", xlab="time, d")
abline(v=min(time):max(time),lwd=.1)
abline(h=seq(0,2,.1),lwd=.1)
lines(time, ct)
dev.off()

