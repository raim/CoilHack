library(png)
library(pixmap)

local <- "~/work/CoilHack/experiments/reactor/pcc6803//20200629_topA"
remote <- file.path("raim@biertank.bioinf.uni-leipzig.de:public_html/lambda",
                    "20200629_topA")

## rsync latest file to server
previous <- ""
prevtime <- strptime("2020-01-01 00:00",format="%Y-%m-%d %H:%M")
while(TRUE) {
    ## note: relies on sorting by name, since mtime is not available
    ## after copy from windows
    latest <- list.files(path=file.path(local,"camera"),
                         pattern="*.bmp")
    details <- file.info(file.path(local,"camera",latest))
    details <- details[with(details, order(as.POSIXct(mtime))), ]
    latest <- tail(rownames(details),1)
    mtime <- tail(details$mtime,1)
    if ( previous!=latest ) {
        ## read bitmap & and add time stamp
        r <- readPNG(latest)
        pr <- pixmapRGB(r)
        newf <- file.path(local,"camera","current_camera.png")
        png(newf, units="in", height=3.5, width=3.5, res=200) 
        par(xaxs="i",yaxs="i",mai=rep(0,4))
        plot(pr)
        legend("bottomright", format(mtime,format="%Y/%m/%d %H:%M"),
               bg="#ffffffAA", seg.len=0, bty="n")
        dev.off()
        ## rsync to server
        rsync <- paste("rsync -e 'ssh -p 22' -avz",
                       newf, file.path(remote,"current_camera.png"))
        system(rsync)
     }
    previous <- latest

    ## offline data
    latest <- list.files(path=file.path(local,"offline"),
                         pattern="offline_data.csv$")
    mtime <- file.info(file.path(local,"offline",latest))$mtime
    latest <- list.files(path=file.path(local),pattern="LOG.csv$")
    mtime <- max(mtime,file.info(file.path(local,latest))$mtime)
    if ( mtime > prevtime ) {
        ## call script to plot offline data
        source(file.path(local,"offline.R"))
        rsync <- paste("rsync -e 'ssh -p 22' -avz",
                       file.path(local,"analysis","current_offline.png"),
                       file.path(local,"analysis","spectra_all.png"),
                       file.path(remote))
        system(rsync)
        prevtime <- mtime
    }
    #cat(paste("."))
}
