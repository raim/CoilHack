
// LIQUID PUMPING MODULE         (designed for server version 2.2.1)
// pump rate can be measured by placing the source or target
// bottle on the scale and recording the weight loss/gain;
// data can be recorded on SD card, and by integrating over
// time, the pump speed can be calibrated to g/h or, with
// additional volume of a reactor as dilution rate 1/h.

#include <Time.h> // convert times (day, hours, minutes, seconds)
#include <UTFT.h> // Display
#include <UTouch.h> // Touchscreen
#include <SPI.h> // Serial Periperal Interface Bus control (for SD Card)
#include <SD.h> // SD Memory Card
#include "hx711.h" // scale
#include "DHT.h" // Temp/Humidity Sensor

// Time
time_t t; // time in milliseconds
time_t st; // synced time from Server
time_t dt1; // timepoint for calculating timestamp transfer time
time_t dt2; // timepoint for calculating timestamp transfer time
time_t dt; // loop duration
bool adjust = false; // Adjust received Timestamp by previous loop duration

// Weight, Temp and Humidity
//double wght = 0;   // weight in gram
long raw = 0; // TESTING ONLY raw values
float tmp = 0.0; // Temperature in °C
float hum = 0.0; // Humidity in %

// Timestamp
String hr; // holds hours from timestamp
String mn; // holds minutes from timestamp
String sc; // holds seconds from timestamp
String dy; // holds day from timestamp
String mt; // holds month from timestamp
String yr; // holds year from timestamp


// HX711 SCALE
// TODO: choose https://github.com/bogde/HX711 or
//              https://github.com/aguegu/ardulibs/tree/master/hx711
// TODO: add calibration routine as described in README.md of above projects
Hx711 scale(A9, A8); // scale on analog pins A8 (SCK) and A9 (DT/DOUT)

// Temperature/Humidity Sensor
DHT dht(8, DHT22); // Temp Sensor on digital pins 8 (Data) and 9 (VCC)

// SD CARD
#define SD_CS_PIN 53 // DEFINE SD CARD PIN
Sd2Card card;
File myFile;  // File on the SD Card
File sendServer; // File on SD Card, which will be sent to Server.
File timeStmp; // Contains Timestamps seperated by Headers.
bool record = true; // record backup data on SD Card? Set true for auto-record.
bool recserv = true; // record server data on SD Card? Buttons for toggling? Set true for auto-record.

// Serial Comms
String ID = "Scale"; // The Arduinos Identifier
String Header = "#time/runtime[ms] raw temperature[C] humidity[%]"; // The Arduinos data file header
String msg; // Messages from Server
String rsp; // Arduino Response Storage
unsigned long sze; // Server file size
bool abrt = false; // abort data transfer?
String clrLne = "                        "; // to clear complete lines

// TOUCHSCREEN
// Initialize display
UTFT    myGLCD(SSD1289,38,39,40,41);
// Initialize touchscreen
UTouch  myTouch( 6, 5, 4, 3, 2);
// Declare which fonts we will be using
extern uint8_t BigFont[];

/* Marked for DELETE 
##########################################
int x, y; // position on touch screen

// record and stop button coordinates
// TODO: define here
int rec_x1, rec_x2, rec_y1, rec_y2;
int stp_x1, stp_x2, stp_y1, stp_y2;
##########################################
*/

/*************************
**  UTILITY FUNCTIONS   **
*************************/


String zeroprint (int value) { // Function to print leading zeros to 1 digit integers to enable printing the desired date and time format.
  
  String res;
  String stval = String(value);
  
  if (stval.length() == 1){
    res = "0" + stval;
    return res;
  } else {
    return stval;
  }
}


/*************************
**   SETUP FUNCTIONS    **
*************************/

// NOTE: setup functions are the ideal playground to test new
//       functionality; the could also be used for proper error handling.

void setupScreen() {

  // Initial setup
  myGLCD.InitLCD();
  myGLCD.clrScr();

  myTouch.InitTouch();
  myTouch.setPrecision(PREC_MEDIUM);

  myGLCD.setFont(BigFont);
  myGLCD.setBackColor(0, 0, 0);
  
}

void setupSDCard() {

  if ( !card.init(SPI_HALF_SPEED, SD_CS_PIN) ) {

    record = false;
    recserv = false;
    myGLCD.setColor(255, 0, 0);
    myGLCD.print("ERROR: NO SD CARD", CENTER, 192);
    myGLCD.setColor(255, 255, 255);
    return;
  } 
  //pinMode(SD_CS_PIN, OUTPUT);
  // TODO: what does this test, when can it fail?
  if ( !SD.begin(SD_CS_PIN) ) {
    record = false;
    recserv = false;
    myGLCD.print("SD Card Error",CENTER,192);
    return;
  }

  // test by writing to a test file
  myFile = SD.open("test.txt", FILE_WRITE);
  if ( myFile ) {
    // if the file opened okay, write to it:
    myFile.println("testing 1, 2, 3.");
    // close and remove the test file:
    myFile.close();
    SD.remove("test.txt");
  } else {
    // if the file didn't open, print an error:
    record = false;
    recserv = false;
    myGLCD.setColor(255, 0, 0);
    myGLCD.print("Failed writing to SD", CENTER, 192);
    myGLCD.setColor(255, 255, 255);
  }
    
  // New Session file initialization

  if (SD.exists("data.txt") == false) { // If data.txt doesn't exist
    myFile = SD.open("data.txt", FILE_WRITE); // data.txt remains on the SD card as a measurement backup.
    if ( myFile ) {
      myFile.print(ID); // write Arduino ID
      myFile.print('\n');
      myFile.print(Header); // write a header on initialization.
      myFile.print('\n');
      myFile.close();
    }
  } else { // else write a new session separator and a header.
    myFile = SD.open("data.txt", FILE_WRITE);
    if (myFile) {
      myFile.print("new");
      myFile.print('\n');
      myFile.print(Header);
      myFile.print('\n');
      myFile.close();
    }
  }

  sendServer = SD.open("servdat.txt", FILE_WRITE);
  if ( sendServer ){
    sendServer.print ("new");
    sendServer.print ('\n');
    sendServer.close();
  }

  if (SD.exists("times.txt") == false) {
    timeStmp = SD.open("times.txt",FILE_WRITE);
    if ( timeStmp ){
      timeStmp.print ("Arduino_Time[ms]");
      timeStmp.print (' ');
      timeStmp.print ("Server_Time");
      timeStmp.print ('\n');
      timeStmp.close();
    }
  } else {
    timeStmp = SD.open("times.txt",FILE_WRITE);
    if (timeStmp){
      timeStmp.print("new");
      timeStmp.print('\n');
      timeStmp.close();
    }
  }
}

/***************************
** ARDUINO MAIN FUNCTIONS **
**    setup and loop      **
***************************/

// Arduino setup function: called upon reset/power-on
// and followed by loop()ing (below)
void setup() {

  Serial.begin(76800,SERIAL_8E1); // Start Serial connection with host (baud 76800 | Parity even | 8 bits | 1 stopbit)

  // initializing Temp/Humid Sensor and power pin (9).
  pinMode(9,OUTPUT);
  digitalWrite(9,HIGH);
  dht.begin();

  // initializing the touch screen.
  setupScreen(); 

  // initializing SD Card.
  myGLCD.print("Initializing", CENTER, 32);
  myGLCD.print("SD Card", CENTER, 64);
  setupSDCard(); // NOTE: this also writes headers to the data files

  /* Mark for DELETE
  ##############################################################################
  //myGLCD.print("Initializing", CENTER, 32);
  //myGLCD.print(" Motor ", CENTER, 64);

  // MOTOR - TODO: replace by code/sampler/stepper
  // setup Channel A
  //pinMode(dirA, OUTPUT); //Initiates Motor Channel A pin
  //pinMode(brkA, OUTPUT); //Initiates Brake Channel A pin

  // test run motor
  //digitalWrite(dirA, HIGH);  //Establishes forward direction of Channel A
  //digitalWrite(brkA, LOW);   //Disengage the Brake for Channel A
  //analogWrite(pwmA, 500);    //Spins the motor on Channel A at full speed
  //delay(5000);
  //analogWrite(pwmA, 100);      //Spins the motor on Channel A at full speed
  ###############################################################################
  */

  // initializing scale.
  // Conversion factor at 24.55°C = 189.885256516669
  // Zerotar raw value at 24.55°C = 9419975
  // myGLCD.print("Initializing", CENTER, 32);
  // myGLCD.print(" Scale ", CENTER, 64);
  // scale.setScale(189.885256516669); // set scale raw value conversion factor.

  /* Mark for Delete
  ##################################################################
  //Serial.print(scale.getGram(), 1);
  //Serial.println(" g");
  
  //Serial.print("Calibrating scale: ");
  //scale.set_scale();
  //scale.tare();
  //Serial.println(scale.get_units(10));
  // TODO: show weight and relative pump speed
  // +/- buttons for pump speed
  // calculate current rate from data on SD-CARD
  // display pump speed depending on available calibrations 
################################################################
*/
}

void loop() {
  
  // print to serial
  //Serial.print(slope*scale.getGram(), 1);
  //Serial.print(scale.get_units(10));
  //Serial.println(" g");
  
  dt1 = millis(); // Timepoint 1 for loop duration calculation
  
  // Timestamp correction
  if (adjust == true){ // Adjust timestamp by loop duration
    st = now();
    adjustTime (round(dt / 1000));
//    myGLCD.printNumI (t,CENTER,140); // FOR TIMESTAMP COMPARISON - REMOVE WHEN DONE TESTING
    timeStmp = SD.open("times.txt",FILE_WRITE); // and print syncronized timestamps in times.txt
    if (timeStmp){
      t = millis();
      timeStmp.print (t);
      timeStmp.print (' ');
      st = now();
      timeStmp.print(year(st));
      timeStmp.print(zeroprint(month(st)));
      timeStmp.print(zeroprint(day(st)));
      timeStmp.print("_");
      timeStmp.print(zeroprint(hour(st)));
      timeStmp.print(":");
      timeStmp.print(zeroprint(minute(st)));
      timeStmp.print(":");
      timeStmp.print(zeroprint(second(st)));
      timeStmp.print('\n');
      timeStmp.close();
    }
  adjust = false;  
  }

//  Manual Timestamp Checking
//  st = now ();                                                          // For Testing only
//  myGLCD.print("           ",CENTER,95); // Timestamp Check             // For Testing only
//  myGLCD.print("           ",CENTER,110); // Timestamp Check            // For Testing only
//  myGLCD.print("           ",CENTER,125); // Timestamp Check            // For Testing only
//  myGLCD.print(zeroprint(hour(st)),CENTER,95); // Timestamp Check       // For Testing only
//  myGLCD.print(zeroprint(minute(st)),CENTER,110); // Timestamp Check    // For Testing only
//  myGLCD.print(zeroprint(second(st)),CENTER,125); // Timestamp Check    // For Testing only


  // Get Data
  t = millis(); // Runtime measurement in ms
  st = now (); // Time measurement based on synced time
//  wght = scale.getGram(); // Get weight
  raw = scale.getValue();
  tmp = dht.readTemperature(); // Get temperature
  hum = dht.readHumidity(); // Get humidity  
  t = (t + millis()) / 2; // Create runtime average out of duration
  st = (st + now ()) / 2; // Create time average out of duration
  
  // PRINT VALUES TO SCREEN
  myGLCD.print("Time [sec]:          ", LEFT, 16);
  myGLCD.printNumI(round(t/1000), RIGHT, 16);
  myGLCD.print("Raw:          ", LEFT, 32);    
  myGLCD.printNumI(raw, RIGHT, 32);
  myGLCD.print("Temp[C]:", LEFT, 48);
  myGLCD.printNumF(tmp, 1, RIGHT, 48);
  myGLCD.print("Humidity[%]:", LEFT, 64);
  myGLCD.printNumF(hum, 1, RIGHT, 64);

  // WRITE DATA TO FILES
  if ( record ) {

    myFile = SD.open("data.txt", FILE_WRITE);
    if ( myFile ) {

      myGLCD.setColor(255, 0, 0);// keep red font until "Stop" button is pressed
      myGLCD.print("Recording", CENTER, 176);    
      myGLCD.setColor(255, 255, 255);
      
      myFile.print(t);
      myFile.print(' ');
//      myFile.print(wght);
//      myFile.print(' ');
      myFile.print(raw);
      myFile.print(' ');
      myFile.print(tmp);
      myFile.print(' ');
      myFile.print(hum);
      myFile.print('\n');
      myFile.close();
    } else {
      myGLCD.setColor(255, 0, 0);// keep red font until "Stop" button is pressed
      myGLCD.print("DATA FILE ERROR", CENTER, 192);
      record = false;
    }
  } 

  if (recserv){  // writing also to servdat, when connected to server.
    sendServer = SD.open ("servdat.txt",FILE_WRITE);
    if(sendServer){
      myGLCD.setColor(255, 0, 0);
      myGLCD.print("   Server-Mode    ", CENTER, 192);    
      myGLCD.setColor(255, 255, 255);

      if (timeStatus() == timeSet){
        sendServer.print(year(st)); // always write real time to servdat.txt instead (when correctly set).
        sendServer.print(zeroprint(month(st)));
        sendServer.print(zeroprint(day(st)));
        sendServer.print("_");
        sendServer.print(zeroprint(hour(st)));
        sendServer.print(":");
        sendServer.print(zeroprint(minute(st)));
        sendServer.print(":");
        sendServer.print(zeroprint(second(st)));
      } else { 
        sendServer.print(t);
      }

      sendServer.print(' ');
//      sendServer.print(wght);
//      sendServer.print(' ');
      sendServer.print(raw);
      sendServer.print(' ');
      sendServer.print(tmp);
      sendServer.print(' ');
      sendServer.print(hum);
      sendServer.print('\n');
      sendServer.close();

    } else {
      myGLCD.setColor(255, 0, 0);// keep red font until "Stop" button is pressed
      myGLCD.print("SERVER FILE ERROR", CENTER, 192);
      recserv = false;
    }
      
  }

  // SERIAL COMMUNICATION
  
  msg = "none";
  rsp = "none";
  
  if (Serial.available() > 0){
    msg = Serial.readStringUntil('\n');
    myGLCD.setColor(255, 0, 0);// keep red font until "Stop" button is pressed
    myGLCD.print("   RECEIVED MESSAGE   ", CENTER, 192);
    myGLCD.print(msg, CENTER, 208);
    
    if (msg == "call"){ 
      recserv = true; // Auto-activate Server file recording when called
      Serial.println ("response"); // respond when called
      Serial.println (ID); // send ID when called
      Serial.println (Header); // send Header when called
      myGLCD.print ("                          ", CENTER, 80);
      myGLCD.setColor (255, 255, 255); // fond should be white here
      myGLCD.print ("Connected",CENTER,80);
      myGLCD.setColor (255, 0, 0); // fond back to red
      
      if (Serial.readStringUntil ('\n') == "rename") {
        ID = Serial.readStringUntil ('\n');
      }
    }
    
    if ( msg == "time") {
      if (Serial.readStringUntil ('\n') == "forced" || timeStatus() == timeNotSet){
        hr = Serial.readStringUntil ('\n');
        mn = Serial.readStringUntil ('\n');
        sc = Serial.readStringUntil ('\n');
        dy = Serial.readStringUntil ('\n');
        mt = Serial.readStringUntil ('\n');
        yr = Serial.readStringUntil ('\n');
        setTime (hr.toInt(),mn.toInt(),sc.toInt(),dy.toInt(),mt.toInt(),yr.toInt());
        if (timeStatus() == timeSet){
          adjust = true;
          rsp = "done";
        }
          
      } else {
        Serial.readStringUntil ('\n');
        Serial.readStringUntil ('\n');
        Serial.readStringUntil ('\n');
        Serial.readStringUntil ('\n');
        Serial.readStringUntil ('\n');
        Serial.readStringUntil ('\n');
        rsp = "pass";
      }
      t = millis();
      Serial.println(t); // Send Ardtime
      Serial.println(rsp); // Send response
    }
      
    if ( msg == "pull" ) {
      sendServer = SD.open ("servdat.txt",FILE_READ);
      if (sendServer){
        sze = (sendServer.size() + 9);
        Serial.println(sze);
        
        if (Serial.readStringUntil('\n') == String(sze)){
          myGLCD.print ("Ready to send!",CENTER,208);
          Serial.println ("begin");
          t = millis();

          while (Serial.available() == 0){
            if ((millis() - t) > 5000){
              abrt = true;
              break;
            }
          }

          if (Serial.readStringUntil('\n') == "ready" && abrt != true){
            myGLCD.print ("Sending Data...",CENTER,208);
            Serial.print ("Start");
            Serial.print ('\n');
	          while (sendServer.available()) {
	            Serial.write(sendServer.read());
	          }
            Serial.print ("EOF");
	          sendServer.close();  
	          SD.remove("servdat.txt");
            myGLCD.print ("                          ", CENTER, 208);
          } else {
              abrt = true;
          }
        } else {
            abrt = true;
        }

        if (abrt){
          myGLCD.print ("Transfer cancelled!", CENTER,208);
          Serial.println("cancel");
          while (Serial.available() > 0){
            Serial.read();
          }
          sendServer.close();
          abrt = false;
        }
      } else {
          Serial.println("0");
      }
    }
  } else{
    myGLCD.print("                          ", CENTER, 208);
  }

  /// End of Serial Comms
  
  myGLCD.setColor(255, 255, 255);

  dt2 = millis(); // Timepoint 2 for loop duration calculation
  dt = (dt2 - dt1); // Time Arduino took for Timesync loop (gets added to timestamp)
//  Serial.setTimeout(1000); // 1 seconds time-out - WHAT IS THIS USED FOR? MFD
}
