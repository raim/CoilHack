
# Searching potential off-targets of sgRNAs in CoilHack

* Date: 2022-03-17
* http://www.rgenome.net/cas-offinder/
* Select:
    - PAM Type: SpCas9 from Streptococcus pyogenes: 5'-NGG-3'
	- Target Genome: Synechocystis sp. PCC 6803 (ASM131838v1) - cyanobacteria = CP012832.1
	- Mismatch number: 2
	- DNA Bulge Size: 2
	- RNA Bulge Size: 2
* Copy/paste sgRNA sequences <geneID>, run, download results.txt,
and save in ~/work/CoilHack/sequences/cas-offinder_<geneID>.txt.
* Blast resulting offtargets in local genome copy.
* Plot in genomeBrowser and inspect.

# Results

* topA: 
    - GATgGTGCGGGCaTT-GTGGAGG	CP012832.1	273975	+	2	1
	- -> this is sll1660.
* gyrB: NONE,
* slr0091:
    - TTTCGTTTACATAGCTTTCAAAGGTGG	CP012832.1	2895782,
	-> this is slr0091, aldehyde dehydrogenase family protein,
	-> this sgRNA DOES NOT target gyrA,
* gyrA_v1: NONE,
* gyrA_v2: NONE.
