# Guide to Add and Document Experiments

For each series of experiments generate a new directory in
`CoilHack/experiments`. This directory should contain the 

* a README.md file that quickly explains the contents,
* measured raw data of the experiments, 
* data analysis  scripts (R, excel, python, etc.), and 
* a summary report, 

BUT NOT any intermediate data files that can be generated by these
scripts.

## Naming scheme: \<DATE\>_\<PERSON\>_\<EXPERIMENT\>

where the DATE should be of format YYYYMM[DD], where the day DD
is optional, and month should be the month the experiment series
had started, PERSON should be a short identifier of the main person
doing the experiments and keeping a labbook on them, and EXPERIMENT
should be a short identifier of the exerperiment run.



## Data Evaluation Pipeline

For each new measurement series, create a new directory in
[experiments](experiments), create a directory with the same name in
[reports](reports) which is NOT TO BE ADDED TO GIT, but should hold
all detailed analysis results.

e.g. experiment "201804_JPK_ecoli_topA"

1. Measure data and add to directory [experiments/201804_JPK_ecoli_topA](experiments/201804_JPK_ecoli_topA)
2. Write evaluation script and add it to the experiments directory AND to
   the git.  Now create a separate directory
   [reports/201804_JPK_ecoli_topA](reports/201804_JPK_ecoli_topA) (or where you wish) in which all
   data produced by the analysis pipeline can be stored, ie.,  result
   figures, tables, .RData files, etc. **DO NOT add these data to the
   git**; the git should only contain the original raw data and the
   analysis.
3. Once you are happy with the analysis, write a summary report in
   Rmarkdown/RStudio, (file ending .Rmd), store it in
   [experiment/201804_JPK_ecoli_topA](experiment/201804_JPK_ecoli_topA)
   and add it to the git. The report should load either raw data or
   processed data in
   [reports/201804_JPK_ecoli_topA](reports/201804_JPK_ecoli_topA). Do
   not store the PDF or HTML generated by the Rmd file in the
   git. Once the analysis is really finished, you may/should also
   store a report PDF in the git.




