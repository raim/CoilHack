
library(platexpress)
#source("~/programs/platexpress/R/parsers.R")
#source("~/programs/platexpress/R/plot.R")
#source("~/programs/platexpress/R/platexpress.R")

DATPATH <- "/home/raim/work/CoilHack/experiments/growthcurves/TR001_biolector"
RESPATH <- "/home/raim/work/CoilHack/results/growthcurves/TR001_biolector" #sub("experiments","results", DATPATH)
dir.create(RESPATH, recursive=TRUE)

file <- "Microbial-Protocol-medium-OTR - TR001_20181115_153514_TRANSFORM_CALIBIOMASS_CALIPH_CALIPO2.csv"

files <- file.path(DATPATH, file)
mapfile <- file.path(DATPATH, "Layout.csv")

## TODO: get temperature and humidity
## TODO: what's wrong with GFP?

## view raw data
raw <- readPlateData(files, type="BioLector")
## TODO: why does this fail?
## raw <- prettyData(raw, yids=c("biomass"="Biomass", pH="pH-hc",O2="pO2-hc", GFP="GFP"))


plotdev(file.path(RESPATH,"biolector_metabolism_all"),type="png",width=8,height=6)
viewPlate(raw,yids=c("pH-hc","pO2-hc"))
dev.off()
plotdev(file.path(RESPATH,"biolector_biomass_all"),type="png",width=8,height=6)
viewPlate(raw,yids=c("Biomass","GFP"))
dev.off()



## get groups
map <- readPlateMap(mapfile,sep=";",fsep=";",asep=":",
                    afields=c("glucose","aTc"),
                    fields=c("strain","medium","glucose","aTc"))
## replace 0 in well IDs!
map$well <- unlist(lapply(strsplit(as.character(map$well),""),
                           function(x) paste0(x[1],"0",x[2])))
## correct blanks
dat <- correctBlanks(raw, map, yids=c("Biomass","GFP"), base=0.01)

## smooth O2
viewPlate(dat, yids="pO2-hc")
dat <- addData(dat, "O2", apply(getData(dat,"pO2-hc"), 2, ma, 10),replace=TRUE)
viewPlate(dat, yids=c("pO2-hc","O2"), axes=c(2,4), ylim=c(50,120))

## looks ok, remove raw O2 data!
dat <- rmData(dat, "pO2-hc")

grp <- getGroups(map, by="strain")
grp2 <- getGroups(map, by=c("strain","aTc.amount"))
grp2.col <- groupColors(map, grp2, "aTc.color")

viewGroups(dat, groups = grp)

## re-group
grp <- grp[c(1,3,4,2)]

## use layout to combine all plots
mat <- matrix(1:(length(dat$dataIDs)*length(grp)),
              ncol=length(dat$dataIDs))

plotdev(file.path(RESPATH,"biolector_all"),type="png",width=8,height=6)
layout(mat=mat)
par(mai=c(.3,.15,.01,.01), mgp=c(1.3,.2,0), tcl=-.25)
ylims <- list(Biomass=c(0,1e3),
              O2=c(75,120),
              GFP=c(0,200),
              "pH-hc"=c(6.2,6.7))
for ( id in names(ylims) )
    for ( i in 1:4 ) 
        viewGroups(dat, groups = grp[i], groups2 = grp2,
                   yids = id, ylim=ylims[[id]],
                   group2.col=grp2.col, no.par=TRUE, embed=TRUE, g1.legend=i==4, g1.legpos="bottomright",g2.legend=id==names(ylims)[length(ylims)], g2.legpos="topright",xlab="time, h")
dev.off()

## fluorescence per biomass
dat <- addData(dat, "mV/BM", getData(dat,"GFP")/getData(dat,"Biomass"))
viewGroups(dat, groups = grp, groups2 = grp2, yids = c("mV/BM"), group2.col=grp2.col, ylim=c(0,.5))

## O2 per biomass
## TODO: use set OTR to calculate qO2
dat <- addData(dat, "O2/BM", getData(dat,"O2")/getData(dat,"Biomass"))
viewGroups(dat, groups = grp, groups2 = grp2, yids = c("O2/BM"), group2.col=grp2.col,ylim=c(0,20),xlim=c(0,8))



## just write out all data
writeData(dat, file.path(RESPATH,"TR001_biolector"))

## get replicate statistics
stats <- groupStats(dat, grp2)
time <- stats$Time
for ( dtype in dat$dataIDs ) {
    cat(paste("writing", dtype,": "))
    for ( stype in names(stats[[dtype]]) ){
        cat(paste0(stype, ", "))
        file <- file.path(RESPATH,
                          make.names(paste0("TR001_biolector_",dtype,"_",
                                            stype,".csv")))
        write.csv(cbind(time,stats[[dtype]][[stype]]),
                  file=file)
    }
    cat("\n")
}
