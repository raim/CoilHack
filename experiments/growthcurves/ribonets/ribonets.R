
### REQUIRED R PACKAGES
INSTALL <- FALSE
if ( INSTALL ) {

    ## select a cran repo
    REPOS <- "https://ftp.gwdg.de/pub/misc/cran/" #used Goettingen/Germany here
    ## WAS MISSING
    install.packages("readxl", dependencies = TRUE, repos=REPOS)
    install.packages("colorRamps", dependencies = TRUE, repos=REPOS)

    ## Thomas Petzold's growth rates package for
    ## first growth rate fits
    ## TODO: test cran version of growthrates
    install.packages("growthrates", repos = REPOS)
    ## otherwise, use github version
    ##install_github("tpetzoldt/growthrates")
    
    ## in-house software platexpress
    ## TODO: make a "release" for ribonets paper
    install.packages("devtools", repos = REPOS)
    library(devtools)
    install_github("raim/platexpress")
}

library(platexpress)
library(growthrates)

### INPUT SETTINGS
DATPATH <- "/home/raim/work/CoilHack/experiments/growthcurves/ribonets"

## EXPERIMENT IDs
## NOTE: order matters because EVC from LW4 is used for LW2/LW3
expids <- c("AP61","NG11","NG12","LW4","LW2","LW3")

### GLOBAL OUTPUT and PLOT SETTINGS
PLOT2PDF <- TRUE
fig.type <- "png" # "pdf" #
OUTPATH <- file.path(DATPATH, "plots")
dir.create(OUTPATH)
setwd(OUTPATH) # all plots and pdfs will go there!


### RUN PARAMETERS

## smoothing
spar <-  0.5 # smooth.spline param spar for GFP/OD smoothing 

## growth phase estimation
easycut <- c(.5,6) # experiment hours for easylinear growth rate
easyspan <- 20 # span parameter `h` for `all_easylinear`
phases <- c(.2,.45,.65) # OD phases for lin.reg. growth rates
odpoints <-  c(.15,.4,.6) # OD values for sampling

## minor style params
## TODO: set plot ylims here
colors <- c("#0E0E0E","#0000E6","#ADBCE6") # data colors for added data
cut.growth <- FALSE # cut lin.reg growth data to valid ranges


### loop through experiments
results <- list()
for ( expid in expids ) {

    ## redirect all "working" plots to a pdf
    if ( PLOT2PDF ) pdf(paste0(expid, "_all.pdf"))

    ## parse plate layout file
    map.file <- file.path(DATPATH, "data", paste0(expid,"_layout.xlsx"))
    map <- readPlateMap(file=map.file,fsep=";",
                        blank.id="LB", fields=c("strain","replicate"))
    
### PARSE DATA and INTERPOLATE TO GLOBAL TIME
    data.files <- file.path(DATPATH, "data", paste0(expid,".csv"))
    if ( expid%in%c("LW4","NG11","AP61") ) { # exported on exon
        raw <- readPlateData(file=data.files, type="Synergy",skip=56,
                             data.ids=c("600","GFP_50:480,520"),dec=".",
                             time.format="%I:%M:%S %p",time.conversion=1/3600)
    } else { # exported on intron
        raw <- readPlateData(files=data.files, type="Synergy",skip=56,
                             data.ids=c("600","GFP_50:480,520"), dec=",",
                             time.format="%H:%M:%S",time.conversion=1/3600)
    }

    ## set new names and colors
    raw <- prettyData(raw,yids=c(OD="600",GFP="GFP_50:480,520"),
                      col=c(OD="#333300",GFP=wavelength2RGB(650)))

### PRE-PROCESSING: cut time-points and skip dodgy wells
    
    ## cut FIRST time-point: often noisy
    raw <- cutData(raw, xrng=c(raw$Time[2], tail(raw$Time,1)))
    
    plotdev(paste0(expid,"_plate_raw"), type=fig.type, width=12, height=8)
    viewPlate(raw, yids=c("OD","GFP"))
    dev.off()
    
    ## skip obviously failed replicates
    skip <- ""
    if ( expid=="NG11" ) skip <- c("D4","D5","D6")
    if ( expid=="AP61" ) skip <- c("C3","A11")
    if ( any(skip!="") ) {

        raw <- skipWells(raw, skip)
        map <- skipWells(map, skip)
       
        plotdev(paste0(expid,"_plate_skipped"), type=fig.type,
                width=12, height=8)
        viewPlate(raw, yids=c("OD","GFP"))
        dev.off()
    }

    ## skip constructs not required: F34-T7 in NG12
    skip.constructs <- c("F34-T7")
    map[map[,2]%in%skip.constructs,2:3] <- NA

    ## replace strain names
    plot.labels <- c("D50 tandem"="D50+SynT")
    for ( i in seq_along(plot.labels) ) {
        cat(paste("assigning new labels:",
                  plot.labels[i], names(plot.labels[i]), "\n"))
        map[which(map[,2]==plot.labels[i]),2] <- names(plot.labels)[i] 
    }
    
    ## add layout and replicate groups to plate data
    raw$layout <- map
    groups <- getGroups(map, by=c("strain"))
    raw$groups$group1 <- raw$groups$group2 <- groups
    
    ## number of replicate groups
    nrep <-  length(raw$groups$group2)

    ## reference and empty vector
    evcid <- "EVC"
    refid <- ifelse(expid=="AP61","D50+T7fw","D50.1.K")
    has.reference <- refid %in% map$strain
    has.evc <- evcid %in% map$strain
    
    ## for all-in-one summary plot
    allwells <- list(all=unlist(getGroups(map, by="strain")))

### BLANK CORRECTION and SMOOTHING
    
    ## BLANK CORRECTION: subtract blank (not required: raise to positive values)
    dat <- correctBlanks(raw, map, yids=c("OD"))#, base=0)

    ## SMOOTH DATA
    smooth.fun <- function(y,...) predict(smooth.spline(y ~ dat$Time,...))$y
    Os <-  apply(getData(dat, "OD"), 2, smooth.fun)
    Ys <-  apply(getData(dat, "GFP"), 2, smooth.fun, spar=spar)
    dat <- addData(dat, "ODs", Os, replace=TRUE)
    dat <- addData(dat, "GFPs", Ys, replace=TRUE)
    
    ## inspect data
    viewPlate(dat, yids=c("OD","ODs"), ylim=c(0,1.25))
    viewPlate(dat, yids=c("GFP","GFPs"), ylim=c(0,1500))
    viewGroups(dat, yids=c("OD","ODs"), ylim=c(0,1.25))
    viewGroups(dat, yids=c("GFP","GFPs"), ylim=c(0,1500))

    plotdev(paste0(expid,"_plate_smoothed"), type=fig.type, width=12, height=8)
    viewPlate(dat, yids=c("OD","ODs", "GFP", "GFPs"),
              ylims=list(GFP=c(0,1500),GFPs=c(0,1500)))
    dev.off()
    
### CALCULATE GROWTH RATES

    ## model exponential growth
    ## easylinear: finds maximal exponential growth rate
    ## NOTE: param easycut - cut between 0.5 and 6 h
    dfg <- data2growthrates(cutData(dat,xrng=easycut), "ODs",
                            plate=dat$layout, wells=unlist(dat$groups$group1))
    fits1 <- all_easylinear(value ~ time | well, data=dfg, h=easyspan)
    mdat <- addModel(fits1, dat, ID="easylinear", col=colors[1])
    
    ## get easylinear parameters and merge with plate layout info
    pars <- growthratesResults(fits1)
    grres <- merge(dat$layout, pars, by="well")
    rownames(grres) <- grres$well # to use well name as index
 
    ## LIN.REG. for first and second growth phase
    ## manual: lm fits for defined OD ranges 0-0.18, .2-0.5
    dfg <- data2growthrates(dat, "ODs",
                            plate=dat$layout, wells=unlist(dat$groups$group1))
    ## cut data between pre-defined ODs (growth phases) and take log
    dfg3 <- dfg[dfg$value>phases[1] & dfg$value<phases[2],] 
    dfg3$value <- log(dfg3$value)
    dfg4 <- dfg[dfg$value>phases[2] & dfg$value<phases[3],] 
    dfg4$value <- log(dfg4$value)

    fitm3 <- getData(dat,"ODs")
    fitm3[] <- NA
    fitm4 <- fitm3
    grres <- cbind(grres, mu2=NA, mu3=NA)
    for ( wid in unlist(dat$groups$group1) ) {

        ## second growth phase
        well <- which(as.character(dfg3$well)==wid)
        fits3 <- lm(value  ~ time, data=dfg3[well,])
        newx3 <- predict(fits3, newdata=data.frame(time=mdat$Time))
        fitm3[,wid] <- exp(newx3)
        grres[wid,"mu2"] <- coef(fits3)[[2]]

        ## third growth phase
        well <- which(as.character(dfg4$well)==wid)
        fits4 <- lm(value  ~ time, data=dfg4[well,])
        newx4 <- predict(fits4, newdata=data.frame(time=mdat$Time))
        fitm4[,wid] <- exp(newx4)
        grres[wid,"mu3"] <- coef(fits4)[[2]]
        
        plot(dfg[as.character(dfg$well)==wid,1:2])
        lines(mdat$Time,exp(newx3),col=4)
        lines(mdat$Time,exp(newx4),col=5)
    }
    mdat <- addData(mdat, "manual3", fitm4, col=colors[3])
    mdat <- addData(mdat, "manual2", fitm3, col=colors[2])

    ## inspect data
    viewPlate(mdat, yids=c("OD","ODs","easylinear","manual2","manual3"),
              ylim=c(0,1.5))#, xlim=c(0,7))
    viewGroups(mdat, yids=c("OD","ODs","easylinear","manual2","manual3"),
               ylim=c(0,1.5))#,xlim=c(0,7))
    viewPlate(mdat, yids=c("OD","ODs","easylinear","manual2","manual3"),
              ylim=c(0.05,1.5), log="y")#, xlim=c(0,7))
    viewGroups(mdat, yids=c("OD","ODs","easylinear","manual2","manual3"),
               ylim=c(0.05,1.5),log="y")#, xlim=c(0,7))
    
    
    ## MAXIMAL GROWTH RATE
    plotdev(paste0(expid,"_growthrates"), type=fig.type,
            width=1.3*nrep, height=3)
    par(mfcol=c(1,4),mai=c(1,.35,.1,.1), mgp=c(1.5,.4,0), tcl=-.25)
    boxplot(grres$lambda ~ grres$strain, las=2, ylim=c(0,1.5),
            ylab=expression("lag phase, h"))
    boxplot(grres$mu ~ grres$strain, las=2, ylim=c(0,1.5),
            ylab=expression("growth rate 1,"~mu*","~h^-1))
    boxplot(grres$mu2 ~ grres$strain, las=2, ylim=c(0,1.5),
            ylab=expression("growth rate 2,"~mu*","~h^-1))
    boxplot(grres$mu3 ~ grres$strain, las=2, ylim=c(0,1.5),
            ylab=expression("growth rate 3,"~mu*","~h^-1))
    dev.off()

    ## ALL DATA over time
    plotdev(paste0(expid,"_alldata_time"), type=fig.type,
            width=3*nrep, height=6)
    par(mfrow=c(3,nrep), mai=rep(.1,4), mgp=c(1,.2,0), tcl=-.1)
    viewGroups(mdat, yids=c("OD","ODs","easylinear","manual2","manual3"),
               embed=TRUE, no.par=TRUE, yaxis=c(1,4), ylim=c(0,1.2))
    viewGroups(mdat, yids=c("OD","ODs","easylinear","manual2","manual3"),
               embed=TRUE, no.par=TRUE, ylim=c(0.01,1),log="y")
    viewGroups(mdat, yids=c("GFP", "GFPs"),
               embed=TRUE, no.par=TRUE, yaxis=c(1,2), ylim=c(0,2e3))
    dev.off()
    
    plotdev(paste0(expid,"_alldata_time_plate"), type="pdf", width=12, height=6)
    viewPlate(mdat, yids=c("OD","ODs", "easylinear","manual2","manual3"),
              yaxis=c(1,4), ylim=c(0,1.2))
    viewPlate(mdat, yids=c("OD", "ODs","easylinear","manual2","manual3"),
              ylim=c(0.01,1),log="y")
    viewPlate(mdat, yids=c("GFP", "GFPs"), yaxis=c(1,2), ylim=c(0,2e3))
    dev.off()
    
### FOLD-CHANGE wrt D50.1.K vs. OD


    ## INTERPOLATE TO OD
    ## TODO: why does it not work for ODs?
    oddat <- interpolatePlateData(mdat, "OD")

    ## inspect all data
    viewPlate(oddat)
    viewGroups(oddat)

    ## get fold-changes at certain ODs
    levels.fc <- list()
    if ( has.reference ) {

        ## level: use OD-interpolated GFP directly, since OD cancels out
        kc <- getData(oddat, ID="GFPs")

        ## SUBTRACT EVC
        ## TODO: properly store and analyse EVC mean vs. smoothing
        GFPBLANK <- NULL # to store
        if ( has.evc ) {

            ##  linear regression on ALL EVC GFP channels
            dfgg <- data2growthrates(oddat, "GFPs", plate=oddat$layout,
                                     wells=oddat$groups$group1$EVC)
            fit.evc <- lm(value ~ time, data=dfgg)
            
            plotdev(paste0(expid,"_GFPBLANK_linreg"),
                    width=3.5, height=3.5, type=fig.type)
            par(mai=c(.5,.5,.1,.1), mgp=c(1.3,.4,0), tcl=-.25)
            plot(dfgg$time, dfgg$value, pch=19, cex=.5, ylim=c(0,120),
                 ylab="EVC Fluorescence, a.u.", xlab="OD")
            lines(oddat$OD,predict(fit.evc, newdata=data.frame(time=oddat$OD)),
                  col=2, lwd=5)
            dev.off()

            GFPBLANK <- fit.evc

        } else { # ELSE: LW2 and LW3: use previous from LW4
            GFPBLANK <- results[["LW4"]]$GFPBLANK
        }

        ## subtract EVC
        EVC <-  predict(GFPBLANK, newdata=data.frame(time=oddat$OD))
        kc <- kc - EVC

        ## add EVC-blank corrected GFP data
        oddat <- addData(oddat, "GFPb", kc, replace=TRUE, col="#ADBCE6")

        ## divide by reference
        ## get mean and smooth
        km <- apply(kc[,oddat$groups$group1[[refid]]], 1, mean,na.rm=TRUE)
        kms <- rep(NA, length(km))
        fit <- smooth.spline(km[!is.na(km)] ~ oddat$OD[!is.na(km)], spar=spar)
        kms[!is.na(km)] <- predict(fit)$y
        kc <- kc/kms

        ## inspect MEAN OF REFERENCE STRAIN
        plotdev(paste0(expid,"_GFP_reference"), type=fig.type,
                width=3.5, height=3.5)
        par(mai=c(.5,.5,.1,.1), mgp=c(1.3,.4,0),  tcl=-.25)
        plot(oddat$OD, km, type="l", lwd=4,
             ylab="mean GFP Fluorescence, a.u.", xlab="OD")
        lines(oddat$OD, kms, lty=2, col=2, lwd=2)
        legend("topleft",title=paste("GFP of reference:",refid),
               legend=c("raw mean", "smoothed mean"), col=1:2, lty=1:2,bty="n")
        dev.off()

        ## add fold-change data
        oddat <- addData(oddat, "foldchange", kc, replace=TRUE, col="#ADBCE6")

        viewPlate(oddat, yids=c("GFPb"),ylim=c(0,1.5))
        plotdev(paste0(expid,"_GFPBLANK"), type=fig.type,
                width=1*nrep, height=3)
        viewGroups(oddat, yids=c("GFP","GFPs","GFPb"), ylim=c(0,1500))
        dev.off()

        viewPlate(oddat, yids=c("foldchange"),ylim=c(0,1.5))
        plotdev(paste0(expid,"_foldchange"), type=fig.type,
                width=1*nrep, height=3)
        viewGroups(oddat, yids=c("foldchange"),ylim=c(0,1.5))
        dev.off()

### GET FOLD-CHANGE DATA AT DIFFERENT ODs, `boxData`
        ## on-thex fly BOXPLOTS - compare GFP/OD at different OD
        plotdev(paste0(expid,"_boxplots"), type=fig.type,
                width=2*length(odpoints), height=6)
        strf <- factor(grres$strain, levels=names(oddat$groups$group1))
        par(mfrow=c(2,length(odpoints)), mai=c(.8,.5,.1,.1),
            mgp=c(1.8,.4,0), tcl=-.25)
        boxplot(grres$mu ~ strf, las=2, ylim=c(0,1.5),
                ylab=expression("growth rate 1,"~mu*","~h^-1))
        boxplot(grres$mu2 ~ strf, las=2, ylim=c(0,1.5),
                ylab=expression("growth rate 2,"~mu*","~h^-1))
        boxplot(grres$mu3 ~ strf, las=2, ylim=c(0,1.5),
                ylab=expression("growth rate 3,"~mu*","~h^-1))
        for ( i in 1:length(odpoints) ) 
            levels.fc[[i]] <- boxData(oddat, rng=odpoints[i],
                                      yid="foldchange",
                                      groups=oddat$groups$group1,
                                      ylim=c(-.1,1.5), na.rm=TRUE,
                                      ylab="fold-change, level")
        dev.off()
        names(levels.fc) <- odpoints
    }
      
    ## all data over OD
    nr <- 2+as.numeric(has.reference)
    plotdev(paste0(expid,"_alldata_OD"), type=fig.type,
            width=3*nrep, height=2*nr)
    par(mfrow=c(nr,nrep), mai=rep(.1,4), mgp=c(1,.2,0), tcl=-.1)
    viewGroups(oddat, yids=c("ODs","easylinear","manual2","manual3"),
               embed=TRUE, no.par=TRUE, yaxis=c(1,4), ylim=c(0.1,1.2))#,log="y")
    viewGroups(oddat, yids=c("GFP","GFPs","GFPb"),
               embed=TRUE, no.par=TRUE, yaxis=c(1,2), ylim=c(0,2e3))
    if ( has.reference ) {
        viewGroups(oddat, yids=c("foldchange"),
                   embed=TRUE, no.par=TRUE, yaxis=c(1,2), ylim=c(-.1,1.5))
    }
    dev.off()
   
    ## close PDF
    if ( PLOT2PDF ) dev.off()


### SUMMARY FIGURE
    means <- colors <- NULL
    if ( has.reference ) {

        ## SELECT COLORS BY FOLD-CHANGE at OD=0.4
        tmp <- levels.fc[["0.4"]]
        means <- sapply(names(mdat$groups$group2), function(x) {
            mean(tmp[as.character(tmp[,2])==x,3])
        })
        ## normalize between 0.3 and 1 for color gradient
        colors <- c(sort(means, decreasing=TRUE) - .3)/(1-.3)
        colors <- round(100*colors+1)
        colors[colors<1] <- 1
        colors[colors>100] <- 100
        colors[] <- rev(colorRamps::matlab.like2(101))[colors]
        colors[refid] <- "#000000"
        if ( has.evc )
            colors[evcid] <- "#999999"
        ## resort groups
        mdat$groups$group2 <- mdat$groups$group2[names(colors)]
        oddat$groups$group2 <- oddat$groups$group2[names(colors)]

        ## NOTE that we plot standard error here, since other
        ## plots confirmed validity of normal dist. assumption
        ## and lack of true biological variance
        mai2 <- .38 # y-axis with annotation
        mai2n <- .1
        wd <- 2
        widths <- c(wd+2*mai2, wd+mai2+mai2n, .25*nrep + mai2/2)
        
        plotdev(paste0(expid,"_summary"), type=fig.type,
                width=sum(widths), height=3, res=300)
        layout(t(1:3), widths=widths)
        par(mai=c(mai2,mai2,mai2n,mai2), mgp=c(1.4,.3,0), tcl=-.25)
        viewGroups(mdat, yids=c("GFP","OD"), stats="SE",
                   embed=TRUE, no.par=TRUE,
                   yaxis=c(1,2), ylims=list(ODs=c(0,.95)),# GFP=c(0,1320)),
                   groups=allwells, group2.col=colors,
                   g2.legpos="topleft", g1.legpos="bottomright",
                   g1.legend=TRUE, xlab="time, h", lwd.orig=0)
        mtext("GFP Fluorescence, a.u.", 4, par("mgp")[1])
        mtext(expression(OD["600 nm"]), 2, par("mgp")[1])
        ##abline(h=0.4)# TODO: horizontal line only until OD, then vert to time
        par(mai=c(mai2,mai2,mai2n,0), mgp=c(1.4,.3,0))
        tmp <- viewGroups(cutData(oddat, xrng=c(.05,.7)),stats="SE",
                   yids=c("foldchange"),embed=TRUE, no.par=TRUE,
                   ylim=c(-0.1,1.25),groups=allwells, group2.col=colors,
                   g2.legend=FALSE, g1.legend=FALSE,
                   ylab="fold-change", xlab=NA,
                   xlim=c(0.05,.7), lwd.orig=0)
        abline(v=0.4)        
        abline(h=0:1,lwd=.5)
        mtext(expression(OD["600 nm"]), 1, par("mgp")[1]*1.15)
        par(mai=c(mai2,0,mai2n,mai2/2), xpd=TRUE)
        plot(1,col=NA,axes=FALSE,ylab=NA,xlab=NA,
             ylim=c(-0.1,1.25),xlim=0:1)
        abline(h=0:1,lwd=.5)
        text(0.5,-.125, "fold-change", cex=1.2)#, pos=4)
        text(0.5,-.2, expression("at"~OD["600 nm"]*"=0.4"),cex=1.2)#, pos=4)
        
        par(new=TRUE, xpd=TRUE)
        tmp <- levels.fc[["0.4"]]
        val <- tmp[,3]
        fct <-  factor(tmp[,2], levels=names(colors))
        boxplot(val ~ fct, axes=FALSE, ylab=NA, xlab=NA,
                ylim=c(-0.1,1.25), border=colors, las=2, lwd=2)
        ## calculate and plot means inside boxplots
        mns <- aggregate(val~fct, FUN=mean)
        points(1:nrow(mns), mns[,2], pch=4)
        axis(4)
        axis(2,labels=NA)
        ##text(1:length(colors), rep(-.15,length(colors)),
        ##     names(colors), srt=90, font=2) #, col=colors
        ##mtext(expression("fold-change\nat"~OD["600 nm"]*"=0.4"),4,par("mgp")[1])
        par(xpd=NA)
        dev.off()
    }
    
    ## merge all results into one big result table
    allres <- grres
    if ( has.reference )
        for ( od in as.character(odpoints) ) 
            allres <- merge(allres, levels.fc[[od]][,c(1,3)], by="well")
    
    allres <- cbind(experiment=expid, allres)
    
    ## STORE
    resl <- list(mdat=mdat, oddat=oddat, # platereader objects
                 table=allres, # growth rate and fold-change table
                 means=means, colors=colors, # sorting/coloring
                 GFPBLANK=GFPBLANK) # store for use in LW2/LW3
    results[[expid]] <- resl
}

## RESULT TABLE
ncol <- rep(NA, length(results))
nlevels <- ncolors <- table <- NULL
for ( i in 1:length(results) ) {
    ids <- paste(names(results)[i],
                 names(results[[i]]$colors),sep="_")
    cols <- results[[i]]$colors
    names(cols) <- ids
    nlevels <- c(nlevels, ids)
    ncolors <- c(ncolors, cols)
    ncol[i] <- length(results[[i]]$colors)
    table <- rbind(table, results[[i]]$table)
}

## WRITE OUT FULL TABLE OF RESULTS
write.table(table, file=file.path(OUTPATH, "results.tsv"),
            sep="\t", na="", row.names=FALSE, quote=FALSE)

## for boxplots over all experiments
## get global well IDs as factors, each with individual sorting
gids <- paste(table[,"experiment"], table[,"strain"], sep="_")
gids <- factor(gids, levels=nlevels)

## FOLD-CHANGES
plotdev("all_foldchanges", height=3*length(odpoints), width=10, type=fig.type)
par(mfcol=c(length(odpoints),1), mai=c(.8,.5,.05,.2),mgp=c(1.75,.5,0),tcl=-.25,
    xaxs="i", xpd=TRUE)
for ( rn in as.character(odpoints) ) {
    fid <- paste0("foldchange@", rn)
    par(xpd=FALSE)
    plot(1, col=NA, xlab=NA, ylab=NA, axes=FALSE, ylim=c(-.05,1.5))
    abline(h=0:1)
    par(new=TRUE, xpd=TRUE)
    boxplot(table[,fid] ~ gids, border=ncolors,
            las=2, ylab=NA, ylim=c(-.05,1.5),axes=FALSE)
    mtext("GFP fold-change", 2, par("mgp")[1], cex=1.2)
    axis(1, at=1:length(nlevels), labels=sub(".*_","",nlevels),las=2)
    axis(2, cex.axis=1.2);axis(4)#,labels=NA)
    abline(v=cumsum(ncol[-length(ncol)])+.5)
    if ( rn==as.character(odpoints)[1] )
        text(cumsum(ncol), 1.45, names(results),cex=1.5, font=2, pos=2)
    text(1,.2, paste("at OD", rn), cex=1.5, pos=4)
    
}
dev.off()


## GROWTHRATES
rateids <- c("lambda","mu","mu2", "mu3")
ratelabs <- c(expression("lag phase, h"),
              expression("growth rate 1,"~mu*","~h^-1),
              expression("growth rate 2,"~mu*","~h^-1),
              expression("growth rate 3,"~mu*","~h^-1))
names(ratelabs) <- rateids

H <- 3*length(odpoints) # 2.5*length(rateids)
plotdev("all_growthrates", height=H, width=10, type=fig.type)
par(mfcol=c(length(rateids),1), mai=c(.8,.5,.05,.2),mgp=c(1.75,.5,0),tcl=-.25,
    xaxs="i", xpd=TRUE)
for ( fid in rateids ) {
    boxplot(table[,fid] ~ gids, border=ncolors,
            las=2, ylab=NA, ylim=c(-.05,1.5),axes=FALSE)
    mtext(ratelabs[fid], 2, par("mgp")[1], cex=1.2)
    axis(1, at=1:length(nlevels), labels=sub(".*_","",nlevels),las=2)
    axis(2, cex.axis=1.2);axis(4)#);box();
    abline(v=cumsum(ncol[-length(ncol)])+.5)
    if ( fid==rateids[1] )
        text(cumsum(ncol), 1.45, names(results),cex=1.5, font=2, pos=2)
}
dev.off()

