library("platexpress")
library(growthrates)

## from lib TeachingDemos; used in plotFeatures
shadowtext <- function(x, y=NULL, labels, col='white', bg='black',
                       auto.bg=TRUE,
                       theta= seq(pi/4, 2*pi, length.out=8), r=0.05, ... ) {


    if ( auto.bg ) {
        bgn <- rep(bg, length(col))
        for ( i in 1:length(col) ) {
            crgb <- col2rgb(col[i])/255
            L <- 0.2126 * crgb[1,1] + 0.7152 * crgb[2,1] + 0.0722 * crgb[3,1]
            bgn[i] <- ifelse(L>.5, "#000000", "#FFFFFF")
        }
        bg <- bgn
    }
   
  xy <- xy.coords(x,y)
  xo <- r*strwidth('A', ...)
  yo <- r*strheight('A', ...)
  
  for (i in theta) {
    text( xy$x + cos(i)*xo, xy$y + sin(i)*yo, labels, col=bg, ... )
  }
  text(xy$x, xy$y, labels, col=col, ... )
}


DATPATH <- "/home/raim/work/CoilHack/experiments/growthcurves/ribonets"
OUTPATH <- file.path(DATPATH, "plots_full")
dir.create(OUTPATH)
setwd(OUTPATH)

PLOT2PDF <- TRUE
fig.type <- "png" # "pdf"
show.rates <- TRUE
cut.growth <- FALSE # cut lin.reg growth data to valid ranges


## TODO:
## * manually calculate derivative dP/dt
## * growth rates use lm fit as start parameters for nls fit

### PARAMETERS
## smoothing
spar <-  0.5 # smooth.spline param spar for GFP/OD smoothing and derivative
## growth phases to treat separately (mu and expression rate)
phases <- c(.2,.45,.65)
odpoints <-  c(.15,.4,.6)
colors <- c("#0E0E0E","#0000E6","#ADBCE6")


## expression rate calculations
n <- 1 # plasmid copy number
t12 <- 5 # protein half-live, hours
delta <- log(2)/t12 # degradation rate = log(2) / protein half-life
## DPSEG PARAMETER
dpseg.P <- .001
dpseg.minl <- 5

## EXPERIMENT IDs
expids <- c("AP61","NG11","NG12","LW4","LW2","LW3")
## NOTE: order matters because EVC from LW4 is used for LW2/LW3

### loop through experiments
results <- list()
for ( expid in expids ) {

    ## redirect all "working" plots to a pdf
    if ( PLOT2PDF ) pdf(paste0(expid, "_all.pdf"))

    ## parse plate layout file
    map.file <- file.path(DATPATH, "data", paste0(expid,"_layout.xlsx"))
    map <- readPlateMap(file=map.file,fsep=";",
                        blank.id="LB", fields=c("strain","replicate"))
    
### PARSE DATA and INTERPOLATE TO GLOBAL TIME
    data.files <- file.path(DATPATH, "data", paste0(expid,".csv"))
    if ( expid%in%c("LW4","NG11","AP61") ) { # exported on exon
        raw <- readPlateData(file=data.files, type="Synergy",skip=56,
                             data.ids=c("600","GFP_50:480,520"),dec=".",
                             time.format="%I:%M:%S %p",time.conversion=1/3600)
    } else { # exported on intron
        raw <- readPlateData(files=data.files, type="Synergy",skip=56,
                             data.ids=c("600","GFP_50:480,520"), dec=",",
                             time.format="%H:%M:%S",time.conversion=1/3600)
    }

    ## set new names and colors
    raw <- prettyData(raw,yids=c(OD="600",GFP="GFP_50:480,520"),
                      col=c(OD="#333300",GFP=wavelength2RGB(650)))

    ## cut FIRST time-point: often noisy
    raw <- cutData(raw, xrng=c(raw$Time[2], tail(raw$Time,1)))
    
### OD BLANK CORRECTION and OD/GFP SMOOTHING

    ## skip obviously failed replicates
    skip <- ""
    if ( expid=="NG11" ) skip <- c("D4","D5","D6")
    if ( expid=="AP61" ) skip <- c("C3","A11")
    if ( any(skip!="") ) {

        plotdev(paste0(expid,"_plate_raw"), type=fig.type, width=12, height=8)
        viewPlate(raw, yids=c("OD","GFP"))
        dev.off()

        raw <- skipWells(raw, skip)
        map <- skipWells(map, skip)
       
        plotdev(paste0(expid,"_plate_skipped"), type=fig.type,
                width=12, height=8)
        viewPlate(raw, yids=c("OD","GFP"))
        dev.off()
    }

    ## add layout and replicate groups to plate data
    raw$layout <- map
    groups <- getGroups(map, by=c("strain"))
    raw$groups$group1 <- raw$groups$group2 <- groups


    ## number of replicate groups
    nrep <-  length(raw$groups$group2)

    ## reference and empty vector
    evcid <- "EVC"
    refid <- ifelse(expid=="AP61","D50+T7fw","D50.1.K")
    has.reference <- refid %in% map$strain
    has.evc <- evcid %in% map$strain
    
    ## for all-in-one summary plot
    allwells <- list(all=unlist(getGroups(map, by="strain")))

### BLANK CORRECTION: subtract blank and raise to positive values
    dat <- correctBlanks(raw, map, yids=c("OD","GFP"), base=0)

### SMOOTH DATA
    smooth.fun <- function(y,...) predict(smooth.spline(y ~ dat$Time,...))$y
    Os <-  apply(getData(dat, "OD"), 2, smooth.fun)
    Ys <-  apply(getData(dat, "GFP"), 2, smooth.fun, spar=spar)
    Ym <-  apply(Ys, 2, ma, 19)
    dat <- addData(dat, "ODs", Os, replace=TRUE)
    dat <- addData(dat, "GFPsm", Ym, replace=TRUE)
    dat <- addData(dat, "GFPs", Ys, replace=TRUE)
    
    ## inspect data
    viewPlate(dat, yids=c("OD","ODs"), ylim=c(0,1.25))
    viewPlate(dat, yids=c("GFP","GFPs"), ylim=c(0,1500))
    viewGroups(dat, yids=c("OD","ODs"), ylim=c(0,1.25))
    viewGroups(dat, yids=c("GFP","GFPs"), ylim=c(0,1500))

    plotdev(paste0(expid,"_plate_smoothed"), type=fig.type, width=12, height=8)
    viewPlate(dat, yids=c("OD","ODs", "GFP", "GFPs"),
              ylims=list(GFP=c(0,1500),GFPs=c(0,1500)))
    dev.off()
    

### MANUAL BLANK CORRECTION: GFP SIGNAL FROM EVC

    if ( has.evc ) {
        ##  linear regression on ALL EVC GFP channels
        dfgg <- data2growthrates(dat, "GFP", plate=dat$layout,
                                 wells=dat$groups$group1$EVC)
        fit.evc <- lm(value ~ time, data=dfgg)
        
        plotdev(paste0(expid,"_GFPBLANK"), width=3.5, height=3.5, type=fig.type)
        par(mai=c(.5,.5,.1,.1), mgp=c(1.3,.4,0), tcl=-.25)
        plot(dfgg$time, dfgg$value, pch=19, cex=.5, ylim=c(0,150))
        lines(dat$Time,predict(fit.evc, newdata=data.frame(time=dat$Time)),
              col=2, lwd=5)
        dev.off()
        
        GFPBLANK <- predict(fit.evc)
    } else { # ELSE: LW2 and LW3: use previous from LW4
        GFPBLANK <- results[["LW4"]]$GFPBLANK[,2]
    }

    ## TODO: subtract EVC GFP after OD interpolation!
    GFPb <- getData(dat, "GFPs") - GFPBLANK
    ## NOTE: raising above 0 removes noise but also raises EVC
    ## fold-change above 0
    ##GFPb <- GFPb - min(GFPb,na.rm=TRUE) # raise above 0
    dat <- addData(dat, "GFPb", GFPb)

    plotdev(paste0(expid,"_GFPBLANK_plate"), type=fig.type, width=12, height=8)
    viewPlate(dat, yids=c("GFP", "GFPb"),ylim=c(0,1500))
    dev.off()
    plotdev(paste0(expid,"_GFPBLANK_groups"), type=fig.type,
            width=2.5*nrep, height=8)
    viewGroups(dat, yids=c("GFP","GFPs", "GFPb"),ylim=c(0,1500))
    dev.off()

### CALCUALTE PROTEIN/CELL PROXY
    
    O <-  getData(dat, "ODs")  # NOTE: smoothed
    Y <-  getData(dat, "GFPb") # NOTE: smoothed & manually blank corrected!
    P <- Y/O
    if ( any(is.infinite(P)) ) {
        cat(paste("removing infinite GFP/OD values\n"))
        P[!is.finite(P)] <- NA # avoids bug in interpolation below
    }
    dat <- addData(dat, "GFP/OD", P, col="#ffbf00")

    ## example well
    viewPlate(dat, yids=c("OD","ODs","GFP","GFPb","GFP/OD"),well="B2")

### CALCULATE GROWTH RATES

    ## model exponential growth
    ## easylinear 1: finds maximal exponential growth rate
    dfg <- data2growthrates(cutData(dat,xrng=c(.5,6)), "ODs",
                            plate=dat$layout, wells=unlist(dat$groups$group1))
    fits1 <- all_easylinear(value ~ time | well, data=dfg, h=20)

    mdat <- addModel(fits1, dat, ID="easylinear", col=colors[1])
    
    ## get easylinear parameters and merge with plate layout info
    pars <- growthratesResults(fits1)
    grres <- merge(dat$layout, pars, by="well")
    rownames(grres) <- grres$well # to use well name as index
 
    ## LIN.REG. for first and second growth phase
    ## manual: lm fits for defined OD ranges 0-0.18, .2-0.5
    dfg <- data2growthrates(cutData(dat,xrng=c(.5,tail(dat$Time,1))), "ODs",
                            plate=dat$layout, wells=unlist(dat$groups$group1))
    ## cut data between pre-defined ODs (growth phases) and take log
    dfg2 <- dfg[dfg$value<.15,] 
    dfg2$value <- log(dfg2$value)
    dfg3 <- dfg[dfg$value>phases[1] & dfg$value<phases[2],] 
    dfg3$value <- log(dfg3$value)
    dfg4 <- dfg[dfg$value>phases[2] & dfg$value<phases[3],] 
    dfg4$value <- log(dfg4$value)

    fitm3 <- getData(dat,"ODs")
    fitm3[] <- NA
    fitm2 <- fitm4 <- fitm3
    grres <- cbind(grres, mu1=NA, mu2=NA, mu3=NA)
    for ( wid in unlist(dat$groups$group1) ) {
        well <- which(as.character(dfg2$well)==wid)
        fits2 <- lm(value  ~ time, data=dfg2[well,])
        newx2 <- predict(fits2, newdata=data.frame(time=mdat$Time))
        fitm2[,wid] <- exp(newx2)
        grres[wid,"mu1"] <- coef(fits2)[[2]]

        well <- which(as.character(dfg3$well)==wid)
        fits3 <- lm(value  ~ time, data=dfg3[well,])
        newx3 <- predict(fits3, newdata=data.frame(time=mdat$Time))
        fitm3[,wid] <- exp(newx3)
        grres[wid,"mu2"] <- coef(fits3)[[2]]

        well <- which(as.character(dfg4$well)==wid)
        fits4 <- lm(value  ~ time, data=dfg4[well,])
        newx4 <- predict(fits4, newdata=data.frame(time=mdat$Time))
        fitm4[,wid] <- exp(newx4)
        grres[wid,"mu3"] <- coef(fits4)[[2]]
        
        plot(dfg[as.character(dfg$well)==wid,1:2])
        lines(mdat$Time,exp(newx2),col=2)
        lines(mdat$Time,exp(newx3),col=4)
        lines(mdat$Time,exp(newx4),col=5)
    }
    mdat <- addData(mdat, "manual3", fitm4, col=colors[3])
    mdat <- addData(mdat, "manual2", fitm3, col=colors[2])
    mdat <- addData(mdat, "manual1", fitm2)

    ## inspect data
    viewPlate(mdat, yids=c("OD","easylinear","manual1","manual2","manual3"),
              ylim=c(0,1.5))#, xlim=c(0,7))
    viewGroups(mdat, yids=c("OD","easylinear","manual1","manual2","manual3"),
               ylim=c(0,1.5))#,xlim=c(0,7))
    viewPlate(mdat, yids=c("OD","easylinear","manual1","manual2","manual3"),
              ylim=c(0.05,1.5), log="y")#, xlim=c(0,7))
    viewGroups(mdat, yids=c("OD","easylinear","manual1","manual2","manual3"),
               ylim=c(0.05,1.5),log="y")#, xlim=c(0,7))
    
    
### EXPRESSION RATE k

    ## k = dP/dt + (d+mu)*P/n
    P <-  getData(mdat, "GFP/OD") ## protein level, RAW!
    
    ## get derivative via smooth.spline
    Ps <- apply(P, 2, function(y) {
        if (all(!is.na(y)))
            res <- smooth.spline(x=mdat$Time, y=y)
        else res <- NA
        res})
    dP <- lapply(Ps, function(x) {
        if ( class(x)=="smooth.spline")
            res <- predict(x, x=mdat$Time, deriv=1)$y
        else res <- rep(NA, length(mdat$Time))
        res
    })
    dP <- t(do.call("rbind", dP))
    ## use smoothed P also for calculation?
    sP <- lapply(Ps, function(x) {
        if ( class(x)=="smooth.spline")
            res <- predict(x, mdat$Time, deriv=0)$y
        else res <- rep(NA, length(mdat$Time))
        res
    })
    sP <- t(do.call("rbind", sP))

    mdat <- addData(mdat, "dP", dP, replace=TRUE)
    mdat <- addData(mdat, "sP", sP, replace=TRUE)

    viewPlate(mdat, yids=c("sP","GFP/OD"), ylim=c(0,2000))
    viewGroups(mdat, yids=c("sP","dP"))

    ## calculate predicted expression rate for
    ## easylinear growth rate of first phase, and
    ## manually fitted rate for second phase
    k1 <- t(((grres[colnames(dP),"mu"] +delta) * t(sP) + t(dP))/n)
    k2 <- t(((grres[colnames(dP),"mu2"]+delta) * t(sP) + t(dP))/n)
    k3 <- t(((grres[colnames(dP),"mu3"]+delta) * t(sP) + t(dP))/n)

    ## cut each growth and epxression rate for the range where mu was defined
    OD <- getData(mdat, "ODs")
    k1[!OD<phases[1]] <- NA
    k2[!(OD>phases[1]&OD<phases[2])] <- NA
    k3[!(OD>phases[2]&OD<phases[3])] <- NA
    if ( cut.growth ) {
        mdat$easylinear$data[!OD<phases[1]] <- NA
        mdat$manual2$data[!(OD>phases[1]&OD<phases[2])] <- NA
        mdat$manual3$data[!(OD>phases[2]&OD<phases[3])] <- NA
    }
    
    ## combine!
    kc <- k1
    kc[(OD>phases[1]&OD<phases[2])] <- k2[(OD>phases[1]&OD<phases[2])]
    kc[(OD>phases[2]&OD<phases[3])] <- k3[(OD>phases[2]&OD<phases[3])]
    
    mdat <- addData(mdat, "k1", k1, replace=TRUE, col=colors[1])
    mdat <- addData(mdat, "k2", k2, replace=TRUE, col=colors[2])
    mdat <- addData(mdat, "k3", k3, replace=TRUE, col=colors[3])
    mdat <- addData(mdat, "k", kc, replace=TRUE, col="#ffbf00")

    ## rate results
    plotdev(paste0(expid,"_rates"), type=fig.type, width=15, height=3)
    viewGroups(mdat, yids=c("k1","k2","k3"), ylim=c(-100,500))
    dev.off()
    ## rate results
    plotdev(paste0(expid,"_rates_plate"), type=fig.type, width=12, height=8)
    viewPlate(mdat, yids=c("k1","k2","k3"), ylim=c(-100,500))
    dev.off()

### FOLD-CHANGE wrt D50.1.K

    ## level: GFP/OD, rate: combined k
    if ( has.reference ) {
        
        kc <- getData(mdat, ID="k")
        km <- apply(kc[,dat$groups$group1[[refid]]], 1, median,na.rm=TRUE)
        kms <- rep(NA, length(km))
        fit <- smooth.spline(km[!is.na(km)] ~ mdat$Time[!is.na(km)], spar=spar)
        kms[!is.na(km)] <- predict(fit)$y
        kc <- kc/kms
        mdat <- addData(mdat, "rate_FC_time", kc, replace=TRUE, col="#ffbf00")

        plot(mdat$Time, km, type="l")
        lines(mdat$Time, kms, col=2)

        viewPlate(mdat, yids=c("rate_FC_time"))
        plotdev(paste0(expid,"_FC_rate_time"),type=fig.type,
                width=1*nrep,height=3)
        viewGroups(mdat, yids=c("rate_FC_time"),ylim=c(0,1.5))
        dev.off()
    }
    
### PLOTS

    ## MAXIMAL GROWTH RATE
    ## TODO: plot easylinear lag phases
    plotdev(paste0(expid,"_growthrates"), type=fig.type,
            width=1.3*nrep, height=3)
    par(mfcol=c(1,4),mai=c(1,.35,.1,.1), mgp=c(1.5,.4,0), tcl=-.25)
    boxplot(grres$lambda ~ grres$strain, las=2, ylim=c(0,1.5),
            ylab=expression("lag phase, h"))
    boxplot(grres$mu ~ grres$strain, las=2, ylim=c(0,1.5),
            ylab=expression("growth rate 1,"~mu*","~h^-1))
    boxplot(grres$mu2 ~ grres$strain, las=2, ylim=c(0,1.5),
            ylab=expression("growth rate 2,"~mu*","~h^-1))
    boxplot(grres$mu3 ~ grres$strain, las=2, ylim=c(0,1.5),
            ylab=expression("growth rate 3,"~mu*","~h^-1))
    dev.off()

    ## ALL DATA over time
    nr <- 3+show.rates
    plotdev(paste0(expid,"_alldata_time"), type=fig.type,
            width=3*nrep, height=6)
    par(mfrow=c(nr,nrep), mai=rep(.1,4), mgp=c(1,.2,0), tcl=-.1)
    viewGroups(mdat, yids=c("OD", "easylinear","manual2","manual3"),
               embed=TRUE, no.par=TRUE, yaxis=c(1,4), ylim=c(0,1.2))
    viewGroups(mdat, yids=c("OD", "easylinear","manual2","manual3"),
               embed=TRUE, no.par=TRUE, ylim=c(0.01,1),log="y")
    viewGroups(mdat, yids=c("GFP", "GFP/OD"),
               embed=TRUE, no.par=TRUE, yaxis=c(1,2), ylim=c(0,2e3))
    if ( show.rates )
        viewGroups(mdat, yids=c("k1","k2","k3"),
                   embed=TRUE, no.par=TRUE, yaxis=c(1,2), ylim=c(0,500))
    dev.off()
    
    plotdev(paste0(expid,"_alldata_time_plate"), type="pdf", width=12, height=6)
    viewPlate(mdat, yids=c("OD", "easylinear","manual2","manual3"),
              yaxis=c(1,4), ylim=c(0,1.2))
    viewPlate(mdat, yids=c("OD", "easylinear","manual2","manual3"),
              ylim=c(0.01,1),log="y")
    viewPlate(mdat, yids=c("GFP", "GFP/OD"), yaxis=c(1,2), ylim=c(0,2e3))
    viewPlate(mdat, yids=c("k1","k2","k3"), yaxis=c(1,2), ylim=c(0,500))
    dev.off()
    
### INTERPOLATE TO OD

    ## TODO: why does it not work for ODs?
    oddat <- interpolatePlateData(mdat, "OD")
    viewGroups(oddat)

    ## BOXPLOTS - compare GFP/OD at different OD
    levels <- rates <- list()
    ylm <- 1000
    if ( expid=="AP61" ) ylm <- 2000
    nr <- 2+show.rates
    plotdev(paste0(expid,"_boxplots"), type=fig.type,
            width=2*length(odpoints), height=3*nr)
    strf <- factor(grres$strain, levels=names(oddat$groups$group1))
    par(mfrow=c(nr,length(odpoints)), mai=c(.8,.5,.1,.1),
        mgp=c(1.8,.4,0), tcl=-.25)
    boxplot(grres$mu ~ strf, las=2, ylim=c(0,1.5),
            ylab=expression("growth rate 1,"~mu*","~h^-1))
    boxplot(grres$mu2 ~ strf, las=2, ylim=c(0,1.5),
            ylab=expression("growth rate 2,"~mu*","~h^-1))
    boxplot(grres$mu3 ~ strf, las=2, ylim=c(0,1.5),
            ylab=expression("growth rate 3,"~mu*","~h^-1))
    for ( i in 1:length(odpoints) ) 
        levels[[i]] <- boxData(oddat, rng=odpoints[i],
                               yid="GFP/OD",
                               groups=oddat$groups$group1,
                               ylim=c(-100,ylm), na.rm=TRUE,
                               ylab="rel.fluor. F = GFP/OD")
    if ( show.rates )
        for ( i in 1:length(odpoints) ) 
            rates[[i]] <- boxData(oddat,
                                  rng=odpoints[i], yid=paste0("k",i),
                                  groups=oddat$groups$group1,
                                  ylim=c(-100,ylm/2), na.rm=TRUE,
                                  ylab=expression("expression rate k, F"~h^-1))
    dev.off()
    if ( length(rates) )
        names(rates) <- names(levels) <- odpoints

### FOLD-CHANGE wrt D50.1.K vs. OD

    ## level: GFP/OD, rate: combined k
    levels.fc <- rates.fc <- list()
    if ( has.reference ) {

        kc <- getData(oddat, ID="k")
        km <- apply(kc[,oddat$groups$group1[[refid]]], 1, median,na.rm=TRUE)
        kms <- rep(NA, length(km))
        fit <- smooth.spline(km[!is.na(km)] ~ oddat$OD[!is.na(km)], spar=spar)
        kms[!is.na(km)] <- predict(fit)$y
        kc <- kc/kms
        oddat <- addData(oddat, "rate_FC", kc, replace=TRUE, col="#ffbf00")

        plot(oddat$OD, km, type="l")
        lines(oddat$OD, kms, col=2)

        viewPlate(oddat, yids=c("rate_FC"))
        plotdev(paste0(expid,"_FC_rate"), type=fig.type, width=1*nrep, height=3)
        viewGroups(oddat, yids=c("rate_FC"),ylim=c(0,1.5))
        dev.off()

        ## level: use OD-interpolated GFP directly, since OD cancels out
        kc <- getData(oddat, ID="GFPs")

        ## SUBTRACT EVC
        if ( has.evc ) {

            ## get EVC mean and smooth
            ke <- apply(kc[,oddat$groups$group1[[evcid]]], 1, mean,na.rm=TRUE)
            kes <- rep(NA, length(ke))
            fit <- smooth.spline(ke[!is.na(ke)] ~ oddat$OD[!is.na(ke)],
                                 spar=spar)
            kes[!is.na(ke)] <- predict(fit)$y
        } else { # ELSE: LW2 and LW3: use previous from LW4
            ## TODO: properly store and retrieve
        }

        ## subtract EVC
        kc <- kc -kes

        ## divide by reference
        ## get mean and smooth 
        km <- apply(kc[,oddat$groups$group1[[refid]]], 1, mean,na.rm=TRUE)
        kms <- rep(NA, length(km))
        fit <- smooth.spline(km[!is.na(km)] ~ oddat$OD[!is.na(km)], spar=spar)
        kms[!is.na(km)] <- predict(fit)$y
        kc <- kc/kms
        oddat <- addData(oddat, "level_FC", kc, replace=TRUE, col="#ADBCE6")

        plot(oddat$OD, km, type="l")
        lines(oddat$OD, kms, col=2)

        viewPlate(oddat, yids=c("level_FC"),ylim=c(0,1.5))
        plotdev(paste0(expid,"_FC_level"), type=fig.type,
                width=1*nrep, height=3)
        viewGroups(oddat, yids=c("level_FC"),ylim=c(0,1.5))
        dev.off()

        ## BOXPLOTS - compare GFP/OD at different OD
        nr <- 2+show.rates
        plotdev(paste0(expid,"_foldchange"), type=fig.type,
                width=2*length(odpoints), height=nr*3)
        strf <- factor(grres$strain, levels=names(oddat$groups$group1))
        par(mfrow=c(nr,length(odpoints)), mai=c(.8,.5,.1,.1),
            mgp=c(1.8,.4,0), tcl=-.25)
        boxplot(grres$mu ~ strf, las=2, ylim=c(0,1.5),
                ylab=expression("growth rate 1,"~mu*","~h^-1))
        boxplot(grres$mu2 ~ strf, las=2, ylim=c(0,1.5),
                ylab=expression("growth rate 2,"~mu*","~h^-1))
        boxplot(grres$mu3 ~ strf, las=2, ylim=c(0,1.5),
                ylab=expression("growth rate 3,"~mu*","~h^-1))
        for ( i in 1:length(odpoints) ) 
            levels.fc[[i]] <- boxData(oddat, rng=odpoints[i],
                                      yid="level_FC",
                                      groups=oddat$groups$group1,
                                      ylim=c(-.1,1.5), na.rm=TRUE,
                                      ylab="fold-change, level")
        if ( show.rates )
            for ( i in 1:length(odpoints) ) 
                rates.fc[[i]] <- boxData(oddat,
                                         rng=odpoints[i], yid="rate_FC",
                                         groups=oddat$groups$group1,
                                         ylim=c(-.1,1.5), na.rm=TRUE,
                                         ylab="fold-change, rate")
        dev.off()
        if ( show.rates )
            names(rates.fc) <-  odpoints
        names(levels.fc) <- odpoints
    }
      
    ## all data over OD
    nr <- 2+as.numeric(has.reference)+show.rates
    plotdev(paste0(expid,"_alldata_OD"), type=fig.type,
            width=3*nrep, height=2*nr)
    par(mfrow=c(nr,nrep), mai=rep(.1,4), mgp=c(1,.2,0), tcl=-.1)
    viewGroups(oddat, yids=c("easylinear","manual2","manual3"),
               embed=TRUE, no.par=TRUE, yaxis=c(1,4), ylim=c(0.1,1.2),log="y")
    viewGroups(oddat, yids=c("GFP", "GFP/OD"),
               embed=TRUE, no.par=TRUE, yaxis=c(1,2), ylim=c(0,2e3))
    if ( show.rates ) 
        viewGroups(oddat, yids=c("k1","k2","k3"),
                   embed=TRUE, no.par=TRUE, yaxis=c(1,2), ylim=c(-100,500))
    if ( has.reference ) {
        if ( show.rates )
            viewGroups(oddat, yids=c("rate_FC","level_FC"),
                       embed=TRUE, no.par=TRUE, yaxis=c(1,2), ylim=c(-.1,1.5))
        else
            viewGroups(oddat, yids=c("level_FC"),
                       embed=TRUE, no.par=TRUE, yaxis=c(1,2), ylim=c(-.1,1.5))
    }
    dev.off()
   
    ## close PDF
    if ( PLOT2PDF ) dev.off()

    ## merge all results into one big result table
    allres <- grres
    if ( has.reference )
        for ( od in as.character(odpoints) ) {
            allres <- merge(allres, levels[[od]][,c(1,3)], by="well")
            allres <- merge(allres, levels.fc[[od]][,c(1,3)], by="well")
            if ( show.rates ) {
                allres <- merge(allres, rates[[od]][,c(1,3)], by="well")
                allres <- merge(allres, rates.fc[[od]][,c(1,3)], by="well")
            }        
        }
    allres <- cbind(experiment=expid, allres)

    ## SUMMARY FIGURE
    means <- colors <- NULL
    if ( has.reference ) {

        ## SELECT COLORS BY FOLD-CHANGE
        tmp <- levels.fc[["0.4"]]
        means <- sapply(names(mdat$groups$group2), function(x) {
            mean(tmp[as.character(tmp[,2])==x,3])
        })
        ## normalize between 0.3 and 1 for color gradient
        colors <- c(sort(means, decreasing=TRUE) - .3)/(1-.3)
        colors <- round(100*colors+1)
        colors[colors<1] <- 1
        colors[colors>100] <- 100
        colors[] <- rev(colorRamps::matlab.like2(101))[colors]
        colors[refid] <- "#000000"
        if ( has.evc )
            colors[evcid] <- "#999999"
        ## resort groups
        mdat$groups$group2 <- mdat$groups$group2[names(colors)]
        oddat$groups$group2 <- oddat$groups$group2[names(colors)]
        
        plotdev(paste0(expid,"_summary"), type=fig.type,
                width=6+.3*(nrep+1), height=3, res=300)
        layout(t(1:3), widths=c(1.1,1,.1*(nrep+1)))
        par(mai=c(.36,.4,.1,.36), mgp=c(1.5,.3,0), tcl=-.25)
        viewGroups(mdat, yids=c("GFP","OD"),embed=TRUE, no.par=TRUE,
                   yaxis=c(1,2), ylims=list(ODs=c(0,.95), GFPb=c(0,1320)),
                   groups=allwells, group2.col=colors,
                   g2.legpos="topleft", g1.legpos="bottomright",
                   g1.legend=TRUE, xlab="time, h", lwd.orig=0)#, xlim=c(0,16), log="y")
        mtext("GFP Fluorescence, a.u.", 4, par("mgp")[1])
        mtext(expression(OD["600 nm"]), 2, par("mgp")[1])
        ##par(mai=c(.36,.36,.1,.01), mgp=c(1.5,.4,0), tcl=-.25)
        ##viewGroups(mdat, yids=c("GFP/OD"),embed=TRUE, no.par=TRUE,
        ##           ylim=c(0,1500),groups=allwells, group2.col=colors,
        ##           g2.legpos="topleft", g1.legend=FALSE,
        ##           ylab="GFP/OD", xlab="time, h", lwd.orig=0)#, xlim=c(0,16))
        par(mai=c(.36,.36,.1,.01), mgp=c(1.6,.3,0))
        viewGroups(cutData(oddat, xrng=c(.05,.7)),
                   yids=c("level_FC"),embed=TRUE, no.par=TRUE,
                   ylim=c(-0.1,1.25),groups=allwells, group2.col=colors,
                   g2.legend=FALSE, g1.legend=FALSE,
                   ylab="fold-change", xlab=expression(OD["600 nm"]),
                   xlim=c(0.05,.7), lwd.orig=0)
        abline(v=0.4)
        abline(h=0:1,lwd=.5)
        par(mai=c(.36,0,.1,.36))
        plot(1,col=NA,axes=FALSE,ylab=NA,xlab=NA,ylim=c(-0.1,1.25))
        abline(h=0:1,lwd=.5)
        par(new=TRUE, xpd=TRUE)
        tmp <- levels.fc[["0.4"]]
        boxplot(tmp[,3] ~ factor(tmp[,2], levels=names(colors)),
                ylim=c(-0.1,1.25), border=colors, las=2, lwd=2,axes=FALSE)
        axis(4)
        axis(2,labels=NA)
        text(1:length(colors), rep(-.2,length(colors)),
             names(colors), srt=90, font=2) #, col=colors
        ##shadowtext(1:length(colors), rep(-.1,length(colors)),
        ##           names(colors), srt=90, 
        ##           auto.bg=TRUE, r=.1, col=colors)
        mtext(expression("fold-change at"~OD["600 nm"]*"=0.4"),4,par("mgp")[1])
        par(xpd=NA)
        dev.off()
    }
    
    
    ## STORE
    ## TODO: use "table", and sorting/colors by meany in
    ##       below plots over all experiments
    resl <- list(mdat=mdat, oddat=oddat,
                 table=allres,
                 levels=levels, rates=rates,
                 levels.fc=levels.fc, rates.fc=rates.fc,
                 means=means, colors=colors,
                 GFPBLANK=cbind(dat$Time, GFPBLANK)) # store for use in LW2/LW3
    results[[expid]] <- resl
}

## RESULT TABLE
ncol <- rep(NA, length(results))
nlevels <- ncolors <- table <- NULL
for ( i in 1:length(results) ) {
    ids <- paste(names(results)[i],
                 names(results[[i]]$colors),sep="_")
    cols <- results[[i]]$colors
    names(cols) <- ids
    nlevels <- c(nlevels, ids)
    ncolors <- c(ncolors, cols)
    ncol[i] <- length(results[[i]]$colors)
    table <- rbind(table, results[[i]]$table)
}


## comparative plot EVC
## outdated, since EVC over OD is not used anymore
## but it looked great; if we re-use it this plot
## shows that there is a direct negative correlation
## of Fluor/OD to OD, linear in log-log plot
if  ( FALSE ) {
    plotdev("allplates_EVC", width=3.5, height=3.5, res=200, type=fig.type)
    par(mai=c(.5,.5,.1,.1), mgp=c(1.3,.4,0), tcl=-.25)
    plot(results$LW4$EVC, type="b", cex=.2, col=4, log="xy", xlab="OD", ylab="GFP/OD for EVC")
    lines(results$AP61$EVC, ylim=c(0,5000), xlim=c(0,1.2), type="b", cex=.2, col=1, lty=1)
    lines(results$NG11$EVC, ylim=c(0,4000), xlim=c(0,1.2), type="b", cex=.2, col=2, lty=1)
    lines(results$NG12$EVC, ylim=c(0,4000), xlim=c(0,1.2), type="b", cex=.2, col=3, lty=1)
    legend("topright", c("AP61", "NG11", "NG12", "LW4"), col=1:4, lty=1, pch=1, pt.cex=.2)
    dev.off()
}

plotdev("alldata_levels", height=3*length(odpoints), width=10, type=fig.type)
par(mfcol=c(length(odpoints),1), mai=c(.8,.5,.05,.1),mgp=c(1.75,.5,0),tcl=-.25)
for ( rn in as.character(odpoints) ) {
    allres <- NULL
    ncol <- rep(NA, length(results))
    nlevels <- ncolors <- nids <- NULL
    for ( i in 1:length(results) ) 
        if ( length(results[[i]]$levels.fc) ) {
            res <- results[[i]]$levels.fc[[rn]]
            id <- names(results)[i]
            res$group <- paste(id, res$group, sep="_")
            allres <- rbind(allres, res)
            nlevels <- c(nlevels, paste(id, names(results[[i]]$colors),sep="_"))
            ncolors <- c(ncolors, results[[i]]$colors)
            ncol[i] <- length(unique(res$group))
        }
    boxplot(allres[,3] ~ factor(allres$group, levels=nlevels), border=ncolors,
            las=2, ylab="GFP fold-change", ylim=c(-.05,1.5),axes=FALSE)
    axis(1, at=1:length(nlevels), labels=sub(".*_","",nlevels),las=2)
    axis(2);box();
    abline(h=0:1)
    abline(v=cumsum(ncol)+.5)
    text(cumsum(ncol), 1.45, names(results),cex=1.5, font=2, pos=2)
    legend("topleft", paste("at OD", rn))
    
}
dev.off()


if ( show.rates ) {
    plotdev("alldata_rates", height=3*length(odpoints), width=10, type=fig.type)
    par(mfcol=c(length(odpoints),1), mai=c(1.25,.5,.1,.1),
        mgp=c(1.75,.5,0), tcl=-.25)
    for ( rn in as.character(odpoints) ) {
        allres <- NULL
        ncol <- rep(NA, length(results))
        nlevels <- ncolors <- NULL
        for ( i in 1:length(results) ) 
            if ( length(results[[i]]$rates.fc) ) {
                res <- results[[i]]$rates.fc[[rn]]
                id <- names(results)[i]
                res$group <- paste(id, res$group, sep="_")
                allres <- rbind(allres, res)
                nlevels <- c(nlevels, paste(id,
                                            names(results[[i]]$colors),sep="_"))
                ncolors <- c(ncolors, results[[i]]$colors)
                ncol[i] <- length(unique(res$group))
            }
        boxplot(allres[,3] ~ factor(allres$group, levels=nlevels),
                border=ncolors,
                las=2, ylab="expression rate fold-change", ylim=c(-.05,1.5))
        abline(h=0:1)
        abline(v=cumsum(ncol)+.5)
        legend("topright", paste("at OD", rn))
        
    }
    dev.off()
}
