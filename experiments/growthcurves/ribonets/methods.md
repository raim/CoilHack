## Platereader Data Processing

Raw data were exported from the `Synergy` platereader, and visually
inspected and processed with our in-house R package `platexpress`
(https://github.com/raim/platexpress).

TODO: use equations for R `approx`, lin.reg.-based blank correction and fold-change

* **Pre-processing**: Optical density (OD, at 600 nm) and fluorescence
(GFP, Ex/Em: 480 nm/520 nm) data were linearly interpolated to common
time points (base R function `approx`), i.e., with slight
smoothing of raw data. The first time point was removed from all data
to avoid noise from adaptation to the platereader culture
temperature. Wells with obvious growth artefacts were removed. The
median of OD measurements from blank wells (LB medium only) was
subtracted from all OD values. And finally, OD and GFP data were
further smoothed with the base R function `smooth.spline` (default parameters
and `spar=0.5`).

* **Growth rates** were calculated from the smoothed OD data. The first
exponential phase (until OD of circa 0.2) was fitted with the R package
`growthrates` function `all_easylinear` (span parameter
`h=20`). Second and third phase growth rates were obtained by linear
regression (R function `lm`) of the logarithm of the OD measurement in
OD ranges 0.2-0.45 and 0.45-0.65.

* **Fold-changes**: Smoothed fluorescence data was interpolated (R
function `approx`) to equispaced OD values. A linear regression line
(R function `lm`) of the empty vector control (EVC) fluorescence was
subtracted to correct for the dynamic LB medium and biomass
auto-fluorescence over the growth curve. The EVC values from
experiment `LW4` were used for the experiments `LW3` and `LW2` which
did not have an EVC.  Mean values over replicates of the reference
strain fluorescence (strain `D50.1.K`, or `D50+T7fw` for experiment
`AP61`) were calculated and smoothed (`smooth.spline` with `spar=0.5`)
for each experiment. The fold-change was calculated as the ratio of
all corrected fluorescence data over this smoothed mean fluorescence
of the reference strain replicates.  The resulting curves where then
sampled (interpolated with `approx`) at OD 0.15, 0.4 and 0.6,
representing all three growth phases, for boxplots.


### Minor notes:

Original platereader data had been embedded into Excel sheets and had
further processed manually there. Manually added columns to
results were removed and the sheets corresponding to the original
data were exported from LibreOffice as csv. Data were exported on
different machines with different German (LW2, LW3) or English (LW4,
NG11, AP61) defaults for decimal numbers.
