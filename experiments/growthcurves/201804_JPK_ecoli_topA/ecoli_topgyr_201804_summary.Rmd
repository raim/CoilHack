---
title: "Coil-hack: topA and gyrA/B overexpression in *E.coli*"
author: Anna Behle, Jan-Philipp Kunz, Rainer Machne
date: 21/May/2018
output: 
  pdf_document:
    fig_caption: yes
    toc: true
    toc_depth: 2
bibliography: /home/raim/work/CoilHack/references/coilhack.bib
---

```{r setup, include=FALSE}
## SET PATHS
SRCPATH <- "/home/raim/work/CoilHack/"
DATPATH <- file.path(file.path(SRCPATH,"experiments","201804_JPK_ecoli_topA"))
OUTPATH <- file.path(file.path(SRCPATH,"reports","201804_JPK_ecoli_topA"))
setwd(OUTPATH)
knitr::opts_chunk$set(root.dir=OUTPATH)

## load library
library(platexpress)
## load data from the main evaluation script
load("201804_ecoli_topA.RData")
source("~/programs/platexpress/R/platereader.R")
source("~/programs/platexpress/R/parsers.R")


## rm plate from "well" column, and store as rownames
rownames(rates) <- rates$well
rates$well <- sub(".*\\.","",rates$well)
## re.color OVER ALL experiments
##rates <- amountColors(rates)

grparams <- paste0("OD_",c("lambda","mu","A")) # 
grlims <- lapply(grparams, function(val)
  range(rates[,val],na.rm=T))
names(grlims) <- grparams
grlims[["OD_mu"]][1] <- 0
grlims[["OD_A"]][1] <- 0

```

# Introduction

## The aTc/tetR System

**TODO**: construct figures/tables
 
Anhydrotetracycline (aTc) binds tetR about 35-fold stronger than
tetracycline [@Degenkolb1991] while having a much lower
antimicrobial activity, which itself may not involve ribosomes and
inhibition of translation, but rather cause direct lysis as membrane
perturbants [@Oliva1992, @Chopra1994].

The tetR-responsive promotor PLtetO-1 provides excellent range
of induction from 0 to 100 ng/mL aTc in E.coli [@Lutz1997].

The L03 promoter showed a wide-dynamic range of responses
from 0 to 10000 ng/mL in Synechocystis and no effects on
growth were reported, but the dose-response curve appears
different above 100 ng/mL [@Huang2013].

Significant growth inhibition at 200 ng/mL in *Clostridium
acetobutylicum* [@Dong2012] and at 500 ng/mL in *Mycobacterium
smegmatis* [@Ehrt2005] but series of increasing **mild inhibition may
already be seen at lower concentrations**. No effects on growth are
mentioned up to 10 ug/mL in Synechocystis [@Huang2013], but they only
showed end-point measurements and no growth curves.

A hormetic response to low concentrations (15-30 ng/mL) of
tetracycline has been observed [@Migliore2013].

aTc has a half-life of about 20 h, decreasing strongly with
increasing temperature [@Politi2014]. **TODO: light-dependence of aTc**

## The IPTG/lacI System

**TODO**

# Methods

## Strains and Expression Plasmids

## Culturing
Overnight cultures in LB or M9 to OD xx, well-plate cultures were
inoculated to an OD of 0.1 in the same medium, spiked with the indicated
concentrations of inducer, covered with a drop of mineral oil
to avoid evaporation of cultures, and incubated in the BMG Optima Fluostar
platereader at 37C.

See Appendix for lab notes to each experiment.

## Data Processing & Model Fitting

Our in-houseR package `platexpress` was used to process the data, the package
`grofit` to fit phenomenological models of microbial growth
(Richards, Gompertz, Gompertz exp.).

# Results

DETAILED RESULTS are available in the report 201804_report.pdf !

Overall effects of aTc/IPTG-induced expression of topA and gyrA or
gyrB overexpression on fitted growth rates \(\mu\) are shown in Figure \ref{fig:rates}. We started
with inducer ranges as used in the papers describing the used
promoters, and progressively adapted concentration ranges to see
effects on growth rates.



```{r, echo = FALSE, message = FALSE, warning = FALSE, fig.width=8,fig.height=10,fig.cap="\\label{fig:topA_Ec} **topA_Ec**"}
strains <- c("topA_Ec","topA_Sc","gyrA_Ec","gyrB_Ec")
strain <- "topA_Ec"
#for ( strain in strains ) {
    ## get all individual experiments/plate
par(mfrow=c(5,2), mai=c(.5,.5,.01,.01),mgp=c(1.2,.3,0))
splates <- as.character(unique(rates$plate[which(rates$strain==strain)]))
for (splate in splates ) {
  #cat(paste(strain, splate, "\n"))
  idx <- which(rates$plate==splate & rates$strain==strain)
  grp <- getGroups(rates[idx,], by=c("plate","strain","medium","substance"), verb=FALSE)
  
  for ( i in 1:length(grp) ) {
    #cat(paste(names(grp)[i],"\n"))
    grp2 <- getGroups(rates[idx,], by=c("plate", "strain","medium","substance","amount"), verb=FALSE)
    ## get amount-based colors
    grp2.col <- groupColors(rates[idx,], grp2)
    #par(mfcol=c(1,2), mai=c(.5,.5,.01,.01),mgp=c(1.2,.3,0))
    viewGroups(plates[[splate]], grp[i], grp2, dids="584",
               embed=TRUE,no.par=TRUE,group2.col=grp2.col,
               show.ci95=FALSE,lwd.orig=NA,g1.legend=FALSE,
               g2.legend=FALSE,g2.legpos = "topleft",verb=FALSE)
    mtext(expression("OD"["584 nm"]), 2, par("mgp")[1], col="#0000FF")
    for ( val in "OD_mu" ) #grparams )
      doseResponse(rates[idx,], grp[[i]],val=val,ylim=grlims[[val]])
    
  }
}
```


```{r, echo = FALSE, message = FALSE, warning = FALSE, fig.width=8,fig.height=6,fig.cap="\\label{fig:topA_Sc} **topA_Sc**"}
strain <- "topA_Sc"
## get all individual experiments/plate
par(mfrow=c(3,2), mai=c(.5,.5,.01,.01),mgp=c(1.2,.3,0))
splates <- as.character(unique(rates$plate[which(rates$strain==strain)]))
for (splate in splates ) {
  #cat(paste(strain, splate, "\n"))
  idx <- which(rates$plate==splate & rates$strain==strain)
  grp <- getGroups(rates[idx,], by=c("plate","strain","medium","substance"), verb=FALSE)
  
  for ( i in 1:length(grp) ) {
    #cat(paste(names(grp)[i],"\n"))
    grp2 <- getGroups(rates[idx,], by=c("plate","strain","medium","substance","amount"), verb=FALSE)
    ## get amount-based colors
    grp2.col <- groupColors(rates[idx,], grp2)
    #par(mfcol=c(1,2), mai=c(.5,.5,.01,.01),mgp=c(1.2,.3,0))
    viewGroups(plates[[splate]], grp[i], grp2, dids="584",
               embed=TRUE,no.par=TRUE,group2.col=grp2.col,
               show.ci95=FALSE,lwd.orig=NA,g1.legend=FALSE,
               g2.legend=FALSE,g2.legpos = "topleft",verb=FALSE)
    mtext(expression("OD"["584 nm"]), 2, par("mgp")[1], col="#0000FF")
    for ( val in "OD_mu" ) #grparams )
      doseResponse(rates[idx,], grp[[i]],val=val,ylim=grlims[[val]])
    
  }
}
```


```{r, echo = FALSE, message = FALSE, warning = FALSE, fig.width=8,fig.height=4,fig.cap="\\label{fig:gyrA_Ec} **gyrA_Ec**"}
strain <- "gyrA_Ec"
## get all individual experiments/plate
par(mfrow=c(2,2), mai=c(.5,.5,.01,.01),mgp=c(1.2,.3,0))
splates <- as.character(unique(rates$plate[which(rates$strain==strain)]))
for (splate in splates ) {
  #cat(paste(strain, splate, "\n"))
  idx <- which(rates$plate==splate & rates$strain==strain)
  grp <- getGroups(rates[idx,], by=c("plate","strain","medium","substance"), verb=FALSE)
  
  for ( i in 1:length(grp) ) {
    #cat(paste(names(grp)[i],"\n"))
    grp2 <- getGroups(rates[idx,], by=c("plate","strain","medium","substance","amount"), verb=FALSE)
    ## get amount-based colors
    grp2.col <- groupColors(rates[idx,], grp2)
    #par(mfcol=c(1,2), mai=c(.5,.5,.01,.01),mgp=c(1.2,.3,0))
    viewGroups(plates[[splate]], grp[i], grp2, dids="584",
               embed=TRUE,no.par=TRUE,group2.col=grp2.col,
               show.ci95=FALSE,lwd.orig=NA,g1.legend=FALSE,
               g2.legend=FALSE,g2.legpos = "topleft",verb=FALSE)
    mtext(expression("OD"["584 nm"]), 2, par("mgp")[1], col="#0000FF")
    for ( val in "OD_mu" ) #grparams )
      doseResponse(rates[idx,], grp[[i]],val=val,ylim=grlims[[val]])
    
  }
}
```


```{r, echo = FALSE, message = FALSE, warning = FALSE, fig.width=8,fig.height=8,fig.cap="\\label{fig:gyrB_Ec} **gyrB_Ec**"}
strain <- "gyrB_Ec"
par(mfrow=c(4,2), mai=c(.5,.5,.01,.01),mgp=c(1.2,.3,0))
## get all individual experiments/plate
splates <- as.character(unique(rates$plate[which(rates$strain==strain)]))
for (splate in splates ) {
  #cat(paste(strain, splate, "\n"))
  idx <- which(rates$plate==splate & rates$strain==strain)
  grp <- getGroups(rates[idx,], by=c("plate","strain","medium","substance"), verb=FALSE)
  
  for ( i in 1:length(grp) ) {
    #cat(paste(names(grp)[i],"\n"))
    grp2 <- getGroups(rates[idx,], by=c("plate","strain","medium","substance","amount"), verb=FALSE)
    ## get amount-based colors
    grp2.col <- groupColors(rates[idx,], grp2)
    #par(mfcol=c(1,2), mai=c(.5,.5,.01,.01),mgp=c(1.2,.3,0))
    viewGroups(plates[[splate]], grp[i], grp2, dids="584",
               embed=TRUE,no.par=TRUE,group2.col=grp2.col,
               show.ci95=FALSE,lwd.orig=NA,g1.legend=FALSE,
               g2.legend=FALSE,g2.legpos = "topleft",verb=FALSE)
    mtext(expression("OD"["584 nm"]), 2, par("mgp")[1], col="#0000FF")
    for ( val in "OD_mu" ) #grparams )
      doseResponse(rates[idx,], grp[[i]],val=val,ylim=grlims[[val]])
    
  }
}
```


## Fitting Growth Models with `grofit`

See files \<DATE\>_allfits.pdf for details of the data fits. Systematic errors
can also be seen in the summary report 201804_report.pdf.

Fitting to growth models ("Gompertz", "Gompertz-exp", "Richards") by the R package `grofit`
works surprisingly well, but it hides complex growth in LB. Systematic errors are introduced for complex growth curves (Fig. \ref{fig:fits}), eg. the increasing
growth rate of topA_Ec at aTc>0 in LB, experiment from 20180502 (cyan in Fig. \ref{fig:rates},
lower left panel) may simply stem from the variations in the first LB growth phase.

-> use M9 medium, or LB supplemented
with glucose

```{r, echo = FALSE, message = FALSE, warning = FALSE, fig.width=5.5,fig.height=4,fig.cap="\\label{fig:fits} Model Fits: the good, the bad and the ugly; note the complex growth curves in LB medium in lower two panels"}
par(mfrow=c(2,2),mai=c(.4,.4,.05,.05),mgp=c(1.2,.3,0))
## good fit:
plate <- "20180418"
plot(plates[[plate]]$Time, plates[[plate]]$OD$data[,"A1"],xlab="Time, h",ylab="OD")#,log="y")
lines(plates[[plate]]$Time,plates[[plate]]$OD_model$data[,"A1"],col="blue",lwd=2)
## bad fits:
plot(plates[[plate]]$Time, plates[[plate]]$OD$data[,"A2"],xlab="Time, h",ylab="OD")#,log="y")
lines(plates[[plate]]$Time,plates[[plate]]$OD_model$data[,"A2"],col="blue",lwd=2)
plate <- "20180502"
plot(plates[[plate]]$Time, plates[[plate]]$OD$data[,"A2"],xlab="Time, h",ylab="OD")#,log="y")
lines(plates[[plate]]$Time,plates[[plate]]$OD_model$data[,"A2"],col="blue",lwd=2)
plot(plates[[plate]]$Time, plates[[plate]]$OD$data[,"A11"],xlab="Time, h",ylab="OD")#,log="y")
lines(plates[[plate]]$Time,plates[[plate]]$OD_model$data[,"A11"],col="blue",lwd=2)
legend("right",c("OD","model fit"),col=c("black","blue"), pch=c(1,NA), lty=c(NA,1), lwd=1:2)
```

## Effects on Growth Rate


* **SLOW GROWTH**: We generally observed rather low growth rates, ca. \(\mu=0.4 \text{h}^{-1}\) in LB
and ca. \(\mu=0.15 \text{h}^{-1}\) in M9 medium (Fig. \ref{fig:rates}), corresponding to doubling times \(t_2=\frac{\ln 2}{\mu}\) of \(1.7\text{h}\) and \(6.9 \text{h}\), resp. The otherwise
nice experiment 20180502 showed unusually slow growth also in LB for yet unclear reasons.
It may also be an artefact of data processing upstream (by the BMG software).

* **GROWTH INHIBITION by aTc/topA**: We see a strong inhibition of growth only in the topA strains;
however, aTc is toxic at the used concentrations. The clear
differences between E.coli and PCC6803 topA (topA_Ec and topA_Sc,
resp.) indicate that we do also see effects of topA expression. But
these need to be further verified, most importantly we need empty
vector and wildtype controls.

* **gyrA/B Effects?**: The growth model fits show slight evidence of an increasing growth rate with
increasing gyrA or gyrB expression (or with increasing IPTG concentration, which however
should usually decrease growth rate due to additional load by induction of internal lac operon).

```{r, echo = FALSE, message = FALSE, warning = FALSE, fig.width=6,fig.height=4,fig.cap="\\label{fig:rates} Results Summary: effect on growth rates; Note, that systematic errors of fitting growth rates on LB medium may bias the more subtle trends; eg. the growth rate at aTc>0 of strain topA_Ec, experiment 20180502 (cyan). "}

## common colors by plate/medium
exp <- paste(rates[,"plate"],rates[,"medium"])
pch <- cols <- as.numeric(factor(exp))

## recursive doseResponse
## re-add plate-wise well names for group doseResponse plots
rates$well <- rownames(rates)
grp <- getGroups(rates, by=c("plate","strain","medium","substance"), verb=FALSE)
grp2 <- getGroups(rates, by=c("plate","strain","medium","substance","amount"), verb=FALSE)
rates$plateColor <- cols
grp.col <- groupColors(rates,grp,color="plateColor")

strains <- c("topA_Ec","topA_Sc","gyrA_Ec","gyrB_Ec")
par(mfrow=c(2,2),mai=c(.5,.5,.05,.05),mgp=c(1.2,.3,0))
for ( strain in strains ) {
  tmp <- doseResponse(rates,wells=grp[grep(strain,names(grp))],line=T,pch=NA,
                      val="OD_mu",col=grp.col,ylim=grlims[["OD_mu"]],verb=FALSE)
  leg <- unlist(lapply(tmp, function(x) x[1,"color"]))
  nms <- sub("_.*","",names(leg))
  legend("topright",c(strain,nms),lty=c(NA,rep(1,length(leg))),
         col=c(1,leg),bg="#FFFFFF99",box.lwd = NA,cex=.7)
}
```


## Complex Growth Phases in LB + aTc/topA

Overexpression of topA and/or poisoning of cells by aTc nicely reveals the reported
multiple growth phases (diauxie) in LB medium, with differential effects on either
the fast early or the late phase. Inducing topA_Ec generally allows cells to only complete
the first growth phase (at lower rates) then reaching stationary phase at a much lower
OD. Inducing topA_Sc also slows down the first growth phase, followed by a short stationary
phase (similar to top_Ec), then slow growth to a higher OD that is usually still lower
then uninduced controls.

Additionally, we sometimes observed a direct effect
on the lag phase.

* LB vs. M9
* differential effects on early/late exponential phase responses (OD .3, 1)
* lag phase and yield responses
* fitting errors

## Reproducability
* strangely low growth rate in LB, last experiment
* LB vs M9 .. vs LB: reproducability?



# Appendix A - Lab Notes by JPK
## 20180416
* IPTG dient als Induktor für gyrase – Konstrukte
* Anhydro Tetracyclin (aTc)dient als Induktor für die Topoisomer-Konstrukte
* Für grobes Screening der Induktorkonzentrationen wurde für aTc ein Bereich von 0 -10 ug/mL (0;2;4;6;8;10 ug/mL) gewählt, für IPTG ein Konzentrationsbereich von 0 – 1000 uM (0;200;400;600;800 uM). 
* 20180415: E. coli üN Kulturen der Konstrukte pSHDY\*gyrA, pSHDY\*gyrB, pSHDY\*topA, pSHDY\*topA aus Synechocystis 6803 
* 20180416: 75 mL Kulturvolumen der gestrigen üN Kultur wurden bei 3400 g zentrifugiert. Die erhaltenen Zellpellets wurden bei -20°C eingefroren und gelagert. 
*  Plattenleserexperiment in LB-Medium der Konstrukte topA aus Synechocystis und E. coli. 
    + Reihen 1-6 wurden für E. coli pSHDY\*topA genutzt, 
    + Reihen 7-12 für pSHDY\*topA aus Synechocystis. 
    + Zeilen A-G sind technische Replikate, Zeile H blanks. Induktor war aTc im Bereich von 0 - 10 ug/mL , Antibiotika Kan und Spec. Die OD wurde auf 0,05 zu Beginn des Experimentes eingestellt.

## 20180418
* 20180417: für morgiges Plattenleserexperiment werden 50 mL üN Kulturen der Konstrukte pSHDY\*gyrA, pSHDY\*gyrB, pSHDY\*topA alle aus E. coli, und pSHDY\*topA aus Synechochcystis gepräpt in DH5-alpha in LB-Medium und Kan+Spec angezogen. Angeimpft wurde aus kühl gestellten vorherigen üN Kulturen aud OD 0,1.
* 20180418: Plattenleserexperiment mit LB-Medium durchgeführt. Die OD wurde für alle Proben auf 0,05 eingestellt.
    + Reihen 1 –6 : E. coli pSHDY\*topA, Induktorkonzentrationen 0; 0,8; 1,6; 2,4; 3,2; 4 ug/mL aTc, jeweils in Triplikaten von Zeile A-C, Zeile D blank
    + Reihen 7 –12 : E. coli pSHDY\*topA aus Synechocystis 6803, Induktorkonzentrationen   0; 0,8; 1,6; 2,4; 3,2; 4 ug/mL aTc, jeweils in Triplikaten von Zeile A-C, Zeile D blank
    + Reihen 1 –6 : E. coli pSHDY\*gyrA, Induktorkonzentrationen 0; 200; 400; 600; 800; 1000 uM IPTG, jeweils in Triplikaten von Zeile E-G, Zeile H blank
    + Reihen 7 –12 : E. coli pSHDY\*gyrB, Induktorkonzentrationen 0; 200; 400; 600; 800; 1000 uM IPTG, jeweils in Triplikaten von Zeile E-G, Zeile H blank

## 20180420
* 20180419: üN Kulturen aller Konstrukte (s. 15.4.2018) wurden in M9-Medium plus Kan und Spec angesetzt. Aus einer LB- üN-Kultur wurde 1:50 in M9 angeimpft. 
    + Beim Ansetzten des M9 Mediums ist darauf zu achten, dass das Calciumchlorid und Magnesiumsulfat vorher schon in wässriger Lösung vorliegen und nicht zusammen zugegeben werden! Sonst fällt Calciumsulfat aus. Am besten das Magnesiumsulfat zuletzt zugeben, wenn die übrigen Salze schon in Wasser gelöst wurden.
* 20180420: Plattenleserexperiment in M9 Medium:
    + Für die topA Konstrukte wurden die aTc Konzentrationen 0; 0,8; 1,6; 2,4; 3,2; 4 ug/mL eingestellt. 
    + Für die gyrase Konstrukte wurde IPTG in den Konzentrationen 0; 500; 1000; 1500; 2000; 2500 uM eingestellt. 
    + In den Zeilen A-C wurden technische Replikate von E. coli pSHDY\*topA (1-6) und pSHDY\*topA 6803 (7-12) aufgetragen; Zeile D blanks (mit Antibiotika, Induktor, Medium) 
    + In den Zeilen E-G wurden technische Replikate von E. coli pSHDY\*gyrA (1-6) und pSHDY\*gyrB (7-12) aufgetragen; Zeile H blanks (mit Antibiotika, Induktor, Medium)

## 20180427
* 20180424: Neue üN Kulturen der Konstrukte pSHDY\*topA, gyrA und gyrB aus E. coli. **NOTE: three days from overnight culture to experiment??**
* 20180427: Plattenleser Experiment mit pSHDY\*gyrB, hohe IPTG Konzentrationen und aTC gering
    + Reihe 1-6 Zeile A -C : aTc 0, 0,8, 1,6, 2,4, 3,2, 4 ug/mL E. coli pSHDY\*topA in LB-Medium ,Reihe 7-12 Zeile A-C aTc 0-4 ug/mL E. coli pSHDY-topA in M9 medium, Zeile D jeweilige blanks; Reihe 1-6 Zeile E-G : IPTG 0-2500 uM (500,1000,1500,2000,2500) E. coli pSHDY\*gyrB in LB-Medium , Reihe 7-12 Zeile E-G: IPTG 0-2500 uM E. coli pSHDY\*gyrB in M9 medium, Zeile H jeweilige blanks;

## 20180502
* 20180501: Neue üN aller Konstrukte angesetzt, gyrA und B von Platte , beide topA 1:100 aus flüssiger Kultur
* 20180502: Plattenleserexperiment 
    + Reihe 1-6 Zeile A -C : aTc 0, 0,3, 0,6, 0,9, 1,2, 1,5 ug/mL E. coli pSHDY\*topA in LB-Medium 
    + Reihe 7-12 Zeile A-C aTc 0, 0,3, 0,6, 0,9, 1,2, 1,5 ug/mL E. coli pSHDY\*topA aus 6803 in LB- medium, 
    + Zeile D 1&7: 0% EtOH in LB, 2&8 20% EtOH 3&9 40% EtOH 4&10 60% EtOH 5&11 80% EtOH 6&12 98,65% EtOH; 
    + Reihe1-6 Zeile E-G : IPTG 0-5500 uM E. coli pSHDY\*gyrA in LB-Medium ,
    + Reihe 7-12 Zeile E-G: IPTG 0-5500 uM E. coli pSHDY\*gyrB in LB medium, 
    + Zeile H 1&7 0% EtOH in LB, 2&8 20% EtOH 3&9 40% EtOH 4&10 60% EtOH 5&11 80% EtOH 6&12 98,65% EtOH; 
    + ACHTUNG : Pause bei cycle 147 für Zwischenergebnis !!! (0,2000 uM IPTG,2500 uM IPTG,3500 uM IPTG,4500 uM IPTG ,5500 uM IPTG)


# References