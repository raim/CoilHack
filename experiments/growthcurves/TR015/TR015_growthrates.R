
### FIT GROWTHRATES

library(platexpress)
#source("~/programs/platexpress/R/platereader.R")

#DATPATH <- "C:/Users/Tom/Dropbox/HHu Düsseldorf/Masterarbeit/Plate reader/TR015"
DATPATH <- "/home/raim/work/CoilHack/experiments/growthcurves/TR015"
RESPATH <- sub("experiments","results", DATPATH)
dir.create(RESPATH, recursive=TRUE)

load(file.path(RESPATH,"TR015.RData"))


## STRATEGIES
## a) growthrates:easylinear after Hall et al." Growth rates made easy.
## b) iron-out growth phases by using grofit complex model fits,
##    and use growthrates:easylinear on this
## c) fit linear parts separately, manually or with packages:
##    segmented, dpseg or BayesianPGMM


## 1) package grofit
library(grofit)

tmp <- viewGroups(dat, groups=grp[1], groups2=grp2, yids=c("OD"), group2.col=grp2.col, g2.legend=FALSE, g1.legend=FALSE, embed=TRUE, log="y",show.ci95=F,ylim=c(0.01,1.2))#,ylims=list(OD=c(.01,3)))

gfit <- grofit_plate(data=cutData(dat,xrng=c(0,35)), plate=map, yid="OD", amount="amount",fields=c("strain","medium","glc"),plot=FALSE)

gdat <- addModel(gfit, data=dat, ID=paste0("OD_grofit"))

viewPlate(gdat, yids=c("OD","OD_grofit"))
tmp <- viewGroups(gdat, groups=grp, groups2=grp2, yids=c("OD","OD_grofit"), group2.col=grp2.col, g2.legend=FALSE, g1.legend=FALSE, embed=TRUE, log="y",show.ci95=F,ylim=c(0.05,1.2))#,ylims=list(OD=c(.01,3)))

## dose-response : grofit parameter
results <- merge(map, grofitResults(gfit), by="well")

par(mfcol=c(2,2), mai=c(.5,.5,.05,.05), mgp=c(1.2,.3,0), tcl=-.25)
for ( gr in names(grp) ) {
    doseResponse(results, wells=grp[[gr]], bartype="sd", 
                 val="mu", amount="amount", col="color",
                 xlab="aTc, ng/mL",
                 ylim=c(0,.65), ylab=expression("growth rate "~mu~", h"^-1))
    legend("topright",legend=gr)
}
par(mfcol=c(2,2), mai=c(.5,.5,.05,.05), mgp=c(1.2,.3,0), tcl=-.25)
for ( gr in names(grp) ) {
    doseResponse(results, wells=grp[[gr]],bartype="sd",
                 val="A", amount="amount", col="color",
                 xlab="aTc, ng/mL",
                 ylim=c(0,1.5), ylab=expression("yield: final OD"))
    legend("topright",legend=gr)
}
par(mfcol=c(2,2), mai=c(.5,.5,.05,.05), mgp=c(1.2,.3,0), tcl=-.25)
for ( gr in names(grp) ) {
    doseResponse(results, wells=grp[[gr]],bartype="sd",
                 val="Y", amount="amount", col="color",
                 xlab="aTc, ng/mL",
                 ylim=c(0,1.5), ylab=expression("yield: final OD"))
    legend("topright",legend=gr)
}

## 2) package growthrates
library(growthrates)

## calculate growth rates for original data or for grofit model fits?
fitto <- "OD" # "OD_grofit" #
if ( fitto=="OD_grofit" ) dat <- gdat


## EASY LINEAR FIT  4-12 h
dfg <- data2growthrates(cutData(dat,xrng=c(3,12)), fitto, plate=map)
fits1 <- all_easylinear(value ~ time | well, data=dfg, h=20)

mdat <- addModel(fits1, dat, ID="easylinear")
viewPlate(mdat, ylims=list(easylinear=c(0,2.5)), xlim=c(0,17))

## EASY LINEAR FIT  10-20 h
dfg <- data2growthrates(cutData(dat,xrng=c(10,20)), fitto, plate=map)
fits2 <- all_easylinear(value ~ time | well, data=dfg, h=20)
mdat <- addModel(fits2, mdat, ID="easylinear2")
viewPlate(mdat, ylims=list(easylinear=c(0,2.5)), xlim=c(0,17))



plotdev(file.path(RESPATH,"topA_easylinear"), type="png", height=4,width=12)
tmp <- viewGroups(mdat, groups=grp, groups2=grp2, yids=c(fitto,"easylinear","easylinear2"), group2.col=grp2.col, g2.legend=FALSE, g1.legend=TRUE, g1.legpos="bottomright", log="", lwd.orig=0,show.ci95=F,ylim=c(0.05,1.5),xlim=c(0,35))
dev.off()
plotdev(file.path(RESPATH,"topA_easylinear_log"), type="png", height=4,width=12)
tmp <- viewGroups(mdat, groups=grp, groups2=grp2, yids=c(fitto,"easylinear","easylinear2"), group2.col=grp2.col, g2.legend=FALSE, g1.legend=TRUE, g1.legpos="bottomright",log="y", lwd.orig=0,show.ci95=F,ylim=c(0.05,1.5),xlim=c(0,35))
dev.off()

## dose response : growthrates parameters
grres <- growthratesResults(fits1)
## add to well map
results <- merge(map, grres, by="well")


plotdev(file.path(RESPATH,"topA_easylinear_doseresponse"),
        type="png", height=10,width=10)
par(mfcol=c(2,2), mai=c(.5,.5,.05,.05), mgp=c(1.2,.3,0), tcl=-.25)
for ( gr in names(grp) ) {
    doseResponse(results, wells=grp[[gr]], bartype="sd", 
                 val="mu", amount="amount", col="color",
                 xlab="aTc, ng/mL",
                 #ylim=c(0,.65),
                 ylab=expression("growth rate "~mu~", h"^-1))
    legend("topright",legend=gr)
}
dev.off()
par(mfcol=c(2,2), mai=c(.5,.5,.05,.05), mgp=c(1.2,.3,0), tcl=-.25)
for ( gr in names(grp) ) {
    doseResponse(results, wells=grp[[gr]],bartype="sd",
                 val="lambda", amount="amount", col="color",
                 xlab="aTc, ng/mL",
                 ylim=c(0,10), ylab=expression("lag phase , h"))
    legend("topright",legend=gr)
}
par(mfcol=c(2,2), mai=c(.5,.5,.05,.05), mgp=c(1.2,.3,0), tcl=-.25)
for ( gr in names(grp) ) {
    doseResponse(results, wells=grp[[gr]],bartype="range",
                 val="r2", amount="amount", col="color",
                 xlab="aTc, ng/mL")#,
                 #ylim=c(3.5,6), ylab=expression("initial OD"))
    legend("topright",legend=gr)
}

## 3) MONOD MODEL

library(growthmodels)

dfg <- data2growthrates(dat, fitto, plate=map)

## initial conditions and parameters
yinit <- c(s=2, y=.05)
poinit <- c(phi=0, sin=2, mumax=2, K=1, Y=0.5)
fixed <- c("sin", "phi", "K")#, "Y")
fitted <- names(poinit)[!names(poinit)%in%fixed]

## simulate ode from starting parameters
odem <- ode(yinit, dfg$time[dfg$well=="A1"], ode_monod, poinit)
plot(dfg$time[dfg$well=="A1"], dfg$value[dfg$well=="A1"])
lines(odem[,"time"], odem[,"y"],col=2)
plot(odem[,"time"], odem[,"y"],col=2)


## lower/upper boundaries for parameters
parms <- c(yinit, poinit)
lowp <- uppp <- parms
lowp[] <- 0
uppp[] <- 10; #uppp["Y"] <- .99
uppp["mumax"] <- 10

fits7 <- all_growthmodels(value ~ time | well, data = dfg,
                          FUN = grow_monod,
                          p = parms, lower = lowp, upper = uppp,
                          which = fitted, ncores=5) #, ncores = 1)
apply(coef(fits7),2,sd)

## TODO: check results in fits7@fits[[i]]@fit$message and
## fits7@fits[[i]]@fit$convergence
## "Relative error between `par' and the solution is at most `ptol'."

mdat <- addModel(fits7, mdat, ID="ODEMonod", replace=TRUE, col="#0000AA")
plotdev(file.path(RESPATH,"topA_monod_all"), type="png", height=8,width=12)
viewPlate(mdat, ylim=c(0,1.5), yids=c("OD","ODEMonod"),axes=c(2,4))
dev.off()

plotdev(file.path(RESPATH,"topA_monod"), type="png", height=10,width=10)
par(mfrow=c(2,2), mai=c(.2,.2,.05,.05), mgp=c(1.2,.25,0))
for ( i in 1:4 )
    viewGroups(mdat, groups=grp[i], groups2=grp2, group2.col=grp2.col,
               yids=c("OD","ODEMonod"), g1.legend=i==1, g2.legend=TRUE,
           g2.legpos="bottomright",
           show.ci95=FALSE, lwd.orig=0, log="", ylim=c(.02,1.5),
           embed=TRUE, no.par=FALSE)
dev.off()

## dose response : Monod fit parameters
grres <- growthratesResults(fits7) 
results <- merge(map, grres, by="well")


plotdev(file.path(RESPATH,"topA_monod_doseresponse"),
        type="png", height=10,width=10)
par(mfcol=c(2,2), mai=c(.5,.5,.05,.05), mgp=c(1.2,.3,0), tcl=-.25)
for ( gr in names(grp) ) {
    doseResponse(results, wells=grp[[gr]], bartype="sd", 
                 val="mu", amount="amount", col="color",
                 xlab="aTc, ng/mL",
                 #ylim=c(0,.65),
                 ylab=expression("growth rate "~mu~", h"^-1))
    legend("topright",legend=gr)
}
dev.off()
par(mfcol=c(2,2), mai=c(.5,.5,.05,.05), mgp=c(1.2,.3,0), tcl=-.25)
for ( gr in names(grp) ) {
    doseResponse(results, wells=grp[[gr]],bartype="sd",
                 val="lambda", amount="amount", col="color",
                 xlab="aTc, ng/mL",
                 ylim=c(0,10), ylab=expression("lag phase , h"))
    legend("topright",legend=gr)
}
par(mfcol=c(2,2), mai=c(.5,.5,.05,.05), mgp=c(1.2,.3,0), tcl=-.25)
for ( gr in names(grp) ) {
    doseResponse(results, wells=grp[[gr]],bartype="range",
                 val="r2", amount="amount", col="color",
                 xlab="aTc, ng/mL")#,
                 #ylim=c(3.5,6), ylab=expression("initial OD"))
    legend("topright",legend=gr)
}



### LINEAR PIECEWISE - USING INITIAL GUESS FOR BREAKPOINTS
## see https://www.r-bloggers.com/r-for-ecologists-putting-together-a-piecewise-regression/ for a direct approach scanning possible breakpoints
library(segmented)

## test
x <- dat$Time
y <- log(dat$OD$data[,"A3"])
x <- x[1:length(x)]
y <- y[1:length(y)]
plot(x,y)

out.lm <- lm(y~x)
## TEST: equally spaced vs. visually estimated breakpoints!
o <- segmented(out.lm,psi=seq(min(x),max(x),7)[2:6]) #c(9,21,24,26,30))

plotdev(file.path(RESPATH,"wellA3_segmented"), type="png", height=5,width=5)
plot(x,y, type="b",cex=.5)
plot.segmented(o,add=TRUE, col=2, lwd=3, shade=TRUE, rug=FALSE)
lines.segmented(o,col=1, lwd=1)
abline(v=confint(o)$x[,1])
dev.off()

## segment ALL : visually define breakpoints
breakpoints <- c(2,10,15,20) #seq(2,30,2) # 
brkp <- rep(list(breakpoints), length(map$well))
names(brkp) <- map$well

local.breaks <- FALSE
if ( local.breaks ) {
    wls <- grp[["snoopy"]] 
    brkp[wls] <- rep(list(c(10,20,24,30)), length(wls))
    wls <- grp2[["snoopy_0"]]
    brkp[wls] <- rep(list(c(2, 8, 15)), length(wls))
    wls <- grp2[["snoopy_20"]]
    brkp[wls] <- rep(list(c(10,15,18,21)), length(wls))
    
    wls <- c(grp[["EVC"]] ,grp[["plO3_mVenus"]])
    brkp[wls] <- rep(list(c( 5,10,15)), length(wls))

    wls <- grp[["plO3_topA"]] 
    brkp[wls] <- rep(list(c(2, 10,15,20)), length(wls))
    wls <- grp2[["plO3_topA_0"]]  # same as snoopy_0!
    brkp[wls] <- rep(list(c(2, 8, 15)), length(wls))
    wls <- grp[["plO3_topA_20"]] 
    brkp[wls] <- rep(list(c(10,15,20)), length(wls))
}
## go through wells

sgrp <- c(grp2[grep("snoopy",names(grp2))],
          grp2[grep("topA",names(grp2))],
          grp2[grep("mVenus",names(grp2))],
          grp2[grep("EVC",names(grp2))])
wells <- unlist(sgrp)

## use platexpress interface to segmented package
## TEST: equally spaced vs. visually estimated breakpoints!
segments_sgm <- segmented_plate(data=dat, yid="OD", wells=wells,
                                log=TRUE, man=5,
                                maxtry=5, psi=breakpoints, verb=0,plot=FALSE)
##segments_sgm <- segmented_plate(data=dat, yid="OD", wells=wells,
##                                log=TRUE, man=5,
##                               maxtry=5, npsi=length(breakpoints),
##                                verb=0,plot=FALSE)

## plot wells
dir.create(file.path(RESPATH,"wells"))
for ( i in 1:length(grp2) ) {
    for ( well in unlist(grp2[[i]]) ) {

        x <- dat$Time
        y <- log(dat[["OD"]]$data[,well])

        o  <- segments_sgm[[well]] 
        
        plotdev(file.path(RESPATH,"wells",
                          paste0(names(grp2)[i],"_",well,"_segmented")),
                type="png", height=5,width=5)
        plot(x,y,type="b",cex=.5,main=paste(names(grp2)[i], well))
        plot.segmented(o,add=TRUE, col=2, lwd=3, shade=TRUE, rug=FALSE)
        lines.segmented(o,col=1, lwd=1)
        abline(v=confint(o)$x[,1])
        dev.off()
    }
}

## plot as heatmap with segment arrows
## reorder wells
sgrp <- c(grp2[grep("snoopy",names(grp2))],
          grp2[grep("topA",names(grp2))],
          grp2[grep("mVenus",names(grp2))],
          grp2[grep("EVC",names(grp2))])
wells <- unlist(sgrp)
## get named well colors vector
cols <- map$color
names(cols) <- map$well

## plot
plotdev(file.path(RESPATH, "topA_segmented_phases_heatmap"),
        type="png", width=6,height=8,res=300)
par(mai=c(.5,.5,.05,.05),mgp=c(1.2,.3,0),tcl=-.0,xaxs="i")
image_plate(dat,yid="OD", wells=wells, xlab="time, h",
            q.cut=.99, wcol=cols[wells],col=gray.colors(100,start=1,end=0))
arrows_plate(segments_sgm,wells, lwd.max=10, add=T)#, wcol=cols[wells])
axis(1,tcl=-.25)
dev.off()

## plot with viewGroups/viewPlate

## add to platexpress data to plot!
sgdat <- addModel(fit=segments_sgm, dat, 
                  ID="OD_sgm", col="#0000FF")
sgdat <- addModel(fit=segments_sgm, sgdat, 
                  ID="mu_sgm", col="#00FFFF", add.slopes=TRUE)

## all wells
plotdev(file.path(RESPATH,"topA_segmented_rates_all"), type="png",
        height=10,width=10)
viewPlate(sgdat, yids=c("OD","mu_sgm"),ylims=list(OD=c(0.05,2),mu_sgm=c(-.1,1)))
dev.off()

plotdev(file.path(RESPATH,"topA_segmented_all"), type="png", height=10,width=10)
viewPlate(sgdat, yids=c("OD","OD_sgm","mu_sgm"),ylims=list(OD=c(0.05,2),OD_sgm=c(0.05,2),mu_sgm=c(-.1,1)),)
dev.off()
plotdev(file.path(RESPATH,"topA_segmented_all_log"), type="png", height=10,width=10)
viewPlate(sgdat, yids=c("OD","OD_sgm"),ylims=list(OD=c(0.05,2),OD_sgm=c(0.05,2),mu_sgm=c(-.1,1)),log="y")
dev.off()


## group averages
plotdev(file.path(RESPATH,"topA_segmented_rates"), type="png", height=10,width=10)
par(mfrow=c(2,2), mai=c(.2,.2,.05,.05), mgp=c(1.2,.25,0))
for ( i in 1:4 )
    viewGroups(sgdat, grp[i], grp2, yids=c("mu_sgm"),group2.col=grp2.col,show.ci95=F,lwd.orig=.1,ylims=list(OD=c(0.05,2),mu_sgm=c(-.1,.7)),g2.legpos="topright",g1.legpos="right",g1.legend=FALSE,
               embed=TRUE, no.par=FALSE)
legend("right",legend=expression("growthrate, "~mu~", h"^-1),lty=1)
dev.off()

plotdev(file.path(RESPATH,"topA_segmented_log"), type="png", height=5,width=8)
viewGroups(sgdat, grp, grp2, yids=c("OD_sgm"),group2.col=grp2.col,show.ci95=T,lwd.orig=.1,log="y",ylim=c(0.05,2))
dev.off()
plotdev(file.path(RESPATH,"topA_segmented"), type="png", height=5,width=8)
viewGroups(sgdat, grp, grp2, yids=c("OD_sgm"),group2.col=grp2.col,show.ci95=T,lwd.orig=.1,ylim=c(0.05,2))
dev.off()


### DYN.PROG. SEGMENTATION ?
## from new package  ~/programs/growthphases/
## TODO: create a predict function, that uses fastLm fits for final
## segments in pieces
## TODO: integrate results from dpseg into platexpress, addModel, etc
library(dpseg)


## run dpseg segmentation
dpsg <- dpseg_plate(data=dat, yid="OD", wells=wells,
                    minl=5, P=1e-4, verb=1, log=TRUE)

## plot all wells
dir.create(file.path(RESPATH,"wells"))
for ( i in 1:length(grp2) ) {
    for ( well in unlist(grp2[[i]]) ) {

        x <- dat$Time
        y <- log(dat[["OD"]]$data[,well])

        o  <- dpsg[[well]] 
        
        plotdev(file.path(RESPATH,"wells",
                          paste0(names(grp2)[i],"_",well,"_dpseg")),
                type="png", height=5,width=5)
        plot(o,type="b",cex=.5,main=paste(names(grp2)[i], well))
        dev.off()
    }
}

## plot as heatmap
## reorder wells
sgrp <- c(grp2[grep("snoopy",names(grp2))],
          grp2[grep("topA",names(grp2))],
          grp2[grep("mVenus",names(grp2))],
          grp2[grep("EVC",names(grp2))])
wells <- unlist(sgrp)

plotdev(file.path(RESPATH, "topA_dpseg_phases_heatmap"),
        type="png", width=6,height=8,res=300)
par(mai=c(.5,.5,.05,.05),mgp=c(1.2,.3,0),tcl=-.0,xaxs="i")
image_plate(dat,yid="OD", wells=wells, xlab="time, h",
            q.cut=.99, wcol=cols[wells],col=gray.colors(100,start=1,end=0))
arrows_plate(dpsg, wells, lwd.max=8, maxmu=0.6)#, wcol=cols[wells]), xlim=range(dat$Time))
axis(1,tcl=-.25)
dev.off()


## currently just adds mu and piecewise predict to data
dpdat <- addModel(dpsg, dat, ID="OD_dpsg")
dpdat <- addModel(dpsg, dpdat, ID="mu_dpsg", add.slopes=TRUE)

## all wells
plotdev(file.path(RESPATH,"topA_dpseg_rates_all"), type="png",
        height=10,width=10)
viewPlate(dpdat, yids=c("OD","mu_dpsg"),ylims=list(OD=c(0.05,2),mu_dpsg=c(-.1,1)))
dev.off()

plotdev(file.path(RESPATH,"topA_dpseg_all"), type="png", height=10,width=10)
viewPlate(dpdat, yids=c("OD","OD_dpsg","mu_dpsg"),ylims=list(OD=c(0.05,2),OD_dpsg=c(0.05,2),mu_dpsg=c(-.1,1)),)
dev.off()
plotdev(file.path(RESPATH,"topA_dpseg_all_log"), type="png", height=10,width=10)
viewPlate(dpdat, yids=c("OD","OD_dpsg"),ylims=list(OD=c(0.05,2),OD_dpsg=c(0.05,2),mu_dpsg=c(-.1,1)),log="y")
dev.off()


## group averages
plotdev(file.path(RESPATH,"topA_dpseg_rates"), type="png", height=10,width=10)
par(mfrow=c(2,2), mai=c(.2,.2,.05,.05), mgp=c(1.2,.25,0))
for ( i in 1:4 ) {
    viewGroups(dpdat, grp[i], grp2, yids=c("mu_dpsg"),group2.col=grp2.col,show.ci95=F,lwd.orig=.1,ylims=list(mu_dpsg=c(-.05,.6),mu_dpsg=c(-.1,.7)),g2.legpos="topright",g1.legpos="right",g1.legend=FALSE, embed=TRUE, no.par=FALSE)
    abline(h=0,col=1,lty=2)
}
legend("right",legend=expression("growthrate, "~mu~", h"^-1),lty=1)
dev.off()
plotdev(file.path(RESPATH,"topA_dpseg_rates_ci95"), type="png", height=10,width=10)
par(mfrow=c(2,2), mai=c(.2,.2,.05,.05), mgp=c(1.2,.25,0))
for ( i in 1:4 ) {
    viewGroups(dpdat, grp[i], grp2, yids=c("mu_dpsg"),group2.col=grp2.col,show.ci95=T,lwd.orig=.1,ylims=list(mu_dpsg=c(-.05,.6),mu_dpsg=c(-.1,.7)),g2.legpos="topright",g1.legpos="right",g1.legend=FALSE, embed=TRUE, no.par=FALSE)
    abline(h=0,col=1,lty=2)
}
legend("right",legend=expression("growthrate, "~mu~", h"^-1),lty=1)
dev.off()

plotdev(file.path(RESPATH,"topA_dpseg_log"), type="png", height=5,width=8)
viewGroups(dpdat, grp, grp2, yids=c("OD_dpsg"),group2.col=grp2.col,show.ci95=T,lwd.orig=.1,log="y",ylim=c(0.05,2),g2.legpos="bottomright")
dev.off()
plotdev(file.path(RESPATH,"topA_dpseg"), type="png", height=5,width=8)
viewGroups(dpdat, grp, grp2, yids=c("OD_dpsg"),group2.col=grp2.col,show.ci95=T,lwd.orig=.1,ylim=c(0.05,2))
dev.off()

## TODO: save relevant data!

## save complete R session data
save.image(file.path(RESPATH,"TR015_growthrates.RData"))


## TODO: piecewise linear fits with BayesPGMM
if (!interactive() ) quit(save="no")

library(BayesianPGMM) # or
x <- dat$Time
Y <- NULL
for ( let in toupper(letters[1:4])) {
    y <- ma(log(dat$OD$data[,paste0(let,"3")]),10)
    y <- y[15:(length(y)-15)]
    Y <- rbind(Y,y)
}
    x <- x[15:(length(x)-15)]
X <- matrix(rep(x-x[1],each=4),nrow=4)

plotPGMM(X,Y) ##Plot the data
Results <- BayesPGMM(X,Y,n_clust=4) ##Fit PGMM (can take about 5 minutes)

plotdev(file.path(RESPATH,"bayesPGMM"), type="png", height=5,width=5)
plotPGMM(X,Y,Results) ##Plot results
dev.off()

