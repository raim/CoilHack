---
title: "Determination of Growth Rates from BioLector Data with dpseg"
author: "Rainer Machne"
date: "August 9, 2019"
output: html_document
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
```

## Results
### Determination of Growth Rates

Next, we attempt to determine culture growth rates for experiments
3 and 4. Since the "scatter" measurement showed likely artefactual peaks
towards the end of growth phases, we used the "fluorescence" data
for this analysis. 

The data was first cut between 3 h into the experiment and until 
individual wells were harvested for biomass determinations, to exclude
Temperature-related artefacts and noise from low level  
measurements points.
The fluorescence data was then smoothed with a moving average over 5 data
points, and the in-house algorithm `dpseg` 
(https://gitlab.com/raim/growthphases) was used via the `platexpress`
interface function `dpseg_plate` and parameters $minl=5$ and $P=0.001$ to 
split each growth curve into log-linear segments (Fig. x). For each growth
curve only the maximal segment slope was taken as the maximal growth rate
and a second maximum afterwards was taken as the growth rate of a second
growth phase (Fig. y). 

The maximal growth rates on glucose were between 0.4 $h^{-1}$ and 0.5
$h^{-1}$, and signficantly slower on acetate medium, between 0.05 $h^{-1}$
and 0.25 $h^{-1}$. In both experiments, growth rates first increased then
decreased with increasing medium concentrations.
In both experiments, also a second linear growth phase was detected, 
at ca. 0.05 $h^{-1}$ to 1 $h^{-1}$. These second growth phases were rater short and variable on acetate medium, and longer and more consistent
on glucose medium.

Results for "scatter" data are similar (Appendix XX), but the second
growth phase on glucose medium is not captured as well, due to
the artefactual scatter peaks.

```{r, echo=FALSE, warning=FALSE, results='hide'}
library(platexpress)
sergej.path <- "~/work/CoilHack/experiments/growthcurves/201906_sergej"
RESPATH <- file.path(sergej.path,"Results")
SRCPATH <- sub("Results","Scripts",RESPATH)
ID <- "ace_0625"
dids <- c("ace_0625","glc_0619")
yids <- c("scatter","fluorescence") 
for ( ID in dids ) {
  load(file.path(RESPATH,paste0("dat",ID,".RData")))
  for ( USE in c("fluorescence") )
    source(file.path(SRCPATH,"dpseg.R"))
}
```

