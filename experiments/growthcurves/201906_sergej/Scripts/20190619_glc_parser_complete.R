#######################################################
## MAKE SURE YOU INSTALLED THE LATEST PLATEXPRESS!!! ##
#######################################################

PLOT2PDF <- FALSE # re-direct all plots to PDF

library(platexpress)
#source("~/programs/platexpress/R/plot.R")
### LOADING ALL EXPERIMENT RESULT FILES FROM Biolectur

sergej.path <- "~/work/CoilHack/experiments/growthcurves/201906_sergej"
DATPATH <- file.path(sergej.path,"Data")
RESPATH <- file.path(sergej.path,"Results")


## plot all to PDF
if ( PLOT2PDF )
  pdf(file.path(RESPATH, "ecoli_glc_20190619.pdf"), width=12, height=8)

ALL <- list() ## store all results

### BioLector Pro E.coli experiment with either glc or ace growth


file <- "15_Ecoli_acetate_REDUCTION-1.csv"
files <- file.path(DATPATH, file)
mapfile <- file.path(DATPATH, "20190619_glc_platelayout.xlsx")

dat <- readExperiment(files, type="BioLectorPro",time.conversion=1/3600,
                      layout=mapfile, 
                      fields=c("strain","medium","glc","aco"), 
                      afields=c("glc","aco"),
                      #skip.wells=c("F2","F3","F4"),
                      blank.id="blank", blank.data=c("Biomass","Riboflavine"),
                      group1="medium", group2 = c("medium","amount"),
                      group2.color = "color")

## parse raw data
dat <- prettyData(dat, yids=c(fluorescence="Riboflavine",O2="DO(PSt3)", scatter="Biomass", pH="pH(HP8)", NADH="NADH - NADPH"))

## inspect the data
viewPlate(dat)

viewGroups(dat, yids="scatter",
           show.ci95=TRUE, lwd.orig=0, g2.legpos = "topleft")

viewGroups(dat, yids="fluorescence", show.ci95=TRUE, lwd.orig=0, g2.legpos = "topleft")

viewGroups(dat, yids="O2", ylim = c(0,120), show.ci95=TRUE, lwd.orig=0, g2.legpos = "bottomleft")

viewGroups(dat, yids="pH", show.ci95=TRUE, lwd.orig=0, g2.legpos = "bottomright")

viewGroups(dat, yids = "NADH", show.ci95=TRUE, lwd.orig=0, g2.legpos = "bottomright")

datglc_0619 = dat
datglc = dat



### Calibrations

library(pspline)
library(platexpress)

viewGroups(datglc, yids = "O2")
abline(v=5)
abline(h=100)
##############################################################################################
### Seperate smoothing every replicate, data normed with the highest value in the first 5h ###

smvalues = apply(datglc$O2$data/max(datglc$O2$data[datglc$Time<5,])*100, 2, function(x){smooth.spline(datglc$Time, x)$y})
rownames(smvalues) <- rownames(datglc$O2$data[,0])
smvalues
datglc = addData(datglc, ID="smvalues", dat = smvalues, replace = TRUE)
datglc$smvalues$data


### Calculation oxygen [%] into oxygen [mmol/L], at culture temperature 37°C
smliquidO2 = datglc$smvalues$data/100*224.2/1000
smliquidO2
datglc = addData(datglc, ID="smliquidO2", dat = smliquidO2, replace = TRUE)
viewGroups(datglc, yids = "smliquidO2")

### 

dC_dt = apply(datglc$O2$data, 2,
              function(x){
                tmp = smooth.spline(datglc$Time, x)
                predict(tmp, datglc$Time, deriv=1)$y
              }
)

dC_dt
datglc = addData(data = datglc, ID="dC_dt", dat = dC_dt, replace =TRUE)
datglc$dC_dt$data
viewGroups(datglc, yids = c("dC_dt"))

### scatter plots Dry Cell Weight / Liter
dcw_glc = data.frame("conc.glc" = c("0.1%", "0.2%", "0.3%", "0.4%", "0.5%", "0.6%", "0.7%", "0.8%"), "eppi.weigth[mg" = c(3147.88, 3157.61, 3153.17, 3147.22, 3157.71, 3149.15, 3154.45, 3147.14), "eppi+DC[mg" = c(3149.35, 3161.58, 3159.73, 3156.40, 3169.32, 3163.49, 3170.53, 3165.81))
dcw_glc


### Calculation difference
dcw_glc$gdiff <- dcw_glc$eppi.DC.mg-dcw_glc$eppi.weigth.mg

### 4mL eppi volume
dcw_glc$gL <- dcw_glc$gdiff/4

### calculation g/L dcw_glc -> c-mol/L
### 0.48gc/gdcw_glc = carbon constant, 12.0107 g/mol = molecular weigth C
dcw_glc$biomasscarbon <- dcw_glc$gL*0.48/12.0107

### 2,4,6,8 = glucose conc. in medium, 6 = C in glucose, 180.16 g/mol = molecular weigth of glucose
dcw_glc$mediumcarbon <- c(1*6/180.16, 2*6/180.16, 3*6/180.16, 4*6/180.16, 5*6/180.16, 6*6/180.16, 7*6/180.16, 8*6/180.16)

### yield calculation
dcw_glc$yield <- dcw_glc$biomasscarbon/dcw_glc$mediumcarbon

dcw_glc_0619 = dcw_glc
### cell number at statphase
dcw_glc_0619$cell_number = c(NA, NA, 2111111, 1855556, 3550000, 5500000, 3350000, 3166667)

dcw_glc_0619

### Creating plot -> biomass against scatter (max scatter values)
viewGroups(datglc, yids = "scatter")
abline(v=c(13:14))
abline(v=c(seq(8,12,0.5)))

scatter01 = getData(data = datglc, ID = "scatter", xrng = c(8,9))[, c(seq(1, ncol(datglc$scatter$data[, 1:40]),8))]
scatter02 = getData(data = datglc, ID = "scatter", xrng = c(8,9))[, c(seq(2, ncol(datglc$scatter$data[, 1:40]),8))]
scatter03 = getData(data = datglc, ID = "scatter", xrng = c(8,9))[, c(seq(3, ncol(datglc$scatter$data[, 1:40]),8))]
scatter04 = getData(data = datglc, ID = "scatter", xrng = c(9,10))[, c(seq(4, ncol(datglc$scatter$data[, 1:40]),8))]
scatter05 = getData(data = datglc, ID = "scatter", xrng = c(11.5:12.5))[, c(seq(5, ncol(datglc$scatter$data[, 1:40]),8))]
scatter06 = getData(data = datglc, ID = "scatter", xrng = c(12.5:13.5))[, c(seq(6, ncol(datglc$scatter$data[, 1:40]),8))]
scatter07 = getData(data = datglc, ID = "scatter", xrng = c(12.5:13.5))[, c(seq(7, ncol(datglc$scatter$data[, 1:40]),8))]
scatter08 = getData(data = datglc, ID = "scatter", xrng = c(13,14))[, c(seq(8, ncol(datglc$scatter$data[, 1:40]),8))]
scatter08


glcscatterdata_0619 = cbind(scatter01, scatter02, scatter03, scatter04, scatter05, scatter06, scatter07, scatter08)
glcscatterdata_0619

glcscatter_0619 = sort(unlist(lapply(lapply(datglc$groups$group2, function(x) apply(glcscatterdata_0619[,x],2,mean)), mean)))
glcscatter_0619
plot(glcscatter_0619, dcw_glc_0619$gL, pch = 19, xlim = c(0,4.5), ylim = c(0,5))

linearregression_glc_0619 = lm(dcw_glc_0619$gL ~ 0+glcscatter_0619)
linearregression_glc_0619
plot(glcscatter_0619, dcw_glc_0619$gL, pch = 19, xlim = c(0,4.5), ylim = c(0,5))
abline(linearregression_glc_0619, col="blue")


### biomass
biomass_glc = datglc$scatter$data*coef(linearregression_glc_0619)
datglc = addData(datglc, ID = "biomass_glc", dat = biomass_glc, replace = TRUE)


### Creating plot -> biomass against fluorescence (max fluorescence values)

viewGroups(data = datglc, yids = "fluorescence")
abline(v=c(0:14))

fluorescence01 = getData(data = datglc, ID = "fluorescence", xrng = c(8,9))[, c(seq(1, ncol(datglc$fluorescence$data[, 1:40]),8))]
fluorescence02 = getData(data = datglc, ID = "fluorescence", xrng = c(8,9))[, c(seq(2, ncol(datglc$fluorescence$data[, 1:40]),8))]
fluorescence03 = getData(data = datglc, ID = "fluorescence", xrng = c(8,9))[, c(seq(3, ncol(datglc$fluorescence$data[, 1:40]),8))]
fluorescence04 = getData(data = datglc, ID = "fluorescence", xrng = c(9,10))[, c(seq(4, ncol(datglc$fluorescence$data[, 1:40]),8))]
fluorescence05 = getData(data = datglc, ID = "fluorescence", xrng = c(11.5:12.5))[, c(seq(5, ncol(datglc$fluorescence$data[, 1:40]),8))]
fluorescence06 = getData(data = datglc, ID = "fluorescence", xrng = c(12.5:13.5))[, c(seq(6, ncol(datglc$fluorescence$data[, 1:40]),8))]
fluorescence07 = getData(data = datglc, ID = "fluorescence", xrng = c(12.5:13.5))[, c(seq(7, ncol(datglc$fluorescence$data[, 1:40]),8))]
fluorescence08 = getData(data = datglc, ID = "fluorescence", xrng = c(13,14))[, c(seq(8, ncol(datglc$fluorescence$data[, 1:40]),8))]

flu_glc = cbind(fluorescence01,fluorescence02,fluorescence03,fluorescence04,fluorescence05,fluorescence06,fluorescence07,fluorescence08)
flu_glc

flu_glc = sort(unlist(lapply(lapply(datglc$groups$group2, function(x){apply(flu_glc[, x], 2, mean)}), mean)))
flu_glc

lm_flu_glc = lm(dcw_glc$gL ~ 0+flu_glc)
lm_flu_glc

plot(flu_glc, dcw_glc$gL, pch = 19, xlim = c(0,2.5), ylim = c(0,5))
abline(lm_flu_glc)

### biomass through fluorescence
biomass_flu_glc = datglc$fluorescence$data*coef(lm_flu_glc)
datglc = addData(datglc, ID = "biomass_flu_glc", dat = biomass_flu_glc, replace = TRUE)
viewGroups(datglc, yids = "biomass_flu_glc")

### Average oxygen consumption (qO2) / Dry Cell Weigth -> mmol/h/g

rcells_glc = (datglc$dC_dt$data-290*((100*224.2/100/1000)-datglc$smliquidO2$data))*0.000875
rcells_glc
datglc = addData(datglc, ID="rcells_glc", dat = rcells_glc, replace = TRUE)
viewGroups(datglc, yids = "rcells_glc")

qcells_glc = (datglc$rcells_glc$data/datglc$biomass_glc$data)/0.001
qcells_glc
datglc = addData(datglc, ID="qcells_glc", dat=qcells_glc, replace = TRUE)
viewGroups(datglc, yids = "qcells_glc")

### Average oxygen comsumpton (qO2) / Dry Cell Weight -> mmol/h/g with fluorescence data instead scatter

qcells_flu_glc = (datglc$rcells_glc$data/datglc$biomass_flu_glc$data)/0.001
qcells_flu_glc
datglc = addData(datglc, ID="qcells_flu_glc", dat=qcells_flu_glc, replace = TRUE)
viewGroups(datglc, yids = "qcells_flu_glc", xlim = c(0,15), ylim = c(-40, 50), lwd.orig = 0, show.ci95 = F)
abline(h=-20)

datglc_0619 = datglc


save(datglc_0619, file="datglc_0619.RData")
save(dcw_glc_0619, file="dcw_glc_0619.RData")


### calculating biomass/cellnumber
### the biomass is g/ml, cellnumber = #/mm³
### ml = mm³/1000 -> the volume where cells were counted  = 6e-07ml
### cell per ml -> e.g. 2111111/63-07 = 3.518e+12
### an e.coli cell has a weigth of ~1pg = ~10e-12

cellweigth = data.frame("biomass_g/ml" = c(dcw_glc_0619$gL), "cellnumber" = c(dcw_glc_0619$cell_number))

cellweigth$ecoliweigth <- cellweigth$biomass_g.ml/(cellweigth$cellnumber/6e-07)
cellweigth


### DPSEG GROWTHRATES
save(datglc_0619, file = file.path(RESPATH,"datglc_0619.RData"))

SRCPATH <- sub("Results","Scripts",RESPATH)
ID <- "glc_0619"

for ( USE in c("scatter","fluorescence") )
    source(file.path(SRCPATH,"dpseg.R"))

## USE FOR BETTER qCells

USE <- "biomass_flu_glc"
source(file.path(SRCPATH,"dpseg.R"))


viewGroups(data, yids=c(USE,"dpseg"), show.ci95=FALSE,
           g1.legend=FALSE, g2.legend=FALSE,
           lwd.orig=0, g2.legpos = "bottomright", embed=TRUE, no.par=TRUE)
viewGroups(data, yids=c("rcells_glc"), show.ci95=FALSE,
           g1.legend=FALSE, g2.legend=FALSE,
           lwd.orig=0, g2.legpos = "bottomright", embed=TRUE, no.par=TRUE)


## O2 per scatter
qcells <- getData(data, "rcells_glc")/getData(data, "dpseg")/0.001
data <- addData(data, dat=qcells,
                ID="O2n", replace=TRUE)
viewGroups(data, yids=c("O2n"), show.ci95=FALSE,
           g1.legend=FALSE, g2.legend=FALSE,
           ylim=c(-30,10),
           lwd.orig=0, g2.legpos = "bottomright", embed=TRUE, no.par=TRUE)
