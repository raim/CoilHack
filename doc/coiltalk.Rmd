# Coil-Hack: Construction of a Production Strain

Figure: cell with genes g and t - replace g and t ORF by Reporter,
add g and t with inducible promoter

# What are g and t ?

Figure: transcription, linear, with ribosomes
Figure: transcription helix, polymerase turns around -> R-loop?
Figure: polymerase fixed: overwinding ahead, underwinding behind -> twin-domain model
Figure: t relaxes behind, g supercoils ahead ATP! -> esp. at highly transcribed genes
Figure: g opens the DNA, esp. at GC rich

ATATATATATGCGCGCGCGCGC
easy!             uff!

-> g and t influence elongation rate !

# Menzel & Gellert: 

global view, supercoiling homeostasis g<->t

# The Copenhagen School of Microbiology

old school: RNA ~ growth rate 

new school: hundreds of genes correlate positively or negatively
-> growth genes vs. stress/stationary phase genes

# Metabolic Control Analysis: 

transcription has little control over supercoiling, 
ATP/ADP has 70-80% of control!

# First Results

inducing topA regulates growth rate; same medium

-> ATP/ADP? 
-> supress gyr

# Coil-Seq

Measure local helical tension


