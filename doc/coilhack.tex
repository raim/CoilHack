\documentclass[a4paper,11pt,english]{article}

\usepackage{texshade} % texshade for dna sequences
\usepackage{todonotes} % todo notes

\usepackage[authoryear]{natbib}

\usepackage{amsmath}
\usepackage{amssymb}

%\usepackage{chemmacros}
\usepackage[load=abbr]{siunitx}
%\DeclareSIUnit\Molar{\textsc{m}} % pg. 39 siunitx manual (2013/03/11)

\usepackage{hyperref}
\usepackage{textcomp} % for upright greek letters in units, mostly \mu
\usepackage{graphicx}
\usepackage{subfig}
\usepackage{tabularx} % for table with textwidth
\usepackage{xcolor}

%% TEXT MAKROS
% TEXT
\newcommand{\invivo}{\textit{in vivo}} % 
\newcommand{\invitro}{\textit{in vitro}} % 

% SPECIES
\newcommand{\ecoli}{\textit{Escherichia coli}} % 
\newcommand{\ecol}{\textit{E. coli}} % 
\newcommand{\selong}{\textit{Synechococcus elongatus} PCC~7942} % 
\newcommand{\slong}{PCC~7942} % 
\newcommand{\scystis}{\textit{Synechocystis} sp. PCC~6803} % 
\newcommand{\scyst}{PCC~6803} % 

% REPOS
\newcommand{\coilh}{\href{https://gitlab.com/raim/CoilHack}{\texttt{CoilHack}}}
\newcommand{\coils}{\href{https://gitlab.com/raim/CoilSeq}{\texttt{CoilSeq}}}
\newcommand{\gend}{\href{https://gitlab.com/raim/genomeData}{\texttt{genomeData}}}
\newcommand{\genb}{\href{https://gitlab.com/raim/genomeBrowser}{\texttt{genomeBrowser}}}
\newcommand{\pbr}{\href{https://github.com/raim/PBR}{\texttt{PBR}}}

%% LAYOUT & FONT
\usepackage[top=2.5cm, bottom=1.5cm, left=1.5cm, right=1.5cm]{geometry}

\parskip 1ex
\parindent 1ex

\usepackage{helvet}
\renewcommand{\familydefault}{\sfdefault}

\title{CoilHack - Project Plan}
\author{Rainer Machn\'e, Ilka Axmann}

\begin{document}
\maketitle
\tableofcontents


\clearpage
\section{State of the Art}
\subsection{Introduction}
In \ecoli{} (\ecol{}), the extent of negative DNA supercoiling is
controlled by both gene expression, via supercoiling-sensitive (SCS)
expression of the enzymes gyrase (subunits gyrA and gyrB) and
topoisomerase I (topA) \citep{Menzel1983, Qi1997}, and by the cellular
ATP/ADP content \citep{Snoep2002}. Expression and actitivy of GyrAB
and TopA are likely additionally regulated by various metabolic
inputs, e.g. the redox state of thioredoxin in \textit{Rhodobacter}
\citep{Li2004}, and by diverse modulators, e.g., moonlighting
metabolic proteins \citep{Ashiuchi2003, Sengupta2008b}.

GC-rich genes encoding for cell growth (ribosomal proteins, amino acid
synthesis) require strong negative DNA supercoiling for expression,
while AT-rich catabolic genes are expressed from more relaxed DNA
\citep{Peter2004, Blot2006, Vijayan2009, Lehmann2014}. Thus, dynamic
DNA supercoiling appears to act as a central hub or gate in global
regulatory mechanisms that are involved with general regulation of
growth rate. Supercoiling exerts a torsional strain on the DNA double
helix, and this torsional strain can be considered as a general
sensor of the metabolic status of the cell, differentially
affecting transcription of hundreds of genes, encoding for either cell
growth or a resting state.
% for transcriptional program catalyze growth or
%processes involved . 
These observations are likely related to the so-called `microbial
growth laws' of bacteria, where the ribosomal biomass fraction is
optimized to current nutrient status, and correlates linearly with
growth rate \citep{Maaloe1979, Binder1998} while expression of
nutrient transport and catabolic (ATP-generating) genes is
anti-correlated with ribosome-related growth gene expression
\citep{Scott2014, Weisse2015, Machne2017}.

In \selong{} (\slong{}), DNA supercoiling changes over the day-night
cycle, and mediates the output of the KaiABC circadian clock
\citep{Vijayan2009}. In \scystis{} (\scyst{}), SCS genes are expressed
differentially during day-night cycles \citep{Lehmann2014}. The GC
bias and the functional profiles of SCS genes are similar in all
species.  quick adaptation in (artificial) evolution
\cite{Higgins2016} \& mutzel\todo{}

\paragraph{Interaction with Conventional Regulation.}
In \slong{}, manipulation of the transcription factor RpaA, a
mediator of the KaiABC clock, had similar but stronger effects as
direct manipulation of DNA supercoiling. \cite{Markson2013} could not
obtain consistent results regarding the supercoiling status of RpaA
mutants in \slong{}, but suggest a general feedforward regulation,
where KaiC/RpaA modify supercoiling - eg. by expression of HU
protein - and then further act on the modified DNA structure.
Supercoiling-dependent binding of transcription factors is well known,
e.g. suggested for PrrA in \textit{Rhodobacter} \citep{Eraso2009}, and
elegantly shown on plasmid systems for LacI, including a stabilization
of supercoils within LacI-bound DNA loops \citep{Fulcrand2016}.

\paragraph{Mission Statement: Modulate Growth Rate to Unleash Metabolic Potential.}
Growth rate and gene expression shall be modulated, e.g. growth suppressed
during affluent conditions, to mobilize available metabolic energy for 
production of heterologous plasmids, proteins or whole metabolic pathways.

To achieve this we construct inducible expression systems of gyrAB,
topA, hup or other genes. Alternatively and in parallel, we want to
affect growth by direct inhibition of protein synthesis. For both
approaches, we also want to investigate the potential use of the
endogenous mobile elements in \scyst{}, inteins in GyrB and DnaE or
the self-splicing group I intron in the fMet-tRNA of \scyst{},
or the two predicted 5'UTR RNA aptamers (for adenosylcobalamin, and 
theophyllin).

We define a long-term goal, where endogenous copies of the affected systems
are to be replaced by fluorescence or luminescence-based reporters, 
to read out the current state of the endogenous control systems, and
fully control their actual expression via inducible gene expression
systems.

While this long-term goal is not achievable within forseeable time
frames, it helps to define a basic research program. We define
achievable mile-stones which each by itself should lead to key
progress in the field.

\subsubsection{CRISPRcoil}

Here, we aim to locally manipulate supercoiling by targeting gyrase and/or
topoisomerase activity to specific loci by a fusion of gyrAB or topA
to catalytically inactive dCas9 CRISPR protein.

Previously, dCas9 had been coupled to DNA methylase Dnmt3a and
demethylase Tet1 to target DNA methzlation in mammalian cells
\cite{Liu2016}. Fusion proteins of the \ecoli{} subunits gyrA and
gyrB had previously shown to functionally catalyze negative supercoiling,
in \ecoli{} \cite{Chen2015b} and budding yeast \cite{Trigueros2002b}.

Targeting supercoiling to upstream or downstream regions of operons
may allow to locally manipulate expression levels.

\clearpage
\subsection{Background}
\paragraph{Promoter and Expression Studies in \ecoli{}:}
\begin{itemize}
\item \cite{Qi1997}: multiple topA promoters are differentially
  activated through growth phases
\item \cite{Jensen1999} replace gyrA expression by IPTG-inducible plasmid
\item \cite{Snoep2002} replace topA expression by IPTG-inducible plasmid
\item \cite{Peter2004} and \cite{Blot2006} investigate SCS transcriptomes
\item \cite{Sheridan2001} test SCS promoters on plasmids
\item \cite{Zaslaver2009} \& \cite{Scott2010} test growth-rate responsive
  gene expression
\end{itemize}

\paragraph{Promoter and Expression Studies in \scyst{}:}
\begin{itemize}
\item \cite{Prakash2009} identify SCS transcripts by novobiocin
\item \cite{Lehmann2014} show significant overlap between SCS and
  diurnal transcripts
\item \cite{Mitschke2011} \& \cite{Kopf2014} report the
  transcriptional landsacpe under diverse conditions
\item \cite{Vijayan2009} analyze SCS circadian and \cite{Markson2013}
  compare this with RpaA-dependent transcription in \slong{}
\end{itemize}

\paragraph{Inteins and group I intron in \scyst{}:}
\begin{itemize}
\item \cite{Martin2001} describe inteins in the GyrB subunit, and a
  trans-splicing intein in DnaE;\\ \cite{Topilina2014} review the use
  of inteins, among them a gyrase-derived intein (!), for
  intein-mediated protein expression, e.g., transposase Tn5-based
  tagging methods for sequencing (`ATAC-seq`) use this intein
  \citep{Picelli2014}
\item \cite{Bonocora2001} review the self-splicing group I (gI) intron
  in the fMet-tRNA of \scyst{} and other cyanobacteria;
  \cite{Bernt2013} suggest how gI self-splicing may be redox-sensitive
\end{itemize}

\paragraph{CRISPRi in (Cyano)bacteria:}
\begin{itemize}
\item \cite{Hawkins2015} achieve 300-fold repression in \ecol{} and provide detailed protocols
\item \cite{Yao2016} achieve simultaneous repression of 4 enzymes by 50-95\% in \scyst{}
\item \cite{Ferry2017} construct inducible sgRNAs in mammalian cells, and suggest a strategy to obtain riboswitch-based sgRNA
\item \cite{Felletti2017} review ligand-dependent ribozymes
\end{itemize}

\clearpage
\section{Work Programme}
%\paragraph{General:}
%Select apt strains and establish growth conditions. Th
%\begin{enumerate}
%\item Select apt strains, for \scyst{} coordinate with T. Zavrel and J. Cervency (CzechGlobe, Drasov) for growth experiments
%\subsection{In vitro}
%\begin{enumerate}
%\end{enumerate}

\subsection{Wetlab}

\begin{enumerate}
\item Establish $\Delta L_k$\footnote{$\Delta L_k$: measure of
  supercoiling, relative to total linking number $L_k$ of relaxed DNA;
  $\Delta L_k<0$ for negative supercoiling} measurements by plasmids
  in \ecol{} and \scyst{},
\item Establish ATP/ADP measurements in \ecol{} and
  \scyst{}, see preliminary work in \coils{}
\item Obtain Northern blot probes, qPCR primers and antibodies, see Sect.~\ref{bioinf}
\item Monitor changes in $\Delta L_k$, mRNA (qPCR) and protein
  (antibody) levels at different growth rates and upon administration
  of Novobiocin and other gyrase inhibitors
\item Test promoters on reporter plasmids:
  \begin{itemize}
  \item \ecol{}: ilvPG, gyrA \citep{Sheridan2001}, leu-500
    \citep{Zechiedrich2000}, topA \citep{Qi1997} etc.; and see
    Sect.~\ref{bioinf}
  \item \scyst{}: gyrA/B, topA, hup, see Sect.~\ref{bioinf}
  \end{itemize}
\item Reconstruct the plasmids and strains from \citep{Jensen1999}
  (genomic k.o. of gyr plus IPTG-induced gyr expression from plasmid), and
  from \citep{Snoep2002} (same with topA)
  \begin{enumerate}
  \item Amplify and sequence segments from target strains
  \item Find apt restriction sites
  \item Find apt promoters and vectors, \& clone
  \end{enumerate}
\item Theopyllin-riboswitch induction \citep{Ohbayashi2016}
\item Establish dCas9-based repression of gyrA/B and topA genes
\item Replace genomic copies by fluorescence or luminescence reporters
\end{enumerate}

\subsection{Bioinformatics}
\label{bioinf}

\begin{enumerate}
\item Implement genomes and data (\ecol{}, \scyst{}) in \genb{}
\item Collect coding and upstream/downstream information
  for all GOI (Sect.~\ref{seq}) from literature and sequence databases
\item Promoter studies of GOI: analyze structure, starting from TSS
  data by \cite{Mitschke2011} - select promoters for \invivo{} \& \invitro{}
  studies
\item Align and roughly review known protein structures of gyr, top,
  hu genes from species of interests or full phylogeny
\item Obtain Northern blot probes, qPCR primers and antibodies, 
  ideally from regions with conserved protein sequence
\item Compare transcriptome data \ecol{}, \scyst{}, \slong{}, with
  focus on GOI (Sect.~\ref{seq})
  \begin{itemize}
  \item Diurnal and SCS genes from \scyst{}, see
    overlapping genes in \citep{Lehmann2014}; account TSS/operon data and for
    known regulons, eg. RpaB by Matthias Riediger at Cyano2017
  \item Overlap between SCS data sets 
    \citep{Peter2004,Blot2006} and growth rate response \citep{Scott2010}  
  \end{itemize}
\item Identify SCS promoters in \ecol{} and
  \scyst{} from above analyses
\end{enumerate}



\clearpage
\section{Resources} % Sequences - Genes Of Interest (GOI)}
\label{seq}
Data \& protocol collections: %\paragraph{Resources:}
\begin{itemize}
\item Genes of interest (GOI) are available in \coilh{}
  (folder \texttt{sequences}),
\item Plasmids and Illumina adapters are collected in \coils{}
  (folder \texttt{seq}),
\item Genome-wide data should be integrated in
  \gend{} \& \genb{},
\item Bioreactor set-up can be documented in
  \pbr{} 
\item Protocols should be collected online with Axmann lab standard
  (\url{protocols.io}?)
\end{itemize}

\subsection{\ecoli{}}
Reference Genome: \url{https://www.ncbi.nlm.nih.gov/nuccore/556503834}\\
\ecoli{} str. K-12 substr. MG1655, RefSeq:NC\_000913.3

Available Sequences:
\begin{itemize}
\item gyrA: gyrase subunit A; EcoGene:EG10423, RefSeq:NP\_416734.1, GenBank:AAC75291 
\item gyrB: gyrase subunit B; EcoGene:EG10424, RefSeq:YP\_026241.1, GenBank:AAT48201 
\item topA: topoisomerase I, omega subunit; EcoGene:EG11013, RefSeq:NP\_415790.1, GenBank:AAC74356 
\item hupA: HU protein alpha subunit; EcoGene:EG10466, RefSeq:NP\_418428.1, GenBank:AAC76974 
\item hupB: HU protein beta subunit; EcoGene:EG10467, RefSeq:NP\_414974.1, GenBank:AAC73543 
\item yacG \citep{Sengupta2008a}, pmbA, tldD, ...: gyrase modulators
\end{itemize}

\subsection{\scystis{}}

Co-Expression Cluster index:
\begin{itemize}
\item CL\_sc: supercoiling response \citep{Prakash2009}; 1-upregulated, 2-mixed, 3-down
\item CL\_diur: diurnal expression clusters \citep{Lehmann2014}; 8/3/9-morning; 6-photosystems; 4-nonosci/low-level
\end{itemize}

Available Sequences:
\begin{itemize}
\item gyrA: sll1941; gyrase subunit A; CL\_sc NA; CL\_diur 4
\item gyrA: slr0417; gyrase subunit A; CL\_sc 2; CL\_diur 3
\item gyrB: sll2005; gyrase subunit B; contains intein; CL\_sc 3; CL\_diur 8
\item topA: slr2058; DNA topoisomerase I; CL\_sc 1; CL\_diur 3
\item pmbA: sll0887; putative gyrase modulator; CL\_sc NA; CL\_diur 9
\item tldD: slr1322; putative gyrase modulator; CL\_sc NA; CL\_diur 9
\item hup: sll1712; DNA binding protein HU; CL\_sc 1; CL\_diur 6
\end{itemize}

\clearpage
\section{Staff}

\subsection{The Hammer}
PI: Maximilian Dietsch
\begin{itemize}
\item Order dCas9 system from Paul Hudson; for both gyrA copies
\item Get inhibitor list: gyrase, group I intron, intein; general antibiotics
\item Screen concentrations for growth effects in multicultivator
\item Test metabolic activity of selected concentrations in FMT bioreactors\\
  Goal: no growth but unchanged photosynthetic activity (O2 and CO2 use)
\end{itemize}


\subsection{The Scissors}
PI: Anna Behle
\begin{itemize}
\item pSHDY Report: gyrA/A.2, gyrB, topA and hup promoters with YFP and CFP
  reporters; to be also used for \invitro{} transcription by Lutz
\item pSHDY Express: \ecol{} CDS GyrA and TopA  with TetR \& LacI promoter
  systems for expression in all, \ecol{}, \scyst{}, \slong{}
\item Theophyllin-inducible systems? Either expression or sgRNA for dCas9?
\item Genomic Replacement of topA, gyrA by YFP and CFP reporters
\end{itemize}

\subsection{The Ladder}
PI: Salima R\"udiger
\begin{itemize}
\item Establish full $\Delta$Lk characterization of plasmids
\item Establish measurements on fragment analyzer?
\item Characterize each new expression/reporter plasmid
\item Set supercoiling level \invitro{} for measurements by Lutz
\end{itemize}

\subsection{The Lutz}
PI: Lutz Berwanger

\begin{itemize}
\item \invitro{} transcription with different polymerases,
  different promoters and with different supercoiling
\end{itemize}

\subsection{The Sonicator}
PI: Kai Hochheimer
\begin{itemize}
\item Measure genomic supercoiling by footprinting
\end{itemize}

\clearpage
%% for new refs: reset to global tata.bib, run latex/bibtex,
%% and use `bibexport -o coilhack.bib coilhack.aux`
\footnotesize
\setlength{\bibsep}{0pt}
\bibliographystyle{plainnat}
%\bibliography{/home/raim/ref/tata}
\bibliography{../references/coilhack}
\end{document}
