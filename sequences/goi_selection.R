## goi indices
## TODO: add qPCR house-keeping gene rpoA,
##       add KaiABC genes
##       add glycogen genes.

## TODO: use suppl.material classification of genes from Saha et al. 2016:
## https://mbio.asm.org/content/mbio/7/3/e00464-16/DC1/embed/inline-supplementary-material-1.xlsx?download=true

getGoi <- function(source) {

gois <- list(qPCR=c(grep("topA",source$name),
                    which(source$name=="gyrA"),
                    grep("gyrB",source$name),
                    grep("rpoA", source$name),
                    which(source$name=="gyrA2")),
             qPCR_ref=c(grep("rpoA", source$name),
                        grep("petB", source$name),
                        grep("ppc", source$name),
                        grep("secA", source$name),
                        grep("rnpB", source$name)), # NOTE: ncRNA - not in set
             off_gyrB=c(grep("topA",source$name),
                        which(source$name=="gyrA"),
                        grep("gyrB",source$name),
                        which(source$cyanobaseID=="sll1625"), #sdhB
                        which(source$cyanobaseID=="slr0896")),
             off_gyrA=c(grep("topA",source$name),
                        which(source$name=="gyrA"),
                        grep("gyrB",source$name),
                        which(source$cyanobaseID=="slr1560"), # hisS.2
                        which(source$cyanobaseID=="ssr3154")),
             off_sdh=c(which(source$cyanobaseID=="sll1625"),
                       which(source$cyanobaseID=="slr1233"),
                       which(source$cyanobaseID=="sll0823")),
             pCA=which(source$chr==7),
             pCB=which(source$chr==8),
             pCC=which(source$chr==6),
             homeostasis=c(grep("topA",source$name),
                           which(source$name=="gyrA"),
                           which(source$name=="gyrA2"),
                           grep("gyrB",source$name)),
             topoOnly=c(grep("topA",source$name),
                           which(source$name=="gyrA"),
                           grep("gyrB",source$name)),
             prakash09=c(topA=grep("topA",source$name),
                         gyrB=grep("gyrB",source$name),
                         lexA=which(source$cyanobaseID=="sll1626"),
                         nlpD=which(source$cyanobaseID=="slr0993")),
             synechan1=c(which(source$cyanobaseID=="sll1581"),
                         which(source$cyanobaseID=="sll5042"),
                         which(source$cyanobaseID=="sll5043"),
                         which(source$cyanobaseID=="sll5044")),
             synechan2=c(which(source$cyanobaseID=="ssl5045"),
                         which(source$cyanobaseID=="sll5046"),
                         which(source$cyanobaseID=="sll5047"),
                         which(source$cyanobaseID=="sll5048")),
             synechan3=c(which(source$cyanobaseID=="sll5049"),
                         which(source$cyanobaseID=="sll5050"),
                         which(source$cyanobaseID=="slr5051"),
                         which(source$cyanobaseID=="sll5052")),
             synechan4=c(which(source$cyanobaseID=="slr5053"),
                         which(source$cyanobaseID=="slr5054"),
                         which(source$cyanobaseID=="slr5055"),
                         which(source$cyanobaseID=="slr5056")),
             synechan5=c(which(source$cyanobaseID=="sll5057"),
                         which(source$cyanobaseID=="slr5058"),
                         which(source$cyanobaseID=="sll5059"),
                         which(source$cyanobaseID=="sll5060")),
             topos=c(grep("gyr",source$name),
                     grep("topA",source$name),
                     grep("tldD",source$name), # slr1322
                     grep("sll0887",source$cyanobaseID), # PmbA
                     grep("slr1435",source$cyanobaseID), # PmbA
                     grep("sll1712",source$cyanobaseID), # HU
                     grep("slr1847",source$cyanobaseID)),# YbaB/EbfC
             pentapep=grep("pentapeptide repeat-containing",
                           source$product),
             clock=c(grep("kaiA",source$name),
                     grep("kaiB",source$name),
                     grep("kaiC",source$name)),
             #grep("purF",source$name)),
             glycogen=c(glgC=grep("slr1176",source$cyanobaseID), # ADP-glucose 
                        glgA1=grep("sll0945",source$cyanobaseID),
                        glgA2=grep("sll1393",source$cyanobaseID),
                        glgP1=grep("sll1356",source$cyanobaseID), # degradation, ko has PHB
                        glgP2=grep("slr1367",source$cyanobaseID),
                        glgX=grep("slr0237",source$cyanobaseID),
                        glgX2=grep("slr1857",source$cyanobaseID)),
             phb=c(phaA=grep("phaA",source$name), # reaction 1
                   phaB=grep("phaB",source$name), # reaction 2
                   phaE=grep("phaE",source$name), # PhaEC: reaction 3
                   phaC=grep("phaC",source$name), # PhaEC
                   phaF=grep("phaF",source$name),
                   pirC=grep("sll0944",source$cyanobaseID), # inhibits pgm
                   pipX=grep("ssl0105",source$cyanobaseID), # interacts with pirC
                   pgm=grep("sll0726",source$cyanobaseID), # phosphoglucomutase
                   slr0058=grep("slr0058",source$cyanobaseID), # phasin? number of granules?
                   slr0059=grep("slr0059",source$cyanobaseID),
                   slr0060=grep("slr0060",source$cyanobaseID), # depolymerase?
                   slr0061=grep("slr0061",source$cyanobaseID)),
             signaling=c(rpaA=grep("slr0115",source$cyanobaseID),
                         rpaB=grep("slr0947",source$cyanobaseID),
                         cpmA=grep("sll1489",source$cyanobaseID),
                         sasA=grep("sll0750",source$cyanobaseID), # =hik8
                         #pmgA=grep("pmgA",source$name),
                         ntcA=grep("ntcA",source$name),
                         hik34=grep("hik34",source$name),
                         relA=grep("slr1325",source$cyanobaseID),
                         hpf=grep("lrtA",source$name)),
             nitrogen=c(ntcA=grep("ntcA",source$name),
                        gifA=grep("gifA",source$name),
                        gifB=grep("gifB",source$name),
                        hpf=grep("lrtA",source$name)),
             stress=c(grep("gro", source$name),
                      grep("hsp", source$name),
                      grep("sll1670",source$cyanobaseID),
                      grep("universal",source$product)),
             sigma=c(grep("sigA",source$name), #TODO: anti-sigma and grouping
                     grep("sigB",source$name),
                     grep("sigC",source$name),
                     grep("sigD",source$name),
                     grep("sigE",source$name),
                     grep("sigF",source$name),
                     grep("sigG",source$name),
                     grep("sigH",source$name),
                     grep("sigI",source$name)),
             sigma_assoc=c(grep("rsbU",source$name),
                           grep("slr0643",source$cyanobaseID),#S2p; cleaves sll0857, @Zhang2012b
                           grep("sll1626",source$cyanobaseID), # LexA
                           grep("sll0857",source$cyanobaseID)),# anti-sigma; @Zhang201b2
             DNA_pol=grep("DNA polym", source$product),
             RNA_pol=grep("rpo",source$name),
             RNA_pol_termination=grep("Nus", source$product),
             redox=grep("trx",source$name),
             rubisco=c(grep("rbc",source$name),
                       grep("carbonic anhydrase", source$product),
                       grep("slr0051",source$cyanobaseID)),
             carbon=c(grep("ccm",source$name),
                      grep("sbtA",source$name)),
             PSI=grep("psa",source$name),
             PSIIa=grep("psb",source$name)[1:13], # NOTE: psb28-2 and psbM go up
             PSIIb=grep("psb",source$name)[14:27], 
             NDH_all=grep("ndh",source$name),
             NDH=c(grep("ndhD2",source$name), # selected extremes!
                   grep("ndhD5",source$name),
                   ##grep("ndhB",source$name),
                   grep("ndhH",source$name),
                   grep("ndhF1",source$name),
                   grep("slr1916",source$cyanobaseID), # @Margulis2020, a putative esterase associated with redox regulation
                   grep("pmgA",source$name)),
             division=grep("min",source$name),
             ## SMC, Muk:chromosome segregation
             smc=c(grep("segregation",source$product)),
             parA=c(grep("ParA",source$product),
                    grep("slr0111",source$cyanobaseID)),
             ## McdAB: carboxysome positioning system
             mcdAB=c(McdA=grep("slr0110",source$cyanobaseID), #MYO_127120=YP_007452454/WP_010873876.1
                     McdB=grep("slr0111",source$cyanobaseID)),#MYO_127130=WP_010873877.1
             zring1_pg=c(Pbp1=c(grep("sll0002",source$cyanobaseID)),
                     Pbp2=c(grep("slr1710",source$cyanobaseID)),
                     Pbp3=c(grep("sll1434",source$cyanobaseID)),
                     FtsQ=c(grep("sll1632",source$cyanobaseID)),
                     FtsI=c(grep("sll1833",source$cyanobaseID)),
                     FtsW=c(grep("slr1267",source$cyanobaseID))),
             zring2_ring=c(FtsZ=c(grep("sll1633",source$cyanobaseID)),
                      SepF=c(grep("slr2073",source$cyanobaseID)),
                      ZipN=c(grep("sll0169",source$cyanobaseID)),
                      ZipS=c(grep("sll1939",source$cyanobaseID)),
                      YlmD=c(grep("slr1593",source$cyanobaseID)),
                      Cdv3=c(grep("slr0848",source$cyanobaseID))),
             zring3_mur=c(grep("mur",source$name)),
             motility_pilA=grep("pilA",source$name),
             motility_pilX=c(grep("pilB",source$name),
                             grep("pilC",source$name),
                             grep("pilM",source$name),
                             grep("pilT",source$name)),
             motility_other=c(grep("slr2018",source$cyanobaseID),
                              grep("slr0019",source$cyanobaseID),
                              grep("ssr3341",source$cyanobaseID),
                              grep("slr1667",source$cyanobaseID),
                              grep("slr1668",source$cyanobaseID)),
             putative_dna=c(grep("slr0270",source$cyanobaseID),
                            grep("slr0058",source$cyanobaseID),
                            grep("slr0456",source$cyanobaseID),
                            grep("sll0911",source$cyanobaseID),
                            grep("sll0269",source$cyanobaseID)),
             phycobili1=c(ApcA =grep("slr2067",source$cyanobaseID),
                          ApcB =grep("slr1986",source$cyanobaseID),
                          ApcC =grep("ssr3383",source$cyanobaseID),
                          ApcD =grep("sll0928",source$cyanobaseID),
                          ApcE =grep("slr0335",source$cyanobaseID),
                          ApcF =grep("slr1459",source$cyanobaseID)),
             phycobili2=c(CpcA =grep("sll1578",source$cyanobaseID),
                          CpcB =grep("sll1577",source$cyanobaseID),
                          petH =grep("slr1643",source$cyanobaseID),      # FNR
                          CpcC1=grep("sll1580",source$cyanobaseID),
                          CpcC2=grep("sll1579",source$cyanobaseID),
                          CpcD =grep("ssl3093",source$cyanobaseID),
                          CpcG1=grep("slr2051",source$cyanobaseID),
                          CpcG2=grep("sll1471",source$cyanobaseID)),
             phycobili3=grep("nblA", source$name),
             toxinII=grep("type II toxin",source$product),
             hydrogenas=grep("O7b",source$category),
             term_oxidases=grep("H10",source$category),
             chemotaxis=grep("D7",source$category),
             hydrogenase=c(grep("carbamoyl-phosphate synthase",source$product),
                           grep("hox",source$name),
                           grep("hyp",source$name)),
             glycolysis=c(grep("slr1334",source$cyanobaseID),
                          grep("F9",source$category)),
             TCA=grep("F13",source$category),
             #cobalamine=grep("B3",source$category),
             proteindeg1=grep("M2",source$category)[1:13],
             proteindeg2=grep("M2",source$category)[14:26],
             NADHsynth_all=c(grep("sll1415",source$cyanobaseID),
                         grep("slr0787",source$cyanobaseID),
                         grep("sll0622",source$cyanobaseID),
                         grep("slr0788",source$cyanobaseID),
                         grep("sll1916",source$cyanobaseID),
                         grep("slr0936",source$cyanobaseID),
                         grep("sll0631",source$cyanobaseID),
                         grep("slr1691",source$cyanobaseID),
                         grep("slr0400",source$cyanobaseID),
                         grep("slr1239",source$cyanobaseID),
                         grep("slr1434",source$cyanobaseID)),
             NADHsynth=c(grep("sll1415",source$cyanobaseID),
                         grep("slr0787",source$cyanobaseID),
                         grep("sll0622",source$cyanobaseID),
                         grep("slr0788",source$cyanobaseID),
                         grep("sll1916",source$cyanobaseID),
                         grep("slr0936",source$cyanobaseID),
                         grep("sll0631",source$cyanobaseID),
                         grep("slr1691",source$cyanobaseID),
                         grep("slr0400",source$cyanobaseID),
                         grep("slr1239",source$cyanobaseID),
                         grep("slr1434",source$cyanobaseID)),
             sigE=c(grep("slr1793",source$cyanobaseID),
                    grep("sll0329",source$cyanobaseID),
                    grep("slr1239",source$cyanobaseID),
                    grep("slr1843",source$cyanobaseID)), # @Kariyazono2022
             PPP=c(grep("slr1843",source$cyanobaseID),
                   grep("sll1479",source$cyanobaseID),
                   grep("sll0329",source$cyanobaseID),
                   grep("sll1070",source$cyanobaseID),
                   grep("slr1793",source$cyanobaseID),
                   grep("sll0807",source$cyanobaseID),
                   grep("slr0194",source$cyanobaseID),
                   grep("ssl2153",source$cyanobaseID)),
             ## candidates in Schmelling et al. 2017, core diurnal/circadian
             coreosci=c(grep("ssl0353",source$cyanobaseID),
                        grep("slr1577",source$cyanobaseID),
                        grep("slr1847",source$cyanobaseID)),
             condensin=c(grep("slr1577",source$cyanobaseID),
                         grep("slr1530",source$cyanobaseID)),
             ## TODO: RP - analyze long RP TU833, TU837, each 5'/mid/3'
             RP_select=c(grep("rpl11$", source$name),  # strongest L, ups rpl1
                         grep("rpl1$", source$name),  # TU931
                         grep("rpl13$", source$name), #rpoA, TU833
                         grep("rps3$", source$name),  # upstream rpoA TU837
                         grep("rps1a$", source$name), # TU1129
                         grep("nbp1$", source$name), # =rps1b, TU1878 (single!)
                         grep("rps21$", source$name), # downstream rRNA,
                         grep("rpl28$", source$name)),
             RP_large=c(grep("rpl", source$name),
                        grep("sll1909",source$cyanobaseID),
                        grep("ssl0438",source$cyanobaseID)),
             RP_small=c(grep("rps", source$name),
                        grep("slr0082",source$name),
                        grep("ycf65",source$name),
                        grep("nbp1",source$name)),
             RP_TU837=c(which(source$TU_kopf14=="TU837")),
             RP_TU833=c(which(source$TU_kopf14=="TU833")),
             RP_TU830=c(which(source$TU_kopf14=="TU830")),
             RP_TU931=c(which(source$TU_kopf14=="TU931")))
    gois
}
## glycogen synthase: glgC/slr1176, glgA1/sll0945), glgA2/sll1393
## glycogen phosphorylase:  glgP1/sll1356, GlgP2/slr1367
## glycogen debranching: GlgX1/slr0237, GlgX2/slr1857
## gyrase modulators: pmbA tldD hup
